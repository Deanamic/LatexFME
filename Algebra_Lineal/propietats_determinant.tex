\begin{prop}\label{prop:filaescalada}
	El determinant de la matriu $A'$ que s'obté multiplicant una fila o una columna sencera d'una matriu $A\in\mathcal{M}_n(\real)$ per un escalar $\lambda\in\real$ és \[\det A' = \lambda\det A\,.\]
	\begin{proof}
		A partir del \hyperref[teo:laplace]{teorema de Laplace}, podem calcular el determinant d'$A'$ mitjançant l'expansió per la fila o columna que ha estat multiplicada; per exemple, si la s'ha multiplicat per $\lambda$ la fila $k$:
		\[\det A' = \sum_{j=1}^{n} {a'}_{k,j}\cdot C_{k,j} =\sum_{j=1}^{n} \lambda {a}_{k,j} \cdot C_{k,j} = \lambda \sum_{j=1}^{n} {a'}_{k,j} {a}_{k,j} \cdot C_{k,j} = \lambda\det A\,. \]
	\end{proof}
\end{prop}

\begin{prop}\label{prop:sumafila}
	Siguin $A,B\in\matspace{n}$ matrius que \textit{difereixen només en els coeficients d'una fila o d'una columna} (com a màxim). Sigui la matriu $M$ la matriu que s'obté sumant les dues files/columnes diferents D'$A$ i $B$ i deixant la resta de coeficients iguals (que a $A$ i a $B$). Llavors \[\det M = \det A + \det B \]
	\begin{proof}
		Sigui $k$ l'índex de la columna diferent entre $A$ i $B$. Pel \hyperref[teo:laplace]{teorema de Laplace}, podem expandir el determinant de $M$ per la $k$-èsima columna:
		\begin{multline*}
			\det M = \sum_{i=1}^{n} {m}_{i,k}\cdot C_{i,k} = \sum_{i=1}^{n} ({a}_{i,k}+ {b}_{i,k})\cdot C_{i,k}=\\
			=\sum_{i=1}^{n}{a}_{i,k}\cdot C_{i,k} +\sum_{i=1}^{n} {b}_{i,k}\cdot C_{i,k}=\det A + \det B\,.
		\end{multline*}
		Es pot fer exactament el mateix raonament per files. Noteu, a més, que els cofactors $C_{i,k}$ són iguals per totes tres matrius, ja que, excepte la $k$-èsima columna/fila, tota la resta de columnes/files són iguals.
	\end{proof}
\end{prop}

\begin{prop}\label{prop:filaigual}
	Per qualsevol $A\in\matspace{n}$, si $A$ conté dues files (o columnes) iguals entre elles, llavors $\det A = 0$.
	\begin{proof}
		Siguin $r$ i $s$ els índex de les files idèntiques en $A$. Sigui $B$ la matriu que s'obté intercanviant les files $r$ i $s$ d'$A$. Llavors, per una banda, $\det B = \det A$ ja que les dues matrius són idèntiques. D'altra banda, pel lema \ref{lema:intercanvi} $\det B = -\det A$. Per tant, \[\det A = -\det A \quad \therefore \det A = 0\,. \] El mateix raonament és vàlid per columnes també.
	\end{proof}
\end{prop}

\begin{prop}
	Si sumem un múltiple d'una fila/columna d'$A\in\matspace{n}$ a una fila/columna diferent, el determinant d'$A$ no canvia.
	\begin{proof}
		Sigui $B$ la matriu que s'obté sumant la fila $r$ d'$A$ a la fila $s$ (on $r\ne s$). Sigui $\tilde{A}$ la matriu que s'obté reemplaçant la $r$-èsima fila d'$A$ per la $s$-èsima fila multiplicada per l'escalar $\lambda \in\real$. És a dir,
		\[
		A = 
		\begin{pmatrix}
		a_{1,1}&	\cdots&		a_{1,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{r,1}&	\cdots&		a_{r,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{s,1}&	\cdots&		a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{1,1}&	\cdots&		a_{1,n}\\
		\end{pmatrix}\,,
		\quad
		\tilde{A} = 
		\begin{pmatrix}
		a_{1,1}&	\cdots&		a_{1,n}\\
		\vdots&		\ddots&		\vdots\\
		\lambda a_{s,1}&	\cdots&		\lambda a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{s,1}&	\cdots&		a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{1,1}&	\cdots&		a_{1,n}\\
		\end{pmatrix}\,.
		\]
		
		Per la propietat que s'enuncia en \ref{prop:sumafila}, 
		\begin{equation}\label{eq:decomposition}
			\det B = \det A + \det \tilde{A}\,. 
		\end{equation}
		Sigui la matriu $\tilde{A}'$ la matriu que s'obté reemplaçant la $r$-èsima fila d'$A$ per la $s$-èsima fila (sense multiplicar per $\lambda $): 
		\[\tilde{A}' = 
		\begin{pmatrix}
		a_{1,1}&	\cdots&		a_{1,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{s,1}&	\cdots&		a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{s,1}&	\cdots&		a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{1,1}&	\cdots&		a_{1,n}\\
		\end{pmatrix}\,.\]
		Aplicant la proposició \ref{prop:filaescalada} en \eqref{eq:decomposition}, obtenim \[\det B = \det A + \lambda \cdot\det \tilde{A}'\,.\] Per la propietat de \ref{prop:filaigual}, $\det \tilde{A}' = 0$, ja que $\tilde{A}'$ conté una fila duplicada (la fila $s$ d'$A$). Per tant, \[\det B=\det A\,. \]
	\end{proof}	
\end{prop}

\begin{lema}\label{lema:elementals}
	Direm que una matriu elemental és de tipus $E_1$ si, quan multiplica per l'esquerra a una altra matriu, n'intercanvia la posició de dues files, de tipus $E_2$ si en multiplica escalarment una fila, i de tipus $E_3$ si en suma un múltiple d'una fila a una fila diferent. Llavors:
	\begin{enumerate}[i)]
		\item Les matrius de tipus $E_1$ tenen determinant -1.
		\item Les matrius de tipus $E_2$ tenen determinant $\lambda$, on $\lambda$ és l'escalar pel qual es multiplica la fila.
		\item Les matrius de tipus $E_3$ tenen determinant 1.
		\item Per qualsevol matriu elemental $E\in\matspace{n}$ i per qualsevol matriu $M\in\matspace{n}$, \[\det (EM) = \det E \cdot \det M\,.\]
	\end{enumerate}
	\begin{proof}
		Demostrarem cada una de les proposicions a continuació.
		\begin{enumerate}[i)]
			\item Sigui $E$ una matriu elemental de tipus $E_1$. Les matrius d'aquest tipus tenen dues files intercanviades---en concret, tenen la $r$-èsima i la $s$-èsima fila de la matriu identitat intercanviades, on $r$ i $s$ són els índex de les files que $r$ i $s$ intercanvia en la matriu $M$ quan la multiplica per l'esquerra:
			\[
			E =
			\begin{pmatrix}
			1&		0&&&		\cdots&&&		  &0\\
			0& 		1&&&		&&&					\\
			&		&	\ddots&&&&&&&	 	  		\\
			&		& 		&0&\cdots&1&&		  	\\
			\vdots&	&		&\vdots&&\vdots&&&\vdots\\
			&		& 		&1&\cdots&0&&		  	\\
			&		&		&&&&		\ddots&		\\
			&		&		&&&&		&		 1&0\\
			0&		&		&&	\cdots&&	&	 0&1\\
			\end{pmatrix}\,.
			\]
			Per tant, pel lema \ref{lema:intercanvi}, $\det E = - \det I_n = -1$.
			%
			%
			\item Sigui $E$ una matriu elemental de tipus $E_2$. Aquestes matrius són la identitat amb una fila multiplicada per $\lambda\in\real$, un escalar:
			\[
			E =
			\begin{pmatrix}
				1&		0&		\cdots&		\cdots&		\cdots& 0\\
				0&		1&		&			&&\\
				\vdots&	&		\ddots&		&&\\
				\vdots&	&		&			\lambda&&\\
				\vdots&	&		&			&			\ddots&\\
				0&		&		&			&			&		1
			\end{pmatrix}\,.
			\]
			Llavors, per la proposició \ref{prop:filaescalada}, $\det E = \lambda \det \Id = \lambda$.
			%
			%
			\item Les matrius de tipus $E_3$ són la identitat amb una de les files multiplicada per un escalar $\lambda$ sumada a una de diferent. Per exemple,
			\[
			E =
			\begin{pmatrix}
				1&		0&		\cdots&		\lambda&		\cdots& 0\\
				0&		1&		&			&&\\
				\vdots&	&		\ddots&		&&\\
				\vdots&	&		&			1&&\\
				\vdots&	&		&			&			\ddots&\\
				0&		&		&			&			&		1
			\end{pmatrix}\,.
			\]
			Sigui $E$ una matriu arbitrària d'aquest tipus. Per la proposició \ref{prop:sumafila}, el determinant d'$E$ és el mateix que el de la identitat, i per tant $\det E = 1$.
			%
			%
			\item Sigui $M\in\matspace{n}$ una matriu qualsevol, siguin $\tilde{M}_1$ $\tilde{M}_2$ i $\tilde{M}_3$ les matrius que s'obtenen aplicant transformacions elementals qualssevol de tipus 1, 2 i 3 respectivament i siguin $E_1$, $E_2$ i $E_3$ les matrius elementals corresponent (respectivament). D'una banda, per les proposicions \ref{lema:intercanvi}, \ref{prop:filaescalada} i \ref{prop:sumafila}, tenim que 
			\[\det \tilde{M}_1 = -\det M,\ \det \tilde{M}_2 = \lambda \det M \text{ i } \det \tilde{M}_3 = \det M\,,\]
			respectivament. D'altra banda, per les proposicions de i), ii) i iii), tenim que 
			\[\det E_1 = -1,\ \det E_2 = \lambda \text{ i } \det E_3 = 1\,.\]
			Per tant és cert que
			\[\det \tilde{M}_1 = \det{E_1}\det M,\ \det \tilde{M}_2 = \det{E_2} \det M \text{ i } \det \tilde{M}_3 =\det E_3 \det M\,.\]
		\end{enumerate}
	\end{proof}
\end{lema}

\begin{lema}\label{lema:descomp-elem}
	Qualsevol matriu $A\in\matspace{n}$ es pot ``descompondre'' en un seguit de matrius elementals $E_1, \ldots,E_m \in \matspace{n}$ de manera que
	\[
		A = E_m\cdots E_1\Id_n
	\]
\end{lema}

\begin{teo*}
	Per dues matrius $A,B \in \matspace{n}$ qualssevol, \[\det(AB) = \det A\cdot \det B = \det (BA)\,. \]
	\begin{proof}
		Qualsevol matriu $A\in\matspace{n}$ es pot descompondre en un seguit d'operacions elementals sobre la matriu identitat (lema \ref{lema:descomp-elem}). És a dir, existeixen matrius elementals $E_1,\ldots,E_m\in\matspace{n}$ tals que
		\[
			A = E_m\cdots E_2E_1\,.
		\]
		Per tant
		\[
			AB = (E_m\cdots E_2E_1)B\,.
		\]
		Aplicant el lema \ref{lema:elementals} recursivament:
		\begin{multline}\label{eq:det-prod}
			\det (AB) = \det E_m \cdot \det (E_{m-1}\cdots E_1B)= \\= \det E_m\cdot\det E_{m-1} \cdot \det(E_{m-2}\cdots E_1 B) = \cdots \\\cdots  = \det E_m \cdot \cdots \cdot \det E_1 \cdot \det B\,.
		\end{multline}
		Ara, tornant a aplicar \ref{lema:elementals} recursivament però en sentit recíproc,
		\begin{multline*}
			\det (AB) = \left[\det E_m \cdot \cdots \cdot \det E_2 \cdot \det E_1\right] \cdot \det B =\\= \left[\det E_m \cdot \cdots \cdot \det E_3 \cdot\det (E_2E_1)\right]\det B =\\= \left[\det E_m \cdot \cdots \cdot \det E_4 \cdot\det (E_3E_2E_1)\right]\cdot B = \cdots \\
			 \cdots = \det(E_m\cdots E_1)\det B = \det A \cdot \det B\,.
		\end{multline*}
		
		El fet que $\det(AB) = \det(BA)$ és conseqüència directa d'això últim i de la commutabilitat de la multiplicació entre escalars.
	\end{proof}
\end{teo*}


