\chapter{Equació de la calor o de difusió}
\section{Introducció}
Considerem una vareta modelada com un segment $[0, L]$.
Al llarg del tema $u(x, t)$ modelarà la temperatura
de la vareta al punt $x \in [0, L]$ i instant $t$ o la concentració d'una substància que es
difon al llarg de la vareta. Com hem fet ja a temes anteriors, deduirem l'equació
diferencial que regeix el comportament d'aquests fenòmens a partir de sengles principis
físics. La llei de Fourier diu que la calor flueix dels punts m\'es calents als m\'es freds
amb flux proporcional al gradient de la temperatura. La llei de Fick estableix el
mateix comportament al estudiar la concentració d'una substància en difusió. 

Considerem ara un interval $[a, b]$ de la vareta. La massa o energia calòrica totals
es calculen com $\int_a^b u(x, t)$. Si suposem que no hi ha fonts de calor (o de massa) a 
la vareta, la variació d'aquesta quantitat depèn exclusivament
de la massa o calor que entra o surt per la vora, \'es a dir, el flux que passa a
trav\'es de la vora. Per tant,
\[
    \frac{\dif}{\dif t} \int_a^b u(x, t) \dif x = q(b, t) - q(a, t) 
    \stackrel{\text{Fick}} = D(u_x(b, t) - u_x(a, t)).
\]
D'on
\[
    \int_a^b u_t(x, t) \dif x - \int_a^b u_{xx}(x, t) \dif x = 0.
\]
Com això \'es cert per tot $[a, b]$, en deduïm l'equació del calor,
\[
    u_t = Du_{xx},
\]
per $x \in (0, L)$ i $t > 0$. La constant $D > 0$ s'anomena coeficient de difusió. Reescalant
adequadament les variables, podem assumir que $D = 1$.

Quan autors com Euler, D'Alembert o Fourier van començar l'estudi de l'equació de la calor, 
es coneixien exemples particulars de solucions. Per exemple, la funció
\[
    u(x, t) = e^{-k^2 t} \sin(kx)
\]
satisfà
\[
    u_{xx} = -k^2 e^{-k^2} \sin(kx) = u_t.
\]
A partir d'aquests exemples un pot observar que $\partial_{xx} \sin(kx) = -k^2 \sin(kx)$.
Diem que $\sin(kx)$ \'es una funció pròpia de l'operador $\partial_{xx}$ amb valor
propi $-k^2$. Podem veure, a m\'es, que l'operador $A = \partial_{xx}$ \'es simètric.
Recordem que un operador \'es simètric si $(Ax, y) = (x, Ay) \, \forall x, y \in \E$ 
(en el cas de dimensió finita \'es equivalent a $A^T = A$). En aquest cas, considerant
el producte escalar habitual de $L^2$, $(u, v) = (u, v)_{L^2} = \int_0^L uv$, es t\'e
\[
    (Au, v) = \int_0^L u_{xx} v = \left[u_x v \right]_0^L - \int_0^Lu_xv_x = \int_0^Luv_{xx} = (u, Av),
\]
on el primer terme de la resta s'anul·la si tenim condicions de Dirichlet o de Neumann.
Els vectors propis de valors propis diferents són ortogonals, i
que el teorema espectral real diu que un operador simètric i compacte diagonalitza.
Intentarem diagonalitzar l'operador $\partial_{xx}$, \'es a dir, trobar una base de
funcions que siguin funcions pròpies de l'operador i que les seves combinacions lineals
generin totes les funcions de l'espai que ens interessi. Concretament, 
si fixem $L= \pi$ i plantegem el problema amb condicions de Dirichlet:
\[
    \begin{cases}
        u_t = u_{xx} \\
        u(0, t) = u(\pi, t) = 0
    \end{cases}
\]
Com ja hem dit, les funcions $\sin(kx)$ són funcions pròpies de $\partial_{xx}$ i cal que
$k \in \z$ per tal que siguin solucions del problema, \'es a dir, per tal que pertanyin
a l'espai de funcions que volem estudiar. Per ara, procedirem per analogia amb el
cas de dimensió finita i intentarem veure si obtenim així la solució que busquem. Concretament,
si $A$ fos un operador de dimensió finita i tinguéssim el problema
\[
    \begin{cases}
        u_t = Au \\
        u(0) = g
    \end{cases}
\]
Obtindríem la solució $u = e^{At} g$, i, si $A$ diagonalitz\'es amb valors propis $\lambda_k$,
tindríem
\[
    u = 
    \begin{pmatrix}
        e^{\lambda_1 t} g_1 \\
        \vdots \\
        e^{\lambda_k t} g_k
    \end{pmatrix}
\]
Volem veure ara si les combinacions lineals de la família $\sin(kx)$ amb $k \in \z^{+}$ 
poden generar totes les solucions del problema, però sabem que això \'es cert perquè qualsevol
condició inicial $g = g(x) \in L^2$ admet una sèrie de Fourier de la forma 
\[
    g(x) = \sum_{k=1}^{\infty} b_k \sin(kx),
\]
on els termes dels cosinus són innecessaris perquè ens trobem a l'interval $(0, \pi)$. Aleshores
la solució del problema\footnote{Aquesta deducció de la solució de l'equació de la 
calor \'es una primera aproximació al problema. La demostració que fem \'es vàlida, 
però en farem els detalls amb rigor m\'es endavant.} \'es de la forma
\[
    u(x, t) = \sum_{k = 1}^{\infty} b_k e^{-k^2t} \sin(kx).
\]
Notem que $u$ serà solució del problema si podem derivar sota el signe d'integral i que satisfà
la condició inicial $u(x, 0) = g(x)$. Per tant, si tenim unicitat de solucions, com veurem
m\'es endavant, totes les solucions es poden expressar així.

\section{Mètode de separació de variables}
Fins ara hem vist una via per resoldre el problema mitjançant combinacions lineals de sinus,
funcions pròpies del laplacià. Veurem ara que tamb\'e es pot resoldre amb el mètode de
separació de variables, que \'es m\'es calculístic però \'es útil per altres EDPs com
la d'ones o la d'electromagnetisme.

Considerem el problema $u_t = Du_{xx}$. Busquem solucions de l'equació a variables
separades, \'es a dir, de la forma $u(x, t) = v(x) w(t)$. Si el problema \'es adequat,
obtindrem EDOs per a $v$ i $w$.
\begin{enumerate}[i)]
    \item En primer lloc, cal imposar l'equació $u_t = Du_{xx}$. Separant $u$,
        \[
            u_t = v(x)w'(t) = Dv''(x) w(t) = u_{xx}.
        \]
        \'Es a dir,
        \[
            -\frac{v''(x)}{v(x)} = \frac{-w'(t)}{Dw(t)} = c,
        \]
        amb $c$ constant perquè no depèn ni de la posició ni del temps.
    \item En segon lloc, cal imposar les condicions de vora i resoldre la $EDO$
        corresponent:
        \[
            \begin{cases}
                v(0) = v(1) = 0 &\\
                -v''(x) = c v(x)& x \in (0, L)
            \end{cases}
        \]
        Si fixem $c < 0$, es t\'e
        \[
            v(x) = ae^{\sqrt{-c} x} + b e^{-\sqrt{-c}h}.
        \]
        Imposant $v(0) = 0$, tenim $a+b = 0$, i imposant $v(L) = 0$ obtenim
        \[
            ae^{\sqrt{-c} L} - ae^{-\sqrt{-c}L} = 0
        \]
        Que nom\'es \'es cert si $a = b = 0$. Per tant, aquest cas no ens proporciona
        solucions de variables separades.  Ara b\'e, si fixem $c > 0$, 
        obtenim les solucions de la forma
        \[
            v(x) = a\sin(\sqrt{c} x) + b\cos(\sqrt{c} x).
        \]
        Amb $v(0) = 0$ obtenim $b = 0$ i imposant $v(L) = 0$, tenim que
        $\sin(\sqrt{c}L) = 0$, \'es a dir, $c = \lp\frac{k \pi}{L}\rp^2$. Per
        tant, en aquest cas obtenim la família de solucions de la forma
        $v(x) = a_k \sin\lp \frac{k \pi}{L} x \rp$ per $k \in \n^+$. Trobem
        aleshores la $w$ associada a $v$ per $k$ fixada
        \[
            \frac{-w'(t)}{Dw(t)} = \lp\frac{k\pi}{L} \rp^2 \implies 
            w(t) = e^{-\lp\frac{k\pi}{L} \rp^2 D t}.
        \]
    \item Finalment, hem d'imposar la condició inicial del problema. Considerem com a
        candidates a solució les combinacions lineals de les solucions a variables
        separades obtingudes. \'Es a dir,
        \[
            u(x, t) = \sum_{k= 1}^{\infty} b_k e^{-\lp\frac{k\pi}{L} \rp^2 D t} 
            \sin\lp\frac{k\pi}{L} x \rp.
        \]
        I, a $t = 0$, volem que
        \[
            g(x) = u(x, 0) =\sum_{k= 1}^{\infty} b_k \sin\lp\frac{k\pi}{L} x\rp .
        \]
\end{enumerate}
\section{L'equació del calor en una vareta homogènia}
\begin{obs}
    Les funcions $L$-periòdiques i $L^2$ es poden escriure en termes de la seva 
    seriè de Fourier
    \[
        s_N(x) = \frac{a_0}{2} + \sum_{k = 1}^N \lp a_k \cos \lp \frac{2k \pi}{L} x \rp
        + b_k \sin \lp \frac{2k \pi}{L} x \rp \rp.
    \]
    i $\lim_{n\to \infty} ||f - s_n|| = 0$.
    Cal precisar que la igualtat no \'es estricta sinó que la diferència entre totes
    dues funcions t\'e norma $0$ en $L^2$.
\end{obs}
\begin{prop}
    Donat $L > 0$, la família de funcions $\left \{ \sqrt{\frac{2}{L}} \sin\lp
    \frac{k\pi}{L} x\rp\right\}$ per $k \in \n^+$ \'es una base ortonormal de $L^2(0, L)$.
\end{prop}
\begin{proof}
    Donada $g \in L^2(0, L)$, definim la reflexió senar
    \[
        g_s(x) = 
        \begin{cases}
            g(x) & \text {si } x \in (0, L) \\
            -g(x) & \text{si } x \in (-L, 0)
        \end{cases}
    \]
    Tenim doncs que $g_s \in L^2(-L, L)$ i per tant sabem que, per
    \[
        s_N(x) = \frac{a_0}{2} + \sum_{k = 1}^N \lp a_k \cos \lp \frac{k \pi}{L} x \rp
        + b_k \sin \lp \frac{k \pi}{L} x \rp \rp.
    \]
    on $a_k$ i $b_k$ són els coeficients de la sèrie de Fourier, es t\'e
    $\lim_{n\to \infty} ||g_s - s_n||_2 = 0$. A m\'es,
    \[
        a_k = \frac{1}{L} \int_{-L}^L g_s(x) \cos \lp \frac{k \pi}{L} x \rp \dif x.
    \]
    Per ser el primer terme del producte senar i el segon parell, el producte \'es
    senar i la integral \'es zero, \'es a dir, $a_k = 0$. Anàlogament, $a_0 = 
    \frac{1}{2L}\int_{-L}^L g_s(x) = 0$. Per tant,
    \[
        s_N(x) = \sum_{k = 1}^{\infty} b_k \sin \lp \frac{k \pi}{L} x \rp.
    \]
    I en particular l'aproximació de $g_s$ tamb\'e serà vàlida per $g(x) = g_s(x)$ a 
    l'interval $(0, L)$.  \'Es a dir, hem vist que
    \[
        \lim_{n\to \infty} ||g_s - s_n||_2 = \lim_{n \to \infty} 
        \int_0^L | g(x) - s_k|^2 \dif x = 0.
    \]
    Encara quedaria veure que els elements de la base són de norma $1$ i
    ortogonals entre ells. Es deixa com exercici pel lector.
\end{proof}
\begin{obs}
    Així com per les condicions de Dirichlet la base ortonormal adecuada de
    $L^2(0, \pi)$ \'es $\left \{ \sqrt{\frac{2}{L}} \sin\lp
    \frac{k\pi}{L} x \rp\right\}$ per $k \in \n^+$, per les condicions de Neumann
    la base ortonormal adecuada \'es $\left \{ \sqrt{\frac{2}{L}} 
    \cos\lp \frac{k\pi}{L} x \rp\right\}$ per $k \in \n^+$, perquè volem que els
    elements de la base satisfacin les condicions de vora.
    %TODO revisar coeficient cosinus
\end{obs}

Presentem ara diferents models de condicions de vora.
\begin{itemize}
    \item Condicions de Dirichlet: Es prescriu el valor de $u$ a la vora de $(0,L)$.
        En aquest cas absorbim tota la calor a la vora i la mantenim sempre a
        temperatura zero. Per exemple, si hi ha una massa gran, com el mar, que pot
        absorbir tanta energia com calgui.
    \item Condicions de Neumann (o aïllants): La vora de $(0, L)$ \'es aïllant,
        no hi ha flux de calor a la vora.
        \[
            u_x(0, t) = u_x(L, t) = 0 \, \forall t \geq 0.
        \]
    \item Condicions periòdiques: Vareta circular de longitud L, \'es a dir,
        de radi $R = \frac{L}{2 \pi}$.
        \[
            \begin{rcases}
            u(0, t) = u(L, t) \\ 
                u_x(0, t) = u_x(L, t)
            \end{rcases}
            \forall t \geq 0.
        \]
    \item Condicions mixtes: $u(0, t) = 0$ i $u_x(L, t) = 0$.
    \item Condicions de Robin (o radiació):
        \begin{gather*}
            u_x(0, t) + \alpha u(0, t) = 0 \\
            u_x(L, t) + \beta u(L, t) = 0
        \end{gather*}
        Amb $\alpha, \beta > 0$ constants. Aquestes constants descriuen materials
        que aïllen m\'es o menys en funció de la temperatura.
\end{itemize}
\begin{obs}
    En el cas de Neumann o periòdic no s'absorbeix o s'afegeix calor. Per tant,
    l'energia calòrica total (o la massa de la substància) es conserva en el
    temps
    \[
        \frac{\dif}{\dif t} \int_0^L u(x, t) \dif x = \int_0^L u_t(x, t) \dif x
        = D \int_0^L u_{xx}(x, t) \dif x = D (u_x(L, t) - u_x(0, t)) = 0.
    \]
\end{obs}
\subsection{Existència i unicitat pel problema de Dirichlet}
Recordem el problema de Dirichlet de la calor o de la difusió:
\[
    \begin{cases}
        u_t = Du_{xx} & (x, t) \in (0, \pi)\times(0, \infty) \\
        u(0, t) = u(\pi, t) = 0 & t > 0 \\
        u(x, 0) = g(x) & x \in (0, \infty)
    \end{cases}
\]
Si $g(x) \in L^2$ i $g(x) = \sum_{k=0}^{\infty} b_k \sin(kx)$, considerem
$u(x, t) = \sum_{k \geq 1} b_k e^{-k^2t} sin(kx)$ que hem vist que \'es
solució en sentit formal. Vegem ara amb m\'es precisió el caràcter
d'aquesta solució.
\begin{obs}
    Per la demostració del següent teorema recordem el següent resultat propi
    de l'anàlisi real. Donada una succesió de funcions $h_n$ tal que existeix
    $x_0$ amb $h_n(x_0) \to h (x)$ puntualment i $h_n' \to y$ uniformement,
    aleshores $h$ és diferenciable i $h' = y$.
\end{obs}

\begin{teo*}
    Sigui $g \in L^2(0, \pi)$ i $b_k$ els seus coeficients de Fourier en base
    de sinus. Aleshores
    \begin{enumerate}[i)]
    \item La funció $u$ definida com $u(x, t) = \sum_{k \geq 1} b_k e^{-k^2t} 
        \sin(k x)$
        \'es la única funció $u \in \C^\infty \lp [0, \pi] \times (0, \infty) \rp$
        que satisfà
        \[
            \begin{cases}
                u_t = u_{xx} & (x, t) \in (0, \pi)\times(0, \infty) \\
                u(0, t) = u(\pi, t) = 0 & t > 0 \\
                u(\cdot, t) \to g(x) & \text{en } L^2 \text { quan } t \to 0 
            \end{cases}
        \]
    \item Si a més a més $g \in \C^1([0, \pi])$ i $g(0) = g(\pi)$, aleshores
        $u \in \C^0\lp [0, \pi] \times [0, \infty) \rp$ i $u(x, 0) = g(x) \,
        \forall x \in [0, \pi]$.
    \item Podem fitar la seva norma com 
        \[
            ||u(\cdot, t)||_{L^2(0, \pi)} \leq e^{-t} ||g||_{L^2(0, \pi)}
        \]
    \end{enumerate}
\end{teo*}
\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item 
            Veurem el primer apartat per parts. Vegem en primer lloc 
            la regularitat de la solució per $t \geq 0$.
            Considerem la successió dels coeficients de la sèrie de Fourier de $g$,
            $(b_k) \in l^2$, és a dir, $\sum_{k \geq 1} b_k^2 < \infty$. Es té que
            $b_k \to 0$, i per tant $b_k \leq B$ per alguna $B$. Sigui $t_0$ un
            temps tan petit com vulguem. Demostrarem que tenim convergència uniforme a
            $[0, \pi] \times \lb t_0, \infty\rp$
            per totes les derivades. Per poder aplicar
            el resultat de l'observació anterior, en primer lloc fitem les derivades.
            Es té que
            \footnote{
                La derivada del sinus és el sinus o el cosinus segons la paritat de $j$.
            }
            \begin{gather*}
                \left |\partial_t^{(i)} \partial_x^{(j)} u_N(x, t) \right | = 
                \left | \sum_{k = 1}^N b_k (-k^2)^i k^j e^{-k^2t} 
                \left [ \pm \sin(kx) \text{ o } \pm \cos(kx) \right] \right | \leq \\
                \leq \sum_{k=1}^N |B| k^{2i + j} e^{-k^2 t_0}  < \infty.
            \end{gather*}
            Aplicant el criteri $M$ de Weierstrass les derivades de $u_N$ convergeixen
            uniformement i podem aplicar el resultat anterior per dir que les $u_N$ 
            convergeixen uniformement i són infinitament derivables. En particular,
            podem derivar terme a terme i obtenim que
            \[
                u_t = u_{xx}.
            \]
            A més, $u(0, t) = u(\pi, t) = 0$, perquè $u_N(0, t) = u_N(\pi, t) = 0$ i tenim
            convergència a $0$ i $\pi$. 

            Vegem ara que $u(\cdot, t) \to g$ a $L^2(0, \pi)$ quan $t \to 0$, és a dir,
            \[
                ||u(\cdot, t) - g||_{L^2(0, \pi)}^2 \to 0
            \]
            quan $t \to 0$. Tenim que
            \begin{gather*}
                ||u(\cdot, t) - g||_{L^2(0, \pi)}^2 = 
                \int_0^\pi |u(x, t) - g(x)|^2 \dif x = \\
                = \frac{\pi}{2} \int_0^\pi \left|\sum_{k=1}^{\infty} b_k \lp e^{-k^2t} -1\rp
                \sin(kx) \sqrt{\frac{2}{\pi}} \right|^2 \dif x
            \end{gather*}
            Que, per la identitat de Parseval, és igual a
            \begin{equation}
                \label{eq:norma_suma}
                \frac{\pi}{2} \sum_{k = 1}^{\infty} |b_k|^2|e^{-k^2t} - 1|^2.
            \end{equation}
            A m\'es, com podem fitar els termes de la suma per
            \[
                |b_k|^2 |e^{-k^2t} -1| \leq |b_k|^2.
            \]
            I la suma $\sum_{k \geq 1} |b_k|^2 < \infty$ com hem dit al principi, per tant 
            si fem tendir $t$ a $0$ la suma de $\ref{eq:norma_suma}$, 
            podem intercanviar el límit i la suma i obtenir
            \[
                \lim_{t \to 0} \sum_{k = 1}^{\infty} |b_k|^2 |e^{-k^2t} -1| =
                \sum_{k = 1}^{\infty} \lim_{t \to 0} |b_k|^2 |e^{-k^2t} -1|  = 0.
            \]

            Demostrem finalment la unicitat de la solució. Suposem que $u, v$ són solucions
            del problema. Aleshores $w := u-v$ satisfà el mateix problema que hem plantejat,
            però amb $g = 0$. Per tant, $w(\cdot, t) \to 0$ a $L^2(0, \pi)$ quan $t \to 0$.
            Farem servir el mètode integral o ``de l'energia'', com vam fer per l'equació d'ones.
            Considerem
            \[
                E(t) = \int_0^{\pi} w^2(x, t) \dif x.
            \]
            Per $t > 0$, tenim que
            \begin{gather*}
                E'(t) = \int_0^\pi w w_t \dif x = \int_0^\pi 2 w w_{xx} \dif x = \\
                = [2 w w_x]_0^\pi - \int_0^\pi 2 w_x w_x \dif x  =
                - \int_0^\pi 2 w_x^2  \dif x \leq 0.
            \end{gather*}
            \'Es a dir, $E(t)$ \'es decreixent en el temps. A m\'es, $E(t) \geq 0$ i $\lim_{t \to 0}
            E(t) = 0$, i per tant $E(t) = 0$ per a tot $t \geq 0$. Això implica que $w(x, t) = 0$,
            d'on $u = v$.
        \item No demostrarem el segon apartat, es fa com el primer exercici de la
            llista 5. 
        \item 
            \[
                ||u(\cdot, t)||^2_{L^2(0, \pi)} = \frac{\pi}{2}
                \sum_{k = 1}^{\infty} |b_k|^2 |e^{-2k^2t}| \leq 
                e^{-2t} \sum_{k = 1}^{\infty} |b_k|^2  = e^{-2t} ||g||^2_{L^2(0, \pi)}
            \]
    \end{enumerate}
\end{proof}
\begin{col}
    \label{col:colcalor}
    L'equació del calor t\'e un efecte regularitzador. Per a qualsevol
    $g \in L^2(0, \pi)$, la solució $u$ esdev\'e $C^{\infty}$ en $x$ i $t$
    instantàniament, \'es a dir, per a tot temps $t_0$ arbitràriament petit.
\end{col}
\begin{obs}
    \begin{enumerate}[i)]
        \item[]
        \item Les condicions de vora fan que la temperatura tendeixi cap a zero
            exponencialment ràpid en el temps.
        \item El corol·lari anterior \ref{col:colcalor} estableix una diferència
            molt important entre les solucions de $u_t = u_{xx}$ i $u_{tt} = u_{xx}$,
            ja que les ones no tenen cap efecte regularitzant sobre la condició inicial.
    \end{enumerate}
\end{obs}
\begin{col}
    Per la majoria de condicions inicial $g$, per exemple sempre que $g$ no sigui
    analítica, no es pot resoldre l'equació de difusió per temps negatius, per petits
    que siguin aquests temps. Diem que l'equació de difusió \'es irreversible.
\end{col}
\begin{proof}
    Hem demostrat que la solució per temps futur \'es $C^{\infty}$ en $x$ i $t$ si
    $g \in L^2(0, \pi)$. \'Es m\'es, l'expressió de la solució $u$ implica que aquesta
    \'es analítica\footnote{No demostrarem aquest fet.}.
    Suposem ara que existeix $\varepsilon > 0$ tal
    que es pot resoldre l'equació a $t \in (-\varepsilon, \infty)$. Aleshores $t = 0$ \'es
    un temps futur respecte de $-\varepsilon$ i per tant es pot expressar com la
    solució d'una condició inicial passada, cosa que implica que $g$ ha de ser
    $\C^{\infty}$ i analítica.
\end{proof}
\begin{obs}
    Per algunes funcions analítiques (les que tenen coeficients de Fourier que decauen
    prou ràpid), $u(x, t) = \sum_{k=1}^{\infty} b_k e^{-k^2 t} \sin(kx)$ encara pot
    definir una solució per $-\varepsilon < t < 0$, amb $\varepsilon$ petit.
\end{obs}
\begin{obs}
    \begin{enumerate}[i)]
        \item[]
        \item Considerem el cas amb coeficient de difusió $D > 0$ amb condicions de
            Dirichlet i una vareta de longitud $L$. Aleshores la solució \'es de
            la forma
            \[
                u(x, t) = \sum_{k = 1}^{\infty} b_k e^{-k^2 \lp \frac{\pi}{L} \rp^2 Dt} 
                \sin(k \frac{\pi}{L}x).
            \]
            I podem observar que la taxa de refredament de la vareta \'es inversament
            proporcional a $L^2$. \'Es a dir, si augmenta la longitud de la vareta,
            l'exponencial decau m\'es lentament. En canvi, \'es directament proporcional
            a $D$, i, per tant, l'exponencial decau m\'es lentament si $D$ decreix.
        \item El mètode de separació de variables \'es equivalent a diagonalitzar
            l'operador (en aquest cas el Laplacià). \'Es, però, útil tamb\'e per
            altres equacions, com ara l'equació d'ones.
            Si considerem el problema
            \[
                u_{tt} = c^2 u_{xx},
            \]
            i busquem solucions de la forma $u = v(x) w(t)$, obtenim dues EDOs. La
            primera ens diu que $\frac{v''(x)}{v(x)} = \lambda$, d'on
            \[
                    v(x) = \sin(\frac{k\pi}{L}x),
            \]
            i $\lambda = k^2 \lp \frac{\pi}{L} \rp^2$. La segona ens diu que
            $\frac{w''(t)}{c^2 w(t)} = \lambda$, d'on
            \[
                w_k(t) = a_k \cos \lp \frac{k \pi}{L} c t \rp + b_k \sin \lp
                \frac{k\pi}{L}ct \rp.
            \]
            Si ara imposem les condicions de vora i inicials, suposant que
            \begin{gather*}
                g(x) = \sum_{k=1}^{\infty} \tilde{a}_k \sin \lp \frac{k \pi}{L} x \rp, \\
                h(x) = \sum_{k=1}^{\infty} \tilde{b}_k \cos \lp \frac{k \pi}{L} x \rp.
            \end{gather*}
            aleshores
            \[
                u(x, t) = \sum_{k=1}^{\infty} \lb \tilde{a}_k \sin \lp \frac{k \pi}{L} ct \rp
                + \tilde{b}_k \frac{L}{c \pi k}\cos \lp \frac{k \pi}{L} ct \rp \rb 
                \sin \lp k \frac{\pi}{L}x \rp.
            \]
            En particular, la solució $u$ \'es periòdica en el temps amb 
            període $\frac{2L}{c}$.



    \end{enumerate}
\end{obs}
\section{El cas no homogeni}
\subsection{Vareta no homogènia. Teoria de Sturm-Liouville}
Sigui $p = p(x)$ la conductivitat tèrmica de la vareta, i $r = r(x)$ la seva
densitat de massa. Aleshores, amb el model que havíem plantejat, l'equació
resultant \'es
\[
    r(x) u_t = (p(x)u_x)_x,
\]
\'es a dir,
\[
    r(x) u_t = p(x) u_{xx} + p'(x)u_x.
\]
També el podem escriure com $u_t = Au$, on $A$ \'es l'operador
$A = \frac{1}{r(x)}\partial_x(p(x) \partial_x)$. Voldríem aplicar el teorema espectral
real per poder afirmar que $A$ diagonalitza. Ara b\'e, 
amb el producte escalar habitual $A$ no és simètric. Definim aleshores
el producte escalar
\[
    <u, v>_r = \int_0^L uv r(x) \dif x,
\]
suposant $r > 0, p > 0$ prou regulars.
Amb aquest producte escalar,
\begin{gather*}
    <Au, v>_r = \int_0^L \frac{1}{r(x)}(p(x) u_x)_x v r(x) \dif x= 
    [p(x) u_x v]_0^L - \int_0^L u_x p(x) v_x \dif x = \\
    =-[u p(x) v_x]_0^L + \int_0^L u (p(x) v_x)_x \dif x = <u, Av>_r.
\end{gather*}
És a dir, $A$ és simètric. A més, tenim que amb aquest producte escalar $L^2(a, b)$
és un espai de Hilbert amb la mateixa topologia que amb el producte escalar
habitual, perquè
\[
    0 < c_1 \leq r(x) \leq c_2,
\]
per a tota $x \in [a, b]$, i per tant
\[
    c_1 ||u||_{L^2} \leq ||u||_r \leq c_2 ||u||_{L^2}.
\]
%TODO revisar tema L^2 i derivabilitat i, si satisfà les condicions als extrems.
\begin{teo*}
    Existeix una base ortonormal de $L^2(a, b)$ respecte el producte escalar
    $<\cdot, \cdot>_r$ formada per funcions pròpies de l'operador $A$. És a
    dir, existeixen $\varphi_1,\varphi_2, \dots \in L^2(a, b)$ ortonormals
    i $\lambda_1 < \lambda_2 < \dots$ valors propis tals que per tota $k$
    \[
        \begin{cases}
            \frac{-1}{r(x)} (p(x) \partial_x \varphi_k(x))_x
            = \lambda_k \varphi_k(x) \\
            \varphi_k(a) = \varphi_k(b) = 0
        \end{cases}
    \]
    Aleshores, si $g(x) = \sum_{k=1}^{\infty} b_k \varphi_k(x)$, la solució és
    \[
        u(x, t) = \sum_{k=1}^{\infty} b_k e^{-\lambda_k t} \varphi_k(x).
    \]
\end{teo*}
\begin{example}
    \begin{enumerate}[i)]
    \item[]
    \item Hermite: $r(x) = e^{-\frac{x^2}{2}}$, amb $x \in (-\infty, \infty)$.
    \item Laguerre: $r(x) = e^{-x}$, amb $x \in (0, \infty)$.
    \item Chebyshev: $r(x) = \frac{1}{\sqrt{1-x^2}}$, amb $x \in (-1, 1)$.
\end{enumerate}
\end{example}
\subsection{Equació de la calor no homogènia}
Tornant al problema original, considerem
\[
    \begin{cases}
        u_t = u_{xx} + f(x, t) & x \in (0, \pi), \, t > 0 \\
        u(0, t) = u(\pi, t) = 0 & t > 0 \\
        u(x, 0) = g(x) & x \in (0, \pi)
    \end{cases}
\]
On hem introduït el terme no homogeni $f(x, t)$. La fórmula de Duhamel (\ref{defi:Duhamel}) ens diu que
\[
    u(x, t) = (T_t g)(x) + \int_0^t T_{t-s} f(\cdot, s)(x) \dif s.
\]
Amb el semigrup lineal i continu
\begin{align*}
    T_t \colon L^2(0, \pi) &\to L^2(0, \pi) \\
    g &\mapsto (T_t g)(x) = \sum_{k=1}^{\infty} b_k e^{-k^2t} \sin(kx)
\end{align*}
Tenim que
\[
    ||T_t g||^2 = \sum_{k=1}^{\infty} b_k^2 e^{-2k^2t} \frac{\pi}{2} \leq
    e^{-2t}||g||.
\]
Per tant, $T_t$ \'es una contracció.

Alternativament, sense usar la fórmula de Duhamel, podem resoldre el problema
buscant una solució de la forma
\[
    u(x, t) = \sum_{k=1}^{\infty} c_k(t) \sin(kx).
\]
Si $f(x, t) = \sum_{k \geq 1} d_k(t) \sin(kx)$, com coneixem $b_k$ i $d_k(t)$
podem imposar l'EDO de primer ordre per $c_k(t)$. Serà una EDO del tipus
\[
    c_k' + ac_k = e_k(t),
\]
que haurem de resoldre utilitzant el mètode de variació de les constants.
\begin{obs}
    Donades condicions de vora no homogènies podem resoldre el problema restant
    a $u$ una funció $h(x, t)$ que satisfaci les condicions de vora.
\end{obs}
Resolguem el cas particular on $f(x)$ no depèn de $t$. Podem interpretar $f$
com una font de calor o fred a cada punt de l'interior de la vareta. La
podem resoldre explícitament amb la fórmula de Duhamel. Si $b_k$ i $d_k$ 
són els coeficients de Fourier de $g$ i $f$, tenim que el seu comportament
assimptòtic quan $t\to\infty$ satisfà
\begin{gather*}
    \lim_{t \to \infty} u(x, t) = \lim_{t \to \infty} \sum_{k=1}^{\infty} b_k e^{-k^2t} \sin(kx) + \int_0^t
    \sum_{k= 1}^{\infty} d_k e^{-k^2(t-s)} \sin(kx) \dif s = \\
    = \lim_{t \to \infty} \sum_{k = 1}^{\infty} 
    d_k e^{-k^2 t} \lp \int_0^t e^{k^2 s} \dif s \rp
    \sin(kx)  
    = \lim_{t \to \infty} \sum_{k=1}^{\infty} \frac{d_k}{k^2}(1 - e^{-k^2t}) \sin(kx) = \\
    = \sum_{k=1}^{\infty} \frac{d_k}{k^2} \sin(kx) := v(x).
\end{gather*}
\'Es a dir, tenim que $\lim_{t \to \infty} u(x, t)= v(x)$. Aquesta funció $v(x)$
satisfà
\[
    \begin{cases}
        v_{xx} + f(x) = 0 & x \in (0, \pi) \\
        v(0) = v(\pi) = 0
    \end{cases}
\]
Que anomenem el problema estacionari perquè \'es el problema restant si suposem
que la solució no depèn del temps ($u_t = 0$).
\section{Unicitat de la solució a \texorpdfstring{$\real^n$}{dimensions superiors}}
Demostrarem en aquesta secció la unicitat de la solució per a l'equació del calor en oberts
generals de $\real^n$ mitjançant el mètode integral.
\begin{defi}[domini]
    Un domini $\Omega \subset \real^n$ \'es un obert connex.
\end{defi}
\begin{prop}
    \label{prop:green}
    Si $\Omega$ \'es un domini fitat i Lipschitz,
    \[
        \int_{\Omega} (\Delta u) v = \int_{\partial \Omega} \frac{\partial u}{\partial \nu}
        v - \int_{\Omega} \nabla u \nabla v.
    \]
    Aquest resultat es coneix com identitat de Green. 
\end{prop}
\begin{proof}
    Usant integració per parts a $\real^n$ (\ref{col:int_parts}),
    \begin{gather*}
        \int_{\Omega} (\Delta u) v = \int_{\Omega} \sum_{i = 1}^n u_{x_ix_i} v = 
        \int_{\Omega} \sum_{i = 1}^n u_{x_i} v \nu^i - \int_{\Omega} \sum_{i=1}^n
        u_{x_i} v_{x_i} = \\ 
        = \int_{\partial \Omega} (\nabla u \cdot \nu)
        v - \int_{\Omega} \nabla u \nabla v =
        \int_{\partial \Omega} \frac{\partial u}{\partial \nu}
        v - \int_{\Omega} \nabla u \nabla v.
    \end{gather*}
\end{proof}
\begin{prop}
    Sigui $\Omega \subset \real^n$ un domini fitat i de classe $\C^1$ (Lipschitz seria
    suficient), \'es a dir, que tingui vora regular. L'equació de difusió amb
    condició de Dirichlet o Neumann a $\Omega$
    \[
        \begin{cases}
            u_t = \Delta u + f(t) \\
            u(x, t) = 0 & x \in \partial \Omega, \, t > 0 \\
            u(x, 0) = g(x) & x \in \Omega
        \end{cases}
    \]
    si t\'e solució $u \in \C^1\lp\overline{\Omega} \times \lb0, \infty\rp \rp$, llavors
    \'es única.
\end{prop}
\begin{proof}
    Considerem dues solucions $u_1, u_2$ i definim $v := u_1 - u_2$. Aleshores $v$ resol
    \[
        \begin{cases}
            v_t = \Delta v \\
            v|_{\partial \Omega} = 0 \\
            v(0) = 0
        \end{cases}
    \]
    Definim
    \[
        E(t) = \int_{\Omega} v^2(x, t) \dif x,
    \]
    que podem integrar per ser $\overline{\Omega}$ compacte. Aleshores, usant la identitat
    de Green (\ref{prop:green}),
    \[
        \frac{\dif}{\dif t} E(t) = \int_{\Omega} 2 v v_t \dif x = \int_{\Omega}
        2 v \Delta v \dif x = \int_{\partial \Omega} \frac{\partial v}{\partial \nu}
        v - \int_{\Omega} \nabla v \nabla v.
    \]
    El primer terme d'aquesta resta \'es nul perquè $v = 0$ si treballem amb Dirichlet
    i $\frac{\partial v}{\partial \nu} = 0$ per Neumann. Així doncs,
    \[
        \frac{\dif}{\dif t} E(t) = - \int_{\Omega} 2 |\nabla v|^2 \leq 0.
    \]
    Com $0 \leq E(t)$ i $E(0) = \int_{\Omega} v^2(x, 0) \dif x = 0$, aleshores $E(t) = 0$.
    Finalment,
    \[
        0 = E(t) = \int_{\Omega} v^2(x,t) \dif x \implies v = 0.
    \]
    \'Es a dir, $u_1 = u_2$.
\end{proof}
\begin{obs}
    La demostració funciona exactament igual si les condicions de vora no són homogènies.
\end{obs}
