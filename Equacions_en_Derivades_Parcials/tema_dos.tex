\chapter{Equacions de primer ordre}
\section{Equació lineal del transport}
En aquesta secció estudiarem l'equació lineal del transport. Sigui $u = u(x, t)$ 
la densitat o concentració d'una substància a temps $t$ en 
el punt $x \in \real$ en un tub (prou llarg) que
modelitzarem per $R$. Suposem que la substància corre a velocitat constant 
$c > 0$ en el tub o canal. La massa
total del fluid o substància en un interval $(a, b)$ ve donada per
\[
    \int_a^b u(x, t) \dif x.
\]
La variació temporal de la massa a l'interval $(a, b)$ \'es per tant
\[
    \frac{\dif}{\dif t} \int_a^b u(x, t) \dif x = c(u(a, t) - u(b, t)).
\]
On la darrera igualtat s'ha d'interpretar com un principi físic de conservació
de massa. \'Es a dir, el canvi de massa a l'interval es pot descriure en termes
del canvi de massa als seus extrems. Com això \'es cert per a tot interval $(a, b)$
es t\'e que
\[
    \int_a^b u_t(x, t) \dif x + c \lp u(b, t) - u(a, t) \rp = 0 \iff
    \int_a^b u_t(x, t) + c u_x(x, t) \dif x = 0. 
\]
Si dividim aquesta última igualtat per $b-a$ i fem tendir $b \to a$ obtenim, pel teorema
fonamental del càlcul,
\[
    0 = \lim_{b \to a} \frac{\int_a^b u_t(x, t) + c u_x(x, t) \dif x}{b-a} = 
    u_t(a, t) + c u_x(a, t).
\]
Això \'es cert per a tota $a$ i per tant obtenim l'equació del transport lineal
\[
    u_t + c u_x = 0.
\]

Per tant, podem formular ara el problema de Cauchy per l'equació lineal del transport.
\begin{defi}[equació!lineal del transport]
    \label{defi:Cauchy_Transport}
    Donada $g: \real \to \real$ la concentració a temps $t = 0$, el problema de Cauchy 
    per l'equació lineal del transport consisteix a trobar $u = u(x,t)$ tal que
    \[
        \begin{cases}
            u_t + cu_x = 0, & x \in \real, t \in \real \\
            u(x, 0) = g(x), & x \in \real
        \end{cases}
    \]
\end{defi}
\begin{prop}
    Donada $g \in \C^1(\real)$ existeix una única solució $u \in \C(\real \times \real)$.
    del problema de Caucy de l'equació de transport lineal (\ref{defi:Cauchy_Transport}). A
    m\'es a m\'es, la solució ve donada per $u(x, t) = g(x - ct)$. Les solucions d'aquesta
    forma s'anomenen ones viatgeres.
\end{prop}
\begin{proof}
    Considerem la derivada direccional de $u$ en la direcció $(c, 1)$. Tenim que aquesta
    \'es $(c, 1) \nabla u = cu_x + u_t = 0$. Per tant, la funció \'es constant sobre la
    recta que passa per $(x, t)$ i \'es paral·lela al vector $(c, 1)$. Volem intersecar
    aquesta recta amb la condició inicial $(t = 0)$. Per tant,
    parametritzant la recta per $(x, t) + s(c, 1) = (x + sc, t + s)$ ($s \in \real$),
    volem $t+s = 0$, \'es a dir, $u(x, t) = u(x - ct, 0) = g(x-ct)$. Per tant, tenim
    que $u(x, t) = g(x-ct)$ \'es una condició necessària per satisfer el problema de
    Cauchy però per la pròpia construcció de la solució ja satisfà l'equació en les
    derivades parcials.
\end{proof}

El problema es pot plantejar de forma anàloga per dimensions
superiors i modela, per exemple, un llac on hi ha un corrent a velocitat constant en
la direcció $\vec{c}$. S'escriu, per $g: \real^n \to \real$ donada,
\[
    \begin{cases}
        u_t + \vec{c} \nabla u = 0, & x \in \real^n, t \in \real \\
        u(x, 0) = g(x), & x \in \real^n
    \end{cases}
\]
\begin{prop}
    Donada $g \in \C^1(\real^n)$ existeix una única solució $u \in \C(\real^n \times \real)$
    del problema de Caucy de l'equació de transport lineal en dimensions superiors
    donada per $u(x, t) = g(x - ct)$. 
\end{prop}
\begin{proof}
    La demostració \'es anàloga a la del problema en una sola dimensió.
\end{proof}
\begin{defi}
    A l'equació lineal del transport, les rectes paral·leles al vector $(c, 1)$ 
    sobre cadascuna de les quals $u$ \'es constant es diuen rectes característiques.
\end{defi}

\section{Transport a velocitat variable}
Considerem ara que la velocitat pot dependre tant del punt $x$ com del temps $t$.
\'Es a dir, $c = c(x, t)$. Aquest problema modela, per exemple, aigua baixant per una
muntanya amb pendent no constant. Per formular el problema, procedim igual que quan
la velocitat \'es constant. \'Es a dir, igual que abans, per la conservació de massa,
\[
    \frac{\dif}{\dif t} \int_a^b u(x, t) \dif x = c(a, t)u(a, t) - c(b,t) u(b, t).
\]
I, per tant,
\[
    \int_a^b u_t(x, t) \dif x + \int_a^b \lp c(x, t)u(x,t)\rp_x \dif x = 0.
\]
Finalment,
\[
    0 = \lim_{b \to a} \frac{\int_a^b u_t(x, t) + \lp c(x, t)u \rp_x \dif x}{b-a} = 
    u_t + \lp c(x,t)u \rp_x = 0.
\]
D'on
\[
    u_t + c(x,t) u_x  + c_x(x, t)u= 0.
\]

Per resoldre aquest problema, podem utilitzar el que es coneix com a mètode de les
corbes característiques, que generalitza el procediment utilitzat per resoldre el
problema a velocitat constant. Considerem el camp vectorial $X(x, t) = (c(x, t), 1)$.
En primer lloc trobem les corbes integrals del camp $X$, \'es a dir, 
$(x(s), t(s))$ tal que $(x'(s), t'(s)) = X(x(s), t(s))$. Per fer això cal resoldre
el sistema d'EDOs
\[
    \begin{cases}
        x'(s) = c(x(s), t(s)) \\ 
        t'(s) = 1
    \end{cases}
\]
Com $t'(s) = 1$, tenim que $t = s$ i volem resoldre la EDO $x'(s) = c(x(s), s)$. Aquestes
solucions s'anomenen corbes característiques. Si restringim $u(x, t)$ sobre una
corba característica obtenim $v(s) := u(x(s), t(s))$ amb derivada
\[
    v'(s) = u_x(x(s), t(s)) x'(s) + u_t(x(s), t(s))t'(s).
\]
Que, aplicant la EDO pel camp $X$, \'es igual a
\begin{gather*}
    u_x(x(s), t(s)) c(x(s), t(s)) + u_t(x(s), t(s)) \stackrel{\text{EDP}}{=} -c_x
    (x(s), t(s)) u(x(s), t(s)) = \\
    -c_x (x(s), t(s)) v(s) =: -\alpha(s) v(s).
\end{gather*}
Per tant, $v'(s) = -\alpha(s) v(s)$ i, resolent aquesta equació diferencial,
\[
    v(s) = \exp \lp C - \int_0^s \alpha(z) \dif z \rp.
\]
Si avaluem en un instant $s_1$ tal que $t(s_1) = 0$, com ja coneixem $g$ podem
determinar la constant. Finalment, avaluem l'expressió a $s_2$ tal que $x(s_2) = x$
i $t(s_2) = t$ per obtenir el valor a $(x, t)$.

M\'es generalment, describim ara el mètode de les corbes caràcterístiques per 
resoldre l'equació lineal de primer ordre
\[
    a(x, t)u_t + b(x, t) u_x + d(x, t) u + f(x, t) = 0
\]
amb $x, t \in \real$. El procediment \'es fonamentalment igual al que acabem d'utilitzar.
\begin{enumerate}
    \item Considerem el camp $X(x, t) = (b(x, t), a(x, t))$. En trobem les seves
        corbes integrals resolent la EDO corresponent:
        \[
            \begin{pmatrix}
                x'(s)\\
                t'(s)
            \end{pmatrix}
            =
            \begin{pmatrix}
                b(x(s), t(s))\\
                a(x(s), t(s))
            \end{pmatrix}
        \]
    \item Restringim la funció $u$ sobre una corba integral del camp $X$ parametritzada
        per $(x(s), t(s))$:
        \[
            v(s) := u(x(s), t(s)).
        \]
        Aleshores
        \[
            v'(s) = u_x x'(s) + u_t t'(s) = bu_x + a u_t = -f(x(s), t(s)) - d(x(s), t(s)) v(s).
        \]
        Finalment, resolem aquesta última EDO i en determinem les constants additives gràcies
        a la condició inicial.
\end{enumerate}
\begin{obs}
    En lloc de condició inicial a vegades coneixerem la solució a una corba diferent de
    $t = 0$. El procediment \'es anàleg.
\end{obs}

Estudiarem ara una proposició que físicament es pot interpretar com el fet que la massa
es conserva quan es veu sotmesa al transport a velocitat variable (haurem de suposar
que la massa inicialment estava concentrada en una regió fitada).
\begin{prop}
    Sigui $u \in \C^1(\real \times \real)$ solució de $u_t + (c(x, t)u)_x = 0$. Suposem
    que $u \geq 0$ i que $u(x, 0)$ t\'e suport compacte. Aleshores per a tota $t \in \real$
    \[
        \int_{\real} u(x, t) \dif x = \int_{\real} u(x, 0) \dif x
    \]
    i el suport de $u(\cdot, t)$ \'es compacte.
\end{prop}
\begin{obs}
    Pel cas $c$ constant, com disposem d'una solució explícita, la proposició es pot
    verificar fàcilment. Però pel cas general no disposem d'una fórmula explícita i
    caldrà una demostració m\'es potent.
\end{obs}
\begin{proof}
    %TODO dibuixet per poder entendre aquesta demo
    En primer lloc, cal veure que pel mètode de les característiques podem assegurar
    que $u(\cdot, T)$ t\'e suport compacte. Sigui $[-R, R]$ tal que $\operatorname{supp}
    u(\cdot, 0) \subset [-R, R]$. Considerem $\gamma_1$ la corba característica que
    passa per $(R, 0)$ i $\gamma_2$ la que passa per $(-R, 0)$. Considerem $x_1$ la
    coordenada $x$ del punt de tall de la recta $t = T$ amb $\gamma_1$ i $x_2$ la del punt de
    tall amb $\gamma_2$. Aleshores, com les corbes característiques no es creuen i
    la funció $u$ \'es constant sobre les corbes característiques, $\operatorname{supp}
    u(\cdot, T) \subset [x_1, x_2]$. \'Es a dir, tamb\'e t\'e suport compacte. Aleshores
    \[
        \frac{\dif}{\dif t} \int_{\real} u(x, t) \dif x = \int_{\real} u_t(x, t) \dif x
        = -c \int_{\real} u_x(x, t) \dif x = \lim_{R \to \infty} -c (u(R, t) - u(-R, t)) = 0.
    \]
    On hem pogut derivar sota signe d'integral perquè la derivada \'es contínua i tamb\'e
    t\'e suport compacte com a conseqüència de la demostració anterior, i per tant
    el seu valor absolut t\'e integral finita.
\end{proof}

\section{Equació de transport no homogeni}
Considerem ara el problema del transport no homogeni, \'es a dir, l'equació
\begin{equation}
    \label{eq:transport_no_homogeni}
    \begin{cases}
        u_t + c \nabla u = f(x, t) & x \in \real^n,\, t \in \real \\
        u(x, 0) = g(x) & x \in \real^n
    \end{cases}
\end{equation}
Per resoldre aquesta equació serà útil introduir el concepte de semigrup de l'equació
homogènia.
\begin{defi}[semigrup de l'equació homogènia]
    En general, el semigrup de l'equació homogènia trasllada una funció presa com
    a condició inicial d'una EDP durant un temps $t$. En el cas particular de l'equació
    del transport, fixat un temps $t$, es defineix el semigrup com
    \[
        \begin{aligned}
            T_t\colon \C^1(\real^n) &\to \C^1 (\real^n) \\
            g & \mapsto v(\cdot, t) = g(\cdot - ct)
        \end{aligned}
    \]
\end{defi}
Notem que $T_t$ \'es un endomorfisme lineal.
\begin{prop}
    Per a tot $t, s \in \real$ tenim que $T_s \circ T_t = T_{t+s}$.
\end{prop}
\begin{proof}
    En donarem dues proves. La primera \'es específica del problema de transport.
    Aplicant la solució explícita del problema,
    \[
        (T_s(T_t(g)))(x) = (T_t g)(x-cs) = g((x-cs) - ct) = g(x - c(t +s)) = T_{t+s}(g)(x).
    \]
    La segona \'es m\'es general. Per exemple, tamb\'e serveix quan la velocitat no \'es
    constant. Suposem que $T_t$ \'es el semigrup del problema
    \[
        u_t + (c(x)u)_x = 0.
    \]
    Cal remarcar que en aquest cas $c$ no depèn de $t$. Aleshores, com l'equació \'es
    autònoma, per la unicitat de les solucions que ja hem demostrat tenim 
    automàticament que $T_s \circ T_t = T_{t+s}$. Hi ha una única solució que porta $g$
    a $T_t g$, i, com l'equació \'es autònoma, començar a temps $t$ amb condició inicial
    $g$ i traslladar-se un temps $s$ \'es equivalent a començar a temps $0$ amb
    condició $T_t g$ i traslladar-se un temps $s$.
\end{proof}
\begin{obs}
    El mateix raonament serveix per EDOs i EDPs no lineals i autònomes, donat un teorema
    d'existència i unicitat. Per exemple, podem comprovar que la propietat de semigrup
    es satisfà per l'equació
    \[
        u_t = u^2.
    \]
    En aquest cas, per separació de variables la solució ve donada per $u = -\frac{1}{t+c}$.
    Si $u(0) = g$, aleshores $c = -\frac{1}{g}$ i $u(t) = \frac{g}{1-gt}$. Aleshores, el
    semigrup a temps $t$ serà
    \[
        T_t g = \frac{g}{1-gt}.
    \]
    I aquest satisfà la propietat de semigrup:
    \[
        T_s(T_t g) = \frac{T_t g}{1 - s T_t g} = \frac{\frac{g}{1- gt}}{1 - \frac{sg}{1-gt}}
        = \frac{g}{1 - g(t+s)} = T_{t+s} (g).
    \]
\end{obs}
%TODO revisar ortografia (dièresis)
Vegem ara com utilitzar el semigrup per resoldre l'equació de transport no homogeni
(\ref{eq:transport_no_homogeni}). En primer lloc intentarem trobar la solució de
manera intuitiva, partint del problema que estem modelitzant. En aquest cas, el 
problema homogeni modela un fluid que es transporta a velocitat 
constant. El terme no homogeni es pot interpretar com una font
de fluid. Aleshores, la quantitat de fluid total a $(x, t)$ serà la suma del que
prov\'e de la condició inicial, que donarà un terme $(g(x-\vec{c}t))$, i la
contribució de les fonts desplaçada a la velocitat corresponent. Aquest últim
terme es correspon a integrar al llarg del temps la font a la posició adequada.
Això ens permet introduir el que es coneix com la fórmula de Duhamel.
%TODO afegir demo més o menys genèrica de Duhamel feta al tema de calor (?)
\begin{defi}[fórmula!de Duhamel]
    \label{defi:Duhamel}
    Per al problema del transport no homogeni, la fórmula de Duhamel \'es
    \[
        u(x, t) = g(x - \vec{c}t) + \int_0^t f(x- \vec{c}(t-s), s) \dif s.
    \]
    O, m\'es generalment, en termes del semigrup (i aplicable a altres problemes)
    \[
        u(x, t) = (T_t g)(x) + \int_0^t \lp T_{t-s} f(\cdot, s) \rp(x) \dif s.
    \]
\end{defi}
\begin{prop}
    Donades $g, f \in \C^1 \lp \real^n \times \real \rp$ existeix una única
    solució $u = u(x, t)$ de classe $\C^1 \lp \real^n \times \real \rp$ que
    resol el problema de Cauchy del transport no homogeni 
    (\ref{eq:transport_no_homogeni}). A m\'es a m\'es, la solució ve donada per
    la fórmula de Duhamel (\ref{defi:Duhamel}).
\end{prop}
\begin{proof}
    Vegem en primer lloc la unicitat. Siguin $u, \tilde{u}$ dues solucions. 
    Aleshores $v = u - \tilde{u}$ \'es solució del problema
    \[
        \begin{cases}
            v_t + \vec{c} \nabla v = 0 \\
            v(x, 0) = 0
        \end{cases}
    \]
    que ja hem demostrat que implica que $v = 0$ pel mètode de les característiques.

    Vegem ara l'existència de la solució pel mètode de les característiques.
    Prenem, com en el cas del problema homogeni, la recta $(x + \tilde{s}\vec{c}, t +
    \vec{s})$ amb paràmetre $\tilde{s}$. Aleshores, si definim $w(\tilde{s}) = 
    u(x+\tilde{s} \vec{c}, t + \tilde{s})$, es t\'e que
    \[
        w'(\tilde{s}) = \vec{c} \nabla u(x+\tilde{s} \vec{c}, t + \tilde{s}) +
        u_t(x + \tilde{s} \vec{c}, t + \tilde{s}) = f(x + \tilde{s} \vec{c},
        t + \tilde{s}).
    \]
    Per tant,
    \begin{gather*}
        u(x, t) = w(0) = w(-t) + \int_{-t}^0 w'(\tilde{s}) \dif \tilde{s} = \\
        = u(x - \vec{c}t, 0) + \int^0_{-t} f(x+\tilde{s} \vec{c}, t + \tilde{s}) \dif
        \tilde{s} \stackrel{t+\tilde{s} = s}{=} g(x - \vec{c}t) + \int_0^t f(x
        - (t-s)\vec{c}, s) \dif s.
    \end{gather*}
    Com ja s'ha comentat anteriorment, la construcció amb el mètode de les
    corbes característiques ja assegura que es satisfaci l'equació. Alternativament,
    tamb\'e es podria demostrar l'existència derivant directament la fórmula
    de Duhamel (\ref{defi:Duhamel}).
\end{proof}
\begin{obs}
    En equacions diferencials, s'ent\'en habitualment per un problema ben posat
    (\emph{well-posed problem}) un problema les solucions del qual satisfan:
    \begin{itemize}
        \item Existència.
        \item Unicitat.
        \item Estabilitat: es poden obtenir solucions a temps $t$ arbitràriament
            properes si es prenen condicions inicials suficientment properes.
            Altrament dit, el semigrup $T_t \colon g \mapsto u(\cdot, t)$ \'es
            continu.
    \end{itemize}
\end{obs}
\begin{obs}
    A $R^k$ totes les normes són equivalents i per tant la tria de norma no \'es
    rellevant quan estudiem l'estabilitat d'una EDO. Ara b\'e, en espais de
    Banach arbitraris no totes les normes són equivalents, i per tant la tria
    de norma \'es rellevant i serà diferent segons la EDP estudiada.
\end{obs}
\begin{prop}
    Donat $t \in \real$, el semigrup 
    \[
        \begin{aligned}
            T_t \colon \C^1(\real^n) \times \C^1(\real^n \times \real) 
            &\to \C^1(\real^n)\\
            (g, f) &\mapsto u(\cdot, t)
        \end{aligned}
    \]
    de l'equació del transport no homogeni \'es lineal i continu amb les normes
    dels espais de Banach respectius.
\end{prop}
\begin{proof}
	La linealitat \'es directa. Vegem la continuïtat. Sigui $u(\cdot, t)$ una solució de l'equació. Aleshores
	\begin{gather*}
		||u(\cdot, t)||_{\C^0_b(\real^n)} = ||u(\cdot, t)||_{L^{\infty}(\real^n)}
		= \left \lVert g(\cdot - \vec{c}t) + \int_0^t f(\cdot - \vec{c}(t-s),s) 
		\dif s \right \rVert_{L^{\infty}(\real^n)} \leq \\
		\leq ||g||_{\C^0_b(\real^n)} + t ||f||_{\C^0_b(\real^n \times \real)}.
	\end{gather*}
	Aleshores, per la linealitat de l'equació,
	\[
		\|u - u_{\text{ap}}||_{L^{\infty}} \leq ||g-g_{\text{ap}}||_{L^{\infty}}
		+ t \|f- f_{\text{ap}}\|_{\C^0_b(\real^n \times \real)}.
	\]
	Si imposem $||g - g_{\text{ap}}|| < \frac{\varepsilon}{2}$ i 
	$t \|f- f_{\text{ap}}\|_{\C^0_b(\real^n \times \real)} < \frac{\varepsilon}{2}$,
	aleshores $\|u - u_{\text{ap}}||_{L^{\infty}} < \varepsilon$.
\end{proof}
\section{Solucions generalitzades}
Fins ara hem considerat solucions de classe $\C^1$ per una EDP d'ordre 1. Aquestes s'anomenen
solucions clàssiques. Però tamb\'e es pot admetre un concepte m\'es general de solució,
anomenat solució generalitzada o solució en sentit feble. Vegem un exemple on apareix aquest
tipus de solució
\begin{example}
    Considerem el problema
    \[
        \begin{cases}
            u_t + c \nabla u = 0 & x \in \real, t \in \real \\
            u(x, 0) = g(x) & x \in \real
        \end{cases}
    \]
    amb condició inicial $g(x) =  
    \begin{cases}
        1 & \text{si } x < 0 \\
        0 & \text{si } x > 0
    \end{cases}$. 
    Tenim un candidat a solució $u(x, t) = g(x-ct) =   
    \begin{cases}
        1 & \text{si } x-ct < 0 \\
        0 & \text{si } x-ct > 0
    \end{cases}$,
    però aquest no \'es $\C^1$ sobre la recta $x = ct$.
\end{example}
\begin{defi}[solució generalitzada de l'equació del transport]
    Sigui $\Omega \subset \real \times \real$, $(x, t) \in \Omega$ i $u \colon \Omega
    \to \real$, $u = u(x, t)$ possiblement discontínua. Diem que $u$ \'es una solució
    generalitzada de $u_t + cu_x = 0$ a $\Omega$ si per a tot $[a, b]$ tal que
    $[a, b] \times \{t\} \subset \Omega$ es t\'e que $\int_a^b u(x, t) \dif x$
    \'es derivable quasi per a tot temps $t$ i
    \[
        \frac{\dif}{\dif t} \int_a^b u(x, t) \dif x = -c u(b, t) + cu(a, t)
    \]
    si $(a, t)$ i $(b, t)$ són punts de continuïtat de $u$.
\end{defi}
\begin{obs}
    Amb aquesta definició la solució proposada a l'exemple anterior \'es una solució 
    generalitzada quan $c = 1$.
    Si l'interval $[a, b]$ es troba completament a un costat de la recta $x = t$, la igualtat
    buscada \'es directa. Si l'interval $[a, b]$ talla la recta al punt $c$, aleshores
    \[
        \int_a^b u(x, t) \dif x = c-a
    \]
    i
    \[
        \int_a^b u(x, t+h) \dif x = c-a+h.
    \]
    I, per tant,
    \[
        \frac{\dif}{\dif t} \int_a^b u(x, t) = 1 = -c u(b, t) + cu(a, t).
    \]
\end{obs}










