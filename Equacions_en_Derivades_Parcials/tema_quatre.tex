\chapter{L'equació d'ones}
\section{Modelització}
Considerem una corda vibrant infinita disposada sobre l'eix $x$, i amb posició
vertical $u(x,t)$ en el temps $t$ en la posició horitzontal $x$. Per a modelar
el problema, assumirem que la corda és flexible i elàstica, i que les vibracions
són petites (ja que sinó la EDP a resoldre seria no lineal).

Aplicarem la segona llei de Newton. D'una banda, si $\rho(y)$ és la densitat
de massa en el punt $y$, la força que actua sobre el punt $y$ a temps $t$ és:
\[
\rho(y) u_{tt} (y,t).
\]
D'altra banda, hi ha una força de tensió que actua sobre la corda. Si considerem
l'interval $(x,x+h)$, per a $h$ prou petit, podem considerar que la tensió serà
proporcional a la diferència de derivades als extrems de l'interval (la corda estarà
més doblegada com major sigui la diferència de derivades).
\begin{center}
	\begin{tikzpicture}
		\begin{axis}[
				xmin = 0,
				xmax = 2,
				ymin = -0.5,
				ymax = 5.5,
				samples = 100,
				axis lines = center,
				x tick label style={major tick length=0pt},
				xticklabels={},
				yticklabels={}
		]
			\addplot[domain=0.25:1.75, thick]{5 - 1/x} node[below,pos=0.75] {$\quad\,\, u(y,t)$};
			\addplot[mark=*,color=black] coordinates {(0.5, 0)} node[below]{$x$};
			\addplot[mark=*,color=black] coordinates {(0.5, 3)} node[]{};
			\addplot[mark=*,color=black] coordinates {(1.5, 0)} node[below]{$x+h$};
			\addplot[mark=*,color=black] coordinates {(1.5, 4.333333333)} node[]{};
			\addplot[domain=0.3:0.7, color=red,thick]{4*(x-0.5) + 3};
			\addplot[domain=1.2:1.8, color=red,thick]{0.44444444*(x-1.5) + 4.333333333};
			\draw (axis cs:0.5, 0) [color=gray, dashed] -- (axis cs:0.5, 3);
			\draw (axis cs:1.5, 0) [color=gray, dashed] -- (axis cs:1.5, 4.3333333333);
		\end{axis}
	\end{tikzpicture}
\end{center}
En altres paraules, la tensió total a temps $t$ en l'interval $(x,x+h)$ serà:
\[
\tau(u_x (x+h,t) - u_x(x,t)),
\]
sent $\tau$ una constant positiva.

Si, a més, considerem l'acció de forces externes $f(y,t)$, la segona llei de Newton
ens diu que:
\[
\int_{x}^{x+h} \rho(y) u_{tt} (y,t) \dif y =  \tau(u_x (x+h,t) - u_x(x,t)) + 
\int_{x}^{x+h} f(y,t) \dif y.
\]
Per tant, per a tot interval $(x,x+h)$ i tot temps $t$ es compleix
\[
\int_{x}^{x+h} \lp \rho(y) u_{tt} (y,t) - \tau u_{xx}(y,t) - f(y,t) \rp \dif y = 0.
\]
Dividint per $h$ i aplicant el teorema fonamental del càlcul, arribem a:
\[
0 = \lim_{h \to 0} \frac{\int_{x}^{x+h} \rho(y) u_{tt} (y,t) - \tau u_{xx}(y,t) - f(y,t) \dif y}{h} = 
\rho(x) u_{tt}(x,t) - \tau u_{xx}(x,t) - f(x,t).
\]
De moment, considerem que la densitat de massa $\rho(y)$ és constant. Definint
\[
\frac{\tau}{\rho} = c^2,
\]
arribem a l'equació
\[
u_{tt} - c^2 u_{xx} = f(x,t),
\]
que és l'equació d'ones desitjada.
\begin{defi}[equació!d'ones]
	\label{defi:Eq_Ones}
	El problema de Cauchy de l'equació d'ones consisteix a trobar $u = u(x,t)$ tal que:
	\begin{equation}
        \begin{cases}
            u_{tt} - c^2 u_{xx} = f(x,t), & x \in \real, \, t \in \real \\
            u(x, 0) = g(x), & x \in \real \\
            u_t (x,0) = h(x), & x \in \real
        \end{cases}
    \end{equation}
    donats $g \in \C^2 (\real)$ (posició inicial) i $h \in\C^1 (\real)$
    (velocitat inicial). La constant $c > 0$ és anomenada la velocitat de les ones.
\end{defi}
\section{Equació d'ones homogènia}
\begin{prop}
	L'equació d'ones homogènia
	\begin{equation}
		\label{eq:homo_ones}
        \begin{cases}
            u_{tt} - c^2 u_{xx} = 0, & x \in \real, t \in \real \\
            u(x, 0) = g(x), & x \in \real \\
            u_t (x,0) = h(x), & x \in \real
        \end{cases}
    \end{equation}
    admet una única solució clàssica $u \in \C^2 (\real \times \real)$, amb la següent expressió:
    \[
		u(x,t) = \frac{1}{2} \lp g(x-ct) + g(x+ct) \rp + \frac{1}{2c}
		\int_{x-ct}^{x+ct} h(y) \dif y
	\]
\end{prop}
\begin{proof}
	Escrivim l'equació homogènia de la següent forma:
	\[
		0 = u_{tt} - c^2 u_{xx} = (\partial_t^2 - c^2 \partial_x^2) u.
	\]
	Inspirant-nos en la diferència de quadrats, comprovem que:
	\[
		(\partial_t + c \partial_x)(\partial_t - c \partial_x) u = 
		(\partial_{tt} - c \partial_t \partial_x + c \partial_t \partial_x - c^2 \partial_{xx}) u =
		(\partial_{tt} - c^2 \partial_{xx}) u,
	\]
	on hem usat el lema de Schwarz ($u_{xt} = u_{tx}$), ja que $u \in \C^2 (\real \times \real)$.
	Així doncs, podem escriure l'equació homogènia com un problema de transport:
	\[
		v = u_t - c u_x \implies (\partial_t + c\partial_x) v = 0.
	\]
	La condició inicial d'aquest problema de transport és:
	\[
		v(x,0) = (u_t - c u_x)(x,0) = h(x) - c g'(x).
	\]
	En conseqüència,
	\[
		v(x,t) = (h-cg')(x-ct).
	\]
	D'aquesta manera, hem de resoldre una altra equació de transport:
	\[
		\begin{cases}
			u_t - c u_x = (h-cg')(x-ct) \\
			u(x,0) = g(x)
		\end{cases}
	\]
	Aquest problema no homogeni es pot resoldre amb la fórmula de Duhamel 
	per a l'equació de transport (\ref{defi:Duhamel}):
	\begin{gather*}
		u(x,t) = g(x+ct) + \int_{0}^t (h-cg')(x+c(t-s)-cs) \dif s = \\
		= g(x+ct) + \int_0^t h(x-2cs+ct) \dif s - c \int_0^t g'(x-2cs+ct) \dif s.
	\end{gather*}
	Fent el canvi $y = x-2cs+ct$, obtenim:
	\[
		u(x,t) = g(x+ct) - \frac{1}{2c} \int_{x+ct}^{x-ct} h(y) \dif y + \frac{1}{2}
		\int_{x+ct}^{x-ct} g'(y) \dif y.
	\]
	Aplicant el teorema fonamental del càlcul a l'última integral, arribem a l'expressió desitjada:
	\[
		u(x,t) = \frac{1}{2} \lp g(x-ct) + g(x+ct) \rp + \frac{1}{2c} \int_{x-ct}^{x+ct} h(y) \dif y. 
	\]
	Es deixa com a exercici comprovar que l'expressió anterior realment és solució de l'EDP.
	La unicitat de solucions és conseqüència directa de l'aplicació del mètode de les característiques.
\end{proof}
\begin{defi}[fórmula!de d'Alembert]
	La fórmula de d'Alembert és
	\begin{equation}
		\label{eq:FdAlembert}
		u(x,t) = \frac{1}{2} \lp g(x-ct) + g(x+ct) \rp + \frac{1}{2c}
		\int_{x-ct}^{x+ct} h(y) \dif y
    \end{equation}
\end{defi}
\begin{obs}
	Sigui $u$ solució de \eqref{eq:homo_ones}. Si, a més, $g \in \C_{b}^2 (\real)$ i
	$h \in \C_{b}^1 (\real)$, llavors, donat un temps $t$, es compleix 
	$u(\cdot,t) \in \C_{b}^2 (\real)$ i:
	\[
		||u(\cdot,t)||_{\infty} \leq ||g||_{\infty} + t ||h||_{\infty}
	\]
	La demostració és immediata a partir de la fórmula de d'Alembert \eqref{eq:FdAlembert}.
\end{obs}
\begin{ej}
	Trobeu una fórmula per a $u_t$. A continuació, fiteu $||u_t||_{\infty}$ i
	vegeu $u_t \in \C_b^1$.
\end{ej}

\begin{obs}
	El semigrup solució està ben definit i és un endomorfisme continu:
	\[
	\begin{aligned}
	  T_t \colon \C_{b}^2 (\real) \times \C_{b}^1(\real) &\to 
	  \C_{b}^2(\real) \times \C_{b}^1(\real)\\
	  \begin{pmatrix}
            g \\
            h
      \end{pmatrix}
         &\mapsto 
      \begin{pmatrix}
            u(\cdot,t) \\
            u_t (\cdot,t)
      \end{pmatrix}
	\end{aligned}
	\]
	Serà útil més endavant per a la resolució de problemes no lineals.
\end{obs}
Existeix una deducció alternativa de la fórmula de d'Alembert, que es detalla a continuació.
Considerem el problema homogeni \eqref{eq:homo_ones} amb $c=1$:
\[
u_{tt} = u_{xx}.
\]
Hi ha un canvi de variables que simplifica l'EDP. Considerem:
\[
	\begin{cases}
		y = t+x; \\
		z = t-x
	\end{cases} \iff
	\begin{cases}
		t = \frac{1}{2}(y+z) \\
		x = \frac{1}{2}(y-z)
	\end{cases}
\]
Per la regla de la cadena, observem que:
\[
	u_t = \frac{\partial y}{\partial t} u_y + \frac{\partial z}{\partial t} u_z = u_y + u_z
\]
\[
	u_x = u_y - u_z.
\]
Així, les segones derivades queden:
\[
	u_{tt} = (u_t)_t = (u_t)_y + (u_t)_z = (u_y + u_z)_y + (u_y + u_z)_z = u_{yy} + 2 u_{yz} + u_{zz}
\]
\[
	u_{xx} = (u_x)_x = (u_x)_y - (u_x)_y = (u_y - u_z)_y - (u_y - u_z)_z = u_{yy} - 2 u_{yz} + u_{zz}.
\]
L'EDP resultant és
\[
	0 = u_{tt} - u_{xx} = 4u_{yz} \implies u_{yz} = 0.
\]
L'expressió obtinguda és tan simple que podem ``integrar-la'' i trobar una solució general:
\[
	u_{yz} = 0 \implies v_z = 0, \, v = v(y,z) \iff v = v(y) \implies u_y = v(y).
\]
D'aquesta manera:
\[
	u(y,z) = \int_0^y v(s) \dif s + c(z) = G(y) + F(z),
\]
amb $F,G$ arbitràries a trobar segons les condicions inicials. Desfent el canvi, 
\[
	u(x,t) = F(x-t) + G(x+t),
\]
o, per a $c$ arbitrària:
\begin{equation}
	\label{eq:ones_fg}
	u(x,t) = F(x-ct) + G(x+ct).
\end{equation}
Imposant les condicions inicials de l'equació \eqref{eq:homo_ones}, veiem:
\[
	\begin{cases}
		g(x) = F(x) + G(x) \\
		h(x) = -cF'(x) + cG'(x)
	\end{cases}
\]
Derivant la primera equació deduïm que
\[
	g'(x) = F'(x) + G'(x)
\]
Fent les combinacions lineals adequades amb el sistema, obtenim:
\[
	\begin{cases}
		cg'(x) -h(x) = 2cF'(x) \\
		cg'(x) +h(x) = 2cG'(x)
	\end{cases}
\]
Integrant ambdues equacions podem obtenir expressions per a $F,G$:
\begin{gather*}
	F(x) = \frac{1}{2c} \lp c g(x) - \int_0^{x} h(y) \dif y + A \rp \\
	G(x) = \frac{1}{2c} \lp c g(x) + \int_0^{x} h(y) \dif y + B \rp,
\end{gather*}
per a unes certes constants $A,B$. Finalment, l'expressió de la solució general queda:
\[
	u(x,t) = F(x-ct) + G(x+ct) = \frac{1}{2} \lp g(x-ct) + g(x+ct) \rp + \frac{1}{2c}
	\int_{x-ct}^{x+ct} h(y) dy + A+B,
\]
d'on deduïm que $A + B = 0$. 

Una interpretació que es pot fer de \eqref{eq:ones_fg} és que  $u$ es pot expressar 
com la superposició de dues ones viatgeres $F$ i $G$, que es desplacen a una velocitat $c$
en direccions oposades. A continuació, es mostra un exemple on s'observa aquest comportament.
\begin{example}
Considerem una corda de guitarra sobre l'eix $x$, amb posició inicial $g(x)$ i
velocitat inicial zero. Si la deixem anar, aleshores la posició vertical de la corda
ve donada per l'expressió:
\[
	u(x,t) = \frac{1}{2} (g(x-ct) + g(x+ct)),
\]
És a dir, el resultat són dues ones viatgeres iguals a la inicial però de la meitat d'amplitud.
\begin{center}
	\begin{tikzpicture}
		\begin{axis}[
				xmin = -7,
				xmax = 7,
				ymin = -0.10,
				ymax = 1.25,
				samples = 100,
				axis lines = center,
				x tick label style={major tick length=0pt},
				xticklabels={},
				yticklabels={}
		]
			\addplot[domain=-2:2, thick]{1*cos(45*x)} node[above,pos=0.57] {$\quad\,\, g(x)$};
			\addplot[domain=-5:-1, color=red,thick]{0.5*cos(45*(x+3))} node[above,pos=0.42] {$\frac{1}{2}g(x+ct)$};
			\addplot[domain=1:5, color=red,thick]{0.5*cos(45*(x-3))} node[above,pos=0.58] {$\frac{1}{2}g(x-ct)$};
		\end{axis}
	\end{tikzpicture}
\end{center}
\end{example}
\begin{obs}
	Una conseqüència interessant de \eqref{eq:ones_fg} és la següent: Considerem els punts $A$, $B$, $C$ i $D$
	sobre el para\lgem elogram de la figura a continuació. Aleshores, és immediat veure que:
	\[
	u(A) + u(D) = u(B) + u(C)
	\]
	%TODO: Fer que aquest dibuixet quedi més bonic
	\begin{center}
		\begin{tikzpicture}
			\begin{axis}[
					xmin = 0,
					xmax = 14,
					ymin = 0,
					ymax = 10,
					samples = 100,
					axis lines = center,
					x tick label style={major tick length=0pt},
					ylabel={$t$},
					xlabel={$x$},
					xticklabels={},
					yticklabels={}
			]
				\addplot[domain=2.5:8.5]{1.5*(x-2)-1} node[above, sloped,pos=0.22] {$x-ct = c_3$};
				\addplot[domain=0:10.7]{1.5*(x-2)-4.5} node[below,sloped,pos=0.92] {$x-ct = c_4$};
				\addplot[domain=2.8:11]{-1.5*(x-8)} node[above, sloped, pos=0.11] {$x+ct = c_1 $};
				\addplot[domain=5.8:12]{-1.5*(x-11.5)} node[above,sloped,pos=0.7] {$\;x+ct = c_2$};
				\addplot[mark=*,color=black] coordinates {(16/3, 4)} node[left]{$C$};
				\addplot[mark=*,color=black] coordinates {(85/12, 53/8)} node[above = 2mm]{$D$};
				\addplot[mark=*,color=black] coordinates {(33/4, 39/8)} node[right]{$B$};
				\addplot[mark=*,color=black] coordinates {(13/2, 9/4)} node[below=2mm]{$A$};
			\end{axis}
		\end{tikzpicture}
	\end{center}
\end{obs}

La fórmula de d'Alembert ens diu que el valor de $u$ a la posició $(x,t)$ només depèn de les condicions
inicials a l'interval $[x-ct,x+ct]$. Recíprocament, els valors de la posició i velocitat inicials en un
punt $x_0$ afectaran tot el sector $x_0 -ct \leq x \leq x_0 + ct$, donat un temps $t$. Aquests fets
se sintetitzen en les següents definicions:
\begin{defi}[domini de dependència]
	El domini de dependència de $(x,t)$ del punt $x$ respecte el temps $t$ és l'interval $[x-ct,x+ct]$.
\end{defi}
\begin{defi}[rang d'influència]
	El rang d'influència a temps $t$ del punt $x_0$ és el sector $x_0 -ct \leq x \leq x_0 + ct$.
\end{defi}

\section{Equació d'ones no homogènia}

\begin{prop}
	Donades $g \in \C^2 (\real)$, $h\in \C^1 (\real)$, $f \in \C^1 (\real \times \real)$,
	l'EDP no homogènia
	\begin{equation}
		\label{eq:no_homo_ones}
        \begin{cases}
            u_{tt} - c^2 u_{xx} = f(x,t), & x \in \real, t \in \real \\
            u(x, 0) = g(x), & x \in \real \\
            u_t (x,0) = h(x), & x \in \real
        \end{cases}
    \end{equation}
    admet una única solució de classe $\C^2 (\real)$, i ve donada per:
    \[
		u(x,t) = \frac{1}{2} \lp g(x-ct) + g(x+ct) \rp + \frac{1}{2c}\int_{x-ct}^{x+ct} h(y) \dif y
		+ \frac{1}{2c} \iint_{T(x,t)} f,
    \]
    sent $T(x,t) = \{ (y,s) \in \real^2; \; 0 < s < t, \, x-c(t-s) < y < x + c(t-s)\}$ el triangle característic
    amb vèrtex superior $(x,t)$.
\end{prop}
\begin{proof}
	Siguin $v,w$ respectivament les solucions de les següents EDPs:
	\[
		\begin{cases}
            v_{tt} - c^2 v_{xx} = 0, & x \in \real, t \in \real \\
            v(x, 0) = g(x), & x \in \real \\
            v_t (x,0) = h(x), & x \in \real
        \end{cases}
        \text{;}
        \begin{cases}
            w_{tt} - c^2 w_{xx} = f(x,t), & x \in \real, t \in \real \\
            w(x, 0) = 0, & x \in \real \\
            w_t (x,0) = 0, & x \in \real
        \end{cases}
	\]
	Aleshores, si $u$ és solució de l'EDP no homogènia \eqref{eq:no_homo_ones}, es complirà que $u = v+w$.
	Observem que $v$ satisfà una EDP homogènia, i per tant la seva expressió ve donada per la fórmula
	de d'Alembert \eqref{eq:FdAlembert}. Per tant, només ens cal trobar l'expressió de $w$.
	La idea és utiltzar la fórmula de Duhamel:
	\[
		w(x,t) = \int_{0}^{t} T_{t-s} f(x,s) \dif s.
	\]
	No obstant, recordem que per a l'equació d'ones necessitem dues condicions inicials: $g$ i $h$.
	Per tant, tenim dues opcions per a $T_{t-s} f$: o posem $f$ com a posició inicial o com a velocitat inicial.
	Es pot comprovar que l'opció que funciona és la segona. En efecte, considerem la següent EDP, per a una $s$ fixada:
	\[
		\begin{cases}
            \lp \varphi_{tt} - c^2 \varphi_{xx} \rp (x,t;s)= 0, & x \in \real, t > s \\
            \varphi(x,s;s) = 0, & x \in \real \\
            \varphi_t (x,s;s) = f(x,s), & x \in \real
        \end{cases}
	\]
	És una equació homogènia, i per tant podem aplicar la fórmula de d'Alembert (aplicant
	una certa translació temporal, que podem fer perquè la EDP és autònoma):
	\[
		\varphi(x,t;s) = \frac{1}{2c} \int_{x-c(t-s)}^{x+c(t-s)} f(y,s) \dif s.
	\]
	A partir d'aquí, hom pot comprovar (com a exercici) que
	\[
		w(x,t) = \int_{0}^t \varphi(x,t,s) \dif s
	\]
	resol la EDP considerada. Combinant ambdues expressions:
	\[
		w(x,t) = \frac{1}{2c} \int_{0}^t \int_{x-c(t-s)}^{x+c(t-s)} f(y,s) \dif y \dif s
		= \frac{1}{2c} \iint_{T(x,t)} f,
	\]
	on $T(x,t)$ és el triangle característic anteriorment definit. Per tant,
	com que $u = v+w$, se segueix el resultat.
\end{proof}
\begin{defi}[fórmula!de d'Alembert-Duhamel]
	La fórmula de d'Alembert-Duhamel és:
	\begin{equation}
		\label{eq:FdA-D}
		u(x,t) = \frac{1}{2} \lp g(x-ct) + g(x+ct) \rp + \frac{1}{2c}\int_{x-ct}^{x+ct} h(y) \dif y
		+ \frac{1}{2c} \iint_{T(x,t)} f,
    \end{equation}
\end{defi}
\begin{obs}
	A EDOs, tota solució es podia expressar com a suma d'una solució particular i una
	solució de l'homogènia. A l'anterior demostració ens hem basat en una idea similar:
	Suposem que coneixem una solució ``particular'' de $u_{tt} - c^2 u_{xx} = f$ (que no
	necessàriament satisfà les condicions inicials). Sigui $w$ aquesta solució. Aleshores,
	la solució general $u$ de l'EDP es pot expressar com $u = w+v$, on $v$ és la solució de
	\[
		\begin{cases}
            v_{tt} - c^2 v_{xx} = 0 \\
            v(x, 0) = g - w(x,0) \\
            v_t (x,0) = h - w_t(x,0).
        \end{cases}
	\]
\end{obs}
\begin{example}
	Considerem el problema
	\[
		\begin{cases}
            u_{tt} - c^2 u_{xx} = x \sin t\\
            u(x, 0) = g(x) \\
            u_t (x,0) = h(x).
        \end{cases}
	\]
	Per inspecció, es pot trobar la solució ``particular'' $w = -x \sin t$. Per tant, només cal resoldre:
	\[
		\begin{cases}
            v_{tt} - c^2 v_{xx} = 0\\
            v(x, 0) = g(x) \\
            v_t (x,0) = h(x) + x,
        \end{cases}
	\]
	que és una equació homogènia.
\end{example}

\section{Condicions de vora}
\subsection{Ona semi-infinita}
Fins ara, hem considerat que la corda era infinita, una suposició que ha simplificat
el problema però que no és realista físicament. Ara farem un pas més i suposarem
que la corda és semi-infinita, és a dir, que $x \in \real$, $x \geq 0$. Per tal de garantir que
el problema estigui ben posat (\emph{well-posed}), haurem de preescriure el comportament
de la corda a la vora, en el nostre cas a $x = 0$.
\begin{defi}[condicions!de Dirichlet]
	Les condicions de Dirichlet fixen la posició de la vora de la corda:
	\[
		u(0,t) = d(t).
	\]
	Es diu que les condicions de vora són homogènies si $d(t) \equiv 0$, i no
	homogènies altrament.
\end{defi}
\begin{defi}[condicions!de Neumann]
	Les condicions de Neumann fixen l'angle de la vora de la corda:
	\[
		u_x(0,t) = n(t).
	\]
	Es diu que les condicions de vora són homogènies si $n(t) \equiv 0$, i no
	homogènies altrament.
\end{defi}
Tot seguit, fem una observació sobre el problema de la corda infinita que ens permetrà resoldre
el problema semi-infinit.
\begin{obs}
	\label{obs:ones_simetric}
	Sigui $u$ una solució del problema no homogeni \eqref{eq:no_homo_ones}. Si $g$, $h$ i $f(\cdot,t)$ són 
	funcions parelles, senars o $L$-periòdiques en $x \in \real$, llavors
	$u(\cdot,t)$ també serà parella, senar o $L$-periòdica, respectivament.
\end{obs}
\begin{proof}
	Es farà la demostració només en el cas parell, la resta de casos són anàlegs. Considerem la funció $u^* (x,t) = u(-x,t)$, 
	la reflexió parella de $u$. En primer lloc, observem que:
	\[
		u^* (x,t) = u(-x,t) \implies u_x^*(x,t) = -u_x(-x,t) \implies u_{xx}^*(x,t) = u_{xx}(-x,t).
	\]
	Vegem ara quina equació satisfà $u^*$:
	\[
		\begin{cases}
			(u_{tt}^* - c^2 u_{xx}^*)(x,t) = (u_{tt} - c^2 u_{xx})(-x,t) = f(-x,t) = f(x,t) \\
			u^* (x,0) = u(-x,0) = g(-x) = g(x) \\
			u_{t}^*(x,0) = u_t(x,0) = h(-x) = h(x).
		\end{cases}
	\]
	Per tant, si $g$, $h$ i $f(\cdot,t)$ són parelles, $u^*$ satisfà la mateixa EDP que $u$, i per unicitat
	de solucions s'ha de tenir
	\[
		u(x,t) = u^*(x,t) = u(-x,t),
	\]
	com volíem demostrar.
\end{proof}
A partir d'aquesta observació, podem resoldre del problema de Dirichlet homogeni en la corda semi-infinita:
\[
	\begin{cases}
		u_{tt} - c^2 u_{xx} = f(x,t), & x \geq 0, t \in \real \\
		u(0,t) = 0, & t \in \real \\
		u(x, 0) = g(x), & x \geq 0 \\
		u_t (x,0) = h(x), & x \geq 0.
	\end{cases}
\]
Considerem les reflexions senars de $f(\cdot,t)$, $g$ i $h$:
\[
	g_s(x) = 
	\begin{cases}
	g(x), & x \geq 0 \\
	-g(-x), & x < 0,
	\end{cases}
\]
\[
h_s(x) = 
	\begin{cases}
	h(x), & x \geq 0 \\
	-h(-x), & x < 0,
	\end{cases}
\]
\[
f_s(x,t) = 
	\begin{cases}
	f(x,t), & x \geq 0 \\
	-f(-x,t), & x < 0.
	\end{cases}
\]
A partir d'aquí podem resoldre el problema per a la corda infinita:
\[
	\begin{cases}
		\tilde{u}_{tt} - c^2 \tilde{u}_{xx} = f_s(x,t), & x \in \real, t \in \real \\
		\tilde{u}(x, 0) = g_s(x), & x \in \real \\
		\tilde{u}_t (x,0) = h_s(x), & x \in \real
	\end{cases}
\]
Aquest problema ja l'hem resolt abans amb la fórmula de d'Alembert-Duhamel:
\[
	\tilde{u}(x,t) = \frac{1}{2} \lp g_s(x-ct) + g_s(x+ct) \rp + \frac{1}{2c}\int_{x-ct}^{x+ct} h_s(y) \dif y
		+ \frac{1}{2c} \iint_{T(x,t)} f_s.
\]
Si $g_s \in \C^2 (\real)$, $h_s \in \C^1 (\real)$, i $f_s \in \C^1 (\real \times \real)$,
l'observació anterior garanteix que $\tilde{u}$ és una funció senar de $x$ i contínua, de manera que per a tot temps $t$
\[
	\tilde{u} (0,t) = 0.
\]
Per tant, $u = \tilde{u}_{|[0,+\infty)}$ és $\C^2 (\real \times \real)$ 
i resol el problema de Dirichlet (és solució clàssica).

Observem que hem necessitat unes condicions suficients per tal d'assegurar-nos que $u$ sigui $\C^2$.
Aquestes condicions les hem imposat sobre $g_s$, $h_s$ i $f_s$, i ara ens agradaria traduir-les
a condicions per a $g$, $h$ i $f$. Es té:
\begin{gather*}
	g_s(x) = 
	\begin{cases}
	g(x), & x \geq 0 \\
	-g(-x), & x < 0,
	\end{cases}
	\\ \implies  
	g_s'(x) = 
	\begin{cases}
	g'(x), & x \geq 0 \\
	g'(-x), & x < 0,
	\end{cases}
	\\ \implies
	g_s''(x) = 
	\begin{cases}
	g''(x), & x \geq 0 \\
	-g''(-x), & x < 0.
	\end{cases}
\end{gather*}
Per tant, $g_s$ serà $\C^2$ si i només si $g(0) = 0$ i $g''(0) = 0$. Anàlogament,
$h_s$ serà $\C^1$ si i només si $h(0) = 0$, i $f_s$ serà $\C^1$ si i només si
$f(0,t) = 0$, $\forall t$.

Les condicions obtingudes anteriorment són condicions suficients, però potser no necessàries.
Ara ens intentarem trobar unes condicions de compatibilitat o necessàries per tal que la solució
\[
	u(x,t) = \frac{1}{2} (g_s(x-ct) + g_s(x+ct)) + \frac{1}{2c} \int_{x-ct}^{x+ct} h_s(s) \dif s
	+ \frac{1}{2c} \iint_{T(x,t)} f_s(y,s) \dif y \dif s
\]
compleixi $u \in \C^2([0,+\infty),\real)$. En primer lloc, comparant les condicions 
inicials de $u$ per a $(x,0)$ i $(0,t)$:
\[
	g(0) = u(0,0) = 0.
\]
Si ara considerem les condicions inicials per a $u_t$:
\[
	u(0,t) = 0 \implies u_t(0,t) = 0 \implies h(0) = u_t(0,0) = 0,
\]
obtenint així una altra condició necessària. Finalment:
\[
	\begin{cases}
	u_{tt}(0,t) = 0, \\
	u_{xx}(x,0) = g''(x)
	\end{cases}
	\implies 
	f(0,0) = (u_{tt} - c^2 u_{xx})(0,0) = -c^2 g''(0).
\]
Hem obtingut, per tant, tres condicions necessàries per tal que $u$ sigui solució clàssica.
\begin{defi}[condicions!de compatibilitat]
	Les condicions
	\begin{itemize}
		\item $g(0) = 0$
		\item $h(0) = 0$
		\item $f(0,0) + c^2 g''(0) = 0$
	\end{itemize}
	s'anomenen condicions de compatibilitat.
\end{defi}
\begin{ej}
	Demostreu que les condicions de compatibilitat són també condicions suficients.
\end{ej}
\begin{example}
	Analitzem la reflexió d'una ona fixada a la paret (condicions de Dirichlet homogènies)
	quan $f \equiv 0$, $h \equiv 0$. En aquest cas, l'ona es pot expressar amb la fórmula:
	\[
		u(x,t) = \frac{1}{2} (g_s(x+ct) + g_s(x-ct)),
	\]
	o, equivalentment:
	\[
		u(x,t) = 
		\begin{cases}
			\frac{1}{2} (g(x+ct) + g(x-ct)), & \text{si }x \geq ct \\
			\frac{1}{2} (g(x+ct) - g(-x+ct)), & \text{si }x \leq ct 
		\end{cases}
	\]
	D'aquí, podem interpretar que quan l'ona xoca amb la paret es produeix una inversió de l'amplitud.
	\begin{center}
		\begin{tikzpicture}
			\begin{axis}[
					xmin = 0,
					xmax = 5,
					ymin = -1.5,
					ymax = 1.5,
					samples = 100,
					axis lines = center,
					x tick label style={major tick length=0pt},
					xticklabels={},
					yticklabels={}
			]
				\addplot[domain=2:4, thick]{cos(90*(x-3))};
				\addplot[domain=1:3, color=red,thick]{-cos(90*(x-2))};
				\addplot[domain=2.5:3.5]{1.1};
				\addplot[domain=2.5:2.6]{1.1+0.5*(x-2.5)};
				\addplot[domain=2.5:2.6]{1.1-0.5*(x-2.5)};
				\addplot[domain=1.5:2.5, color=red]{-1.1};
				\addplot[domain=2.4:2.5, color=red]{-1.1+0.5*(x-2.5)};
				\addplot[domain=2.4:2.5, color=red]{-1.1-0.5*(x-2.5)};
			\end{axis}
		\end{tikzpicture}
	\end{center}
\end{example}
\begin{ej}
	En el context de l'exemple anterior, demostreu que si $g$ és una funció parella (és a dir, existeix $a$ tal que
	$g(a+h) = g(a-h)$ per a tot $h$), aleshores hi ha un instant de temps en el qual l'ona incident a la paret
	és nu\lgem a.
\end{ej}

\subsection{Ona finita}
Considerem ara una corda finita continguda a l'interval $x \in [0,L]$. 
Assumint condicions de Dirichlet homogènies, es té:
\begin{equation}
	\label{eq:ones_finita}
	\begin{cases}
		u_{tt} - c^2 u_{xx} = f(x,t), & x \in [0,L], \, t \in \real \\
		u(0,t) = u(L,t) = 0, & t \in \real \\
		u(x,0) = g(x), & x \in [0,L] \\
		u_t (x,0) = h(x), & x \in [0,L].
	\end{cases}
\end{equation}
Sigui $\Omega = (0,L) \times \real$. Per tal que una solució $u$ tingui sentit, s'ha de satisfer:
\begin{itemize}
	\item $u \in \C^2 (\Omega)$ per tal que pugui ser solució clàssica.
	\item $u \in \C^0 (\overline{\Omega})$ per tal que sigui contínua a les vores.
\end{itemize}
Resoldrem el problema mitjançant el mètode de reflexions:
\begin{enumerate}[1.]
	\item Estenem $g$, $h$, $f(\cdot,t)$ de forma senar respecte $x = 0$, de 
	manera que les funcions queden definides a $[-L,L]$.
	\item Fem l'extensió $2L$-periòdica de les funcions obtingudes anteriorment.
\end{enumerate}
\begin{center}
	\begin{tikzpicture}
		\begin{axis}[
				xmin = -3.5,
				xmax = 3.5,
				ymin = -1.5,
				ymax = 1.5,
				samples = 100,
				axis lines = center,
				x tick label style={major tick length=0pt},
				xticklabels={},
				yticklabels={}
		]
			\addplot[domain=0:2, thick]{sin(90*x)};
			\addplot[dashed, domain=-2:0,thick]{sin(90*x)};
			\addplot[dashed, domain=-4:-2, color=red,thick]{sin(90*x)};
			\addplot[dashed, domain=2:4, color=red,thick]{sin(90*x)};
			\addplot[mark=*,color=black] coordinates {(2, 0)} node[below]{$L$};
			\addplot[mark=*,color=black] coordinates {(-2, 0)} node[below]{$-L$};
		\end{axis}
	\end{tikzpicture}
\end{center}
Siguin $g_{ext}$, $h_{ext}$ i $f_{ext}$ les extensions obtingudes, i sigui $\tilde{u}$ la corresponent 
solució obtinguda amb la fórmula de d'Alembert-Duhamel \eqref{eq:FdA-D} a tot $\real$.
Volem que
\[
	u = \tilde{u}_{|[0,L]}
\]
sigui solució clàssica del problema de la corda finita. Per a assegurar-ho, necessitem
$g_{ext} \in \C^2$, $h_{ext} \in \C^1$, $f_{ext} \in \C^1$. Farem un raonament anàleg al de la corda
semi-infinita per a imposar condicions suficients sobre $g$, $h$ i $f$.

Si $g_s$ és la reflexió senar de $g$, observem que
\[
	\begin{rcases}
		 g \in \C^2 \\
		 g(0) = 0 \\
		 g''(0) = 0
	\end{rcases}
	\implies g_s \in \C^2
\]
Per tant, quan fem l'extensió periòdica,
\[
	\begin{rcases}
		 g_s \in \C^2 \\
		 g(L) = 0 \\
		 g''(L) = 0
	\end{rcases}
	\implies g_{ext} \in \C^2
\]
D'aquesta manera, obtenim unes condicions suficents per a garantir $g_{ext} \in \C^2$. Anàlogament:
\[
	\begin{rcases}
		 h \in \C^1 \\
		 h(0) = 0 \\
		 h(L) = 0
	\end{rcases}
	\implies h_{ext} \in \C^1
\]
\[
	\begin{rcases}
		 f \in \C^1 \\
		 f(0,t) = 0 \\
		 f(L,t) = 0
	\end{rcases}
	\implies f_{ext} \in \C^1
\]
Només queda per veure que $u$ compleix les condicions de vora, però això se segueix del fet que
$g_{ext}$, $h_{ext}$ i $f_{ext}$ són senars respecte $x = 0$ i $x = L$ (la comprovació es deixa
com a exercici pel lector), i de l'observació \ref{obs:ones_simetric}.

\begin{obs}
	En aquest cas, també tenim unes condicions necessàries o de compatibilitat, que també
	resulten ser suficients. En concret, són:
	\begin{itemize}
		\item $g(0) = g(L) = 0$
		\item $h(0) = h(L) = 0$
		\item $f(0,0) + c^2g''(0) = f(L,0) + c^2 g''(L) = 0$
	\end{itemize}
\end{obs}
\begin{prop}
	La solució de l'equació \eqref{eq:ones_finita} és única.
\end{prop}
\begin{proof}
	Suposem que tenim dues solucions $u_1$ i $u_2$. Sigui $v = u_1 - u_2$,
	amb $v \in \C^2([0,L],\real)$. Considerem $v_s$, l'extensió senar de $v$ respecte
	$x=0$, definida a $[-L,L]$. Volem $v_s \in \C^2([0,L],\real)$. Observem que:
	\begin{itemize}
		\item $v(0,t) = 0$
		\item $v_{tt}(0,t) = 0 \implies v_{xx} (0,0) = \frac{1}{c^2} v_{tt} (0,0) = 0$.
	\end{itemize}
	Per tant, $v_s$ és $\C^2$. Pel mateix argument, podem veure que $v_{ext} \in \C^2([0,L],\real)$.
	En aquest cas, atès que $v$ satisfà unes condicions inicials nu\lgem es: 
	\begin{gather*}
		(\partial_x - c \partial_t)(\partial_x + c \partial_t)v = (\partial_x - c \partial_t)w = 0
		\implies w \equiv 0, \\
		w = v_x + c v_t = 0 \implies v \equiv 0,
	\end{gather*}
	com volíem veure.
\end{proof}
\begin{col}
	En el cas homogeni ($u_{tt} - c^2 u_{xx} = 0$), la solució del problema de la corda
	finita en $(0,L)$ amb condicions de Dirichlet homogènies \eqref{eq:ones_finita} és $\frac{2L}{c}$-periòdica
	en el temps.
\end{col}
\begin{proof}
	Utilitzant la solució explícita $u = \tilde{u}_{|[0,L] \times \real}$,
	\begin{gather*}
		u(x,t) = \frac{1}{2} (g_{ext} (x-ct) + g_{ext} (x+ct)) + \frac{1}{2c} \int_{x-ct}^{x+ct} h_{ext}(y) \dif y = \\
		=\lp \frac{1}{2} g_{ext} (x-ct) + \frac{1}{2c} \int_{x-ct}^{0} h_{ext} (y) \dif y \rp +
		\lp \frac{1}{2} g_{ext} (x+ct) + \frac{1}{2c} \int_{0}^{x+ct} h_{ext} (y) \dif y \rp = \\
		= F(x-ct) + G(x+ct)
	\end{gather*}
	És suficient comprovar que $F$, $G$ són $2L$-periòdiques. Farem la demostració per a $F$, per a $G$ és anàleg. 
	Observem que, com que $g_{ext}$ és $2L$-periòdica, només cal veure que
	\[
		H(x) = \int_{x}^0 h_{ext} (y) \dif y
	\]
	és $2L$-periòdica. En efecte,
	\[
		H(x+2L) = \int_{x+2L}^x h_{ext} (y) \dif y + \int_{x}^{0} h_{ext} (y) \dif y.
	\]
	Com que $h_{ext}$ és senar respecte $L$ i $2L$-periòdica,
	\[
		\int_{2L}^0 h_{ext} (y) \dif y = 0 \implies \int_{x+2L}^x h_{ext} (y) \dif y = 0
		\implies H(x+2L) = H(x).
	\]
\end{proof}

Fins ara, hem solucionat EDPs amb condicions de vora homogènies, i ens preguntem com ens ho
podem fer per resoldre el cas no homogeni. En primer lloc, considerem condicions de Dirichlet no homogènies:
\[
	\begin{cases}
		u_{tt} - c^2 u_{xx} = f(x,t), & 0 \leq x \leq L, \, t \in \real \\
		u(0,t) = d_0 (t), & t \in \real \\
		u(L,t) = d_L (t), & t \in \real \\
		u(x,0) = g(x), & x \in [0,L] \\
		u_t (x,0) = h(x),  & x \in [0,L]
	\end{cases}
\]
L'objectiu és reduir el cas no homogeni al cas homogeni, que ja sabem resoldre. Ho farem
restant una certa funció a la incògnita $u$, de manera que passi a satisfer condicions de vora homogènies.
L'opció més senzilla serà restar la recta que uneix $d_0 (t)$ i  $d_L (t)$:
\[
	v := u(x,t) - (d_0 (t) + (d_L (t) - d_0 (t))\frac{x}{L}),
\]
que, en efecte, satisfà:
\[
	v(0,t) = v(L,t) = 0.
\]
Per tant, $v$ satisfà una equació amb condicions de vora homogènies amb unes certes
$\tilde{f}$, $\tilde{g}$, $\tilde{h}$ que es poden determinar fàcilment (es deixa com a
exercici pel lector).
\begin{obs}
	L'elecció de $v$ no és única.
\end{obs}
Considerem ara condicions de Neumann no homogènies ($u_x(0,t) = n_0 (t)$, $u_x(L,t) = n_L(t)$).
Volem trobar una funció que tingui angle $n_0 (x)$ a $x = 0$ i $n_L (x)$ a $x = L$. Una paràbola servirà:
\[
	w(x,t) = n_0(t) x + \frac{n_L (t) - n_0 (t)}{L} \frac{x^2}{2}.
\]
Les condicions de vora seran:
\begin{gather*}
	w_x (0,t) = n_0 (t); \\
	w_x (L,t) = n_0 (t) L + \frac{n_L (t) - n_0 (t)}{L}L = n_L (t).
\end{gather*}
Per tant, $u-w$ satisfarà unes condicions de vora homogènies.

\section{Conservació de l'energia i aplicacions}
Al problema 1 de la llista 3 s'estudia la conservació de l'energia en una corda infinita que
satisfà l'equació homogènia i que té les condicions inicials amb suport compacte. Què succeeix en una corda finita?

\begin{prop}
	Per l'equació d'ones homogènia amb condicions de vora nu\lgem es (de Dirichlet o de Neumann), l'energia total
	\[
		E(t) = \int_{0}^L \lp \frac{1}{2} u_t^2(x,t) + \frac{1}{2} c^2 u_{x}^2 (x,t) \rp \dif x
	\]
	és constant en el temps, independentment de les condicions inicials.
\end{prop}
\begin{proof}
	Calculem la derivada de l'energia respecte el temps i veiem que és 0:
	\[
		E'(t) = \int_{0}^L \lp u_t u_{tt} + c^2 u_x u_{xt} \rp \dif x =
		u_x u_t \big|_{x = 0}^{L} + \int_{0}^L \lp u_t u_{tt} - c^2 u_{xx} u_{t} \rp \dif x,
	\]
	on a l'última igualtat hem integrat per parts. Si les condicions són de Dirichlet,
	\begin{gather*}
		u(0,t) = u(L,t) = 0 \\
		\implies u_t(0,t) = u_t (L,t) = 0.
	\end{gather*}
	D'altra banda, si les condicions són de Neumann,
	\[
		u_x (0,t) = u_x (L,t) = 0.
	\]
	En qualsevol cas,
	\[
		\begin{rcases}
			u_x u_t \big|_{x = 0}^{L} = 0 \\
			u_t u_{tt} - c^2 u_{xx} u_{t} = 0
		\end{rcases}
		\implies E'(t) = 0.
	\]
\end{proof}
Una conseqüència curiosa de la conservació de l'energia és que ens permet demostrar de manera
alternativa la unicitat de solucions:
\begin{col}
	Donades $g$ i $h$, si existeix solució de classe $\C^2 ([0,L] \times \real)$ a l'equació
	d'ones amb condicions de vora homogènies (de Dirichlet o de Neumann), aleshores la solució és única.
\end{col}
\begin{proof}
	Si $u$ i $\tilde{u}$ són solucions, aleshores $v = u - \tilde{u}$ resol el problema completament homogeni.
	L'energia és:
	\[
		E(t) = \int_0^L \frac{1}{2} v_t^2 + \frac{c^2}{2} v_x^2 \dif x.
	\]
	Com que l'energia és constant, $E(t) = E(0)$. A $t = 0$,
	\[
		\lp \frac{1}{2} v_t^2 + \frac{c^2}{2} v_x^2 \rp (0,0) = 0 \implies E(t) = E(0) = 0.
	\]
	Aleshores, tenim que la integral d'una funció contínua i positiva és 0, de manera que
	la funció ha de ser idènticament 0. En conseqüència,
	\[
		\frac{1}{2} v_t^2 + \frac{c^2}{2} v_x^2 \equiv 0 \implies v_t \equiv 0 \implies v \equiv 0.
	\]
	
\end{proof}
\section{Equació d'ones en dimensions superiors}
\begin{teo}[Teorema de la divergència]
	Donat un camp vectorial $T \in \C^1(\Omega; \real^n) \cap \C^0(\overline{\Omega}; \real^n)$,
	on $\Omega \subset \real^n$ és un domini suficientment regular\footnote{
		Per exemple, si $\Omega$ és Lipschitz (és a dir, si $\Omega$ és localment la gràfica d'una
		funció Lipschitz).
	}. Aleshores,
	\begin{equation}
		\label{teo:div}
		\int_{\Omega} \diver T \dif x = \int_{\partial \Omega} T \cdot \nu \dif S,
	\end{equation}
	sent $\nu$ la normal exterior a $T$.
\end{teo}
El resultat és un cas particular del teorema de Stokes. Pel propòsit d'aquest curs,
ens serà particularment interessant el següent coro\lgem ari.
\begin{col}[Integració per parts a $\real^n$]
    \label{col:int_parts}
	Donats $u,v \colon \Omega \subset \real^n \to \real$, es compleix
	\begin{equation}
		\label{col:perParts}
		\int_{\Omega} u v_{x_i} = - \int_{\Omega} u_{x_i} v + \int_{\partial \Omega} (uv) \nu^i,
	\end{equation}
	on $u_{x_i}$ representa la derivada parcial respecte $x_i$ i $\nu^i$ és la component $i$-èsima de $\nu$.
\end{col}
\begin{proof}
	Considerant
	\[
		T = (0,...,0,uv,0,...,0) 
	\]
	i aplicant \eqref{teo:div} s'obté el resultat desitjat.
\end{proof}
Fixem-nos ara en l'equació d'ones en $n$ dimensions:
\[
	\begin{cases}
		u_{tt} - c^2 \Delta u = 0, & x \in \Omega \subset \real^n \\
		u(x,0) = g & x \in \Omega \\
		u_t(x,0) = h & x \in \Omega \\
		\text{condicions de vora} & x \in \partial \Omega
	\end{cases}
\]
on les condicions de vora poden ser:
\begin{itemize}
	\item de Dirichlet: $u = 0$
	\item de Neumann: $\nabla u \cdot \nu = 0$.
\end{itemize}
\begin{prop}
	L'energia
	\[
		E(t) = \int_{\Omega} \frac{1}{2} u_t^2 + \frac{c^2}{2} |\nabla u|^2
	\]
	és constant en el temps.
\end{prop}
\begin{proof}
	Derivant $E$ respecte el temps:
	\[
		\frac{d}{dt} E(t) = \int_{\Omega} u_t u_{tt} + c^2\nabla u \nabla u_t.
	\]
	Ara, integrant per parts:
	\[
		\int_{\Omega} u_t u_{tt} + c^2\nabla u \nabla u_t = 
		\int_{\Omega} u_t u_{tt} - c^2\Delta u  u_t \dif x +
		\int_{\partial \Omega} c^2 u_t \nabla u \cdot \nu \dif S,
	\]
	però el primer terme és zero utilitzant la EDP i el segon terme és zero a causa
	de les condicions de vora (siguin les que siguin). Per tant,
	\[
		\frac{d}{dt} E(t) = 0,
	\]
	com volíem veure.
\end{proof}
\begin{col}
	Si existeix alguna solució per l'equació d'ones $n$-dimensional, aleshores és única.
\end{col}
\begin{proof}
	Suposem que $u$, $v$ són dues solucions. Aleshores, $w = u-v$ resol la EDP amb $h = g = 0$.
	Per la proposició anterior,
	\[
		E(t) = E(0) = 0 \implies w = 0.
	\]
\end{proof}
