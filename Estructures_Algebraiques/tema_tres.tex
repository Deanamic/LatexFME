\chapter{Cuerpos}

\section{Preliminares}

\begin{defi}[cuerpo]
    Recordemos la definición de cuerpo. Decimos que un anillo unitario conmutativo $\k$ es un cuerpo si $\k^* = \k \setminus \{0\}$.
\end{defi}

\begin{prop}
    Sea $A$ un anillo conmutativo. Entonces,
    \begin{enumerate}[i)]
        \item Si $A$ es un cuerpo, entonces es íntegro.
        \item Si $A$ es íntegro, entonces es un subanillo de un cuerpo.
        \item $A$ es un cuerpo si, y solo si, $\left\{ \text{ideales de } A \right\}
            = \left\{ 0, A \right\}$.
        \item Sea $B$ un anillo. Si $f \colon A \to B$ es un morfismo de anillos y $A$ es un
            cuerpo, entonces $f$ es inyectiva.
    \end{enumerate}
\end{prop}

\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item Sean $x, y \in A$ tales que $xy = 0$. Si $x = 0$ ya hemos
            acabado. Si $x \neq 0$, entonces $\exists x^{-1} \in A$ y
            se tiene que
            \[
                y = 1y = \left( x^{-1} x \right) y = x^{-1}xy =
                x^{-1}0 = 0.
            \]
        \item Por ser $A$ íntegro, podemos considerar su cuerpo de fracciones $\k = \k \lp A\rp$. Se tiene que $A\subseteq \k$ es un subanillo.
        \item Empecemos por la implicación directa. Si $A$ es un cuerpo, entonces cualquier ideal distinto de $\lp 0\rp$ contiene la unidad, puesto que $a\in I \implies 1=a^{-1}a\in I$. Y un ideal que contiene la unidad es el anillo entero. Para ver la implicación inversa, basta ver que el ideal generado por cualquier elemento no nulo contiene la unidad, de modo que dicho elemento tiene inverso en el anillo.
        \item Consideremos $\ker\lp f\rp \subseteq A$, que es un ideal. Puesto que $f\lp 1\rp = 1$, $1\notin \ker\lp f\rp$ y, como se ha visto en el punto anterior, $\ker\lp f\rp = 0$, con lo que $f$ es inyectiva.
    \end{enumerate}
\end{proof}

\begin{defi}[cuerpo!de las funciones racionales]
    Sea $\k$ un cuerpo y sea $S = \k[x] \setminus \left\{ 0
    \right\}$, que es un sistema multiplicativo por ser $\k\lb x\rb$ íntegro. Definimos $\k(x) = S^{-1}\k[x]$. Es decir,
    \[
        \k(x) = \left\{ f \mid f = \frac{p}{q}, \, p, q
        \in \k[x], \, q\neq 0 \right\}
    \]
    es el cuerpo de las funciones racionales con coeficientes en
    $\k$. Análogamente, definimos $\k\left( x_1, \dots,  x_n
    \right)$.
\end{defi}

\begin{example}
    \begin{enumerate}[1.]
        \item[]
        \item $\q$, $\real$ y $\cx$ son cuerpos con las operaciones habituales.
        \item Sea $\k$ un cuerpo y sea $p\lp x\rp \in \k\lb x\rb$ un polinomio primo.
            Entonces, $\faktor{\k[x]}{\lp p(x) \rp}$ es un cuerpo. Veámoslo.

            Sea $I = \lp f\lp x\rp \rp$ un ideal de $\k[x]$ tal que $\lp p(x) \rp
            \subseteq I$. Entonces, $p(x) = f\lp x\rp \cdot g\lp x\rp$ y, por lo tanto, o bien $f\lp x\rp = 1$ e $I=A$,
            o bien $g\lp x\rp = 1$ e $I=\lp p\lp x\rp\rp$. Así pues, los únicos ideales que contienen a $p(x)$
            son $\lp p(x) \rp$ y $\k[x]$, de modo que
            $\k[x]$/$\lp p(x) \rp$ solo tiene dos
            ideales (el cero y el total) y es un
            cuerpo.
    \end{enumerate}
\end{example}

\begin{obs}
    Sea $\k$ un cuerpo. Existe un único morfismo de anillos $f\colon \z \to \k$.
\end{obs}
\begin{proof}
    Se tiene que
    \begin{align*}
        f\lp 1\rp &= 1, \\
        f\lp 0\rp &= f\lp 0\rp + f\lp 0\rp - f\lp 0\rp = f\lp 0 + 0\rp - f\lp 0\rp = f\lp 0\rp - f\lp 0\rp = 0, \\
        f\lp -1\rp &= f\lp -1\rp + f\lp 1\rp - f\lp 1\rp = f\lp 0\rp - f\lp 1\rp = -1.
    \end{align*}
    Entonces, para todo $n>0$,
    \begin{align*}
        f\lp n\rp &= f( \overbrace{1 + \cdots + 1}^n ) = \overbrace{1 + \cdots + 1}^n, \\
        f\lp -n\rp &= f( \overbrace{\lp -1\rp + \cdots + \lp -1\rp }^n )= \overbrace{\lp -1\rp + \cdots + \lp -1\rp}^n.
    \end{align*}
    Así pues, $f$ está únicamente definido.
\end{proof}

\begin{obs}[Notaci\'on]
    En algunas ocasiones notaremos con un entero $n$ el siguiente elemento de un cuerpo $\k$:
    \[
        n = \underbrace{1_{\k}+\dots+1_{\k}}_{n}.
    \]
\end{obs}

\begin{defi}[caraterística de un cuerpo]\idx{cuerpo!primo}
    Sea $\k$ un cuerpo y sea $f\colon \z \to \k$ el morfismo de anillos entre el anillo de los enteros y $\k$. Para definir la característica y el cuerpo primo de $\k$, estudiaremos dos casos en función del núcleo de $f$.
    \begin{itemize}
        \item $\ker \lp f\rp = \lc 0 \rc$. Por ser $\z$ íntegro, $S=\z\setminus \lc 0\rc$ es un sistema multiplicativo y satisface que $f\lp S\rp \subseteq \k\setminus \lc 0\rc = \k^*$. Pongamos $\q = S^{-1}\z$ y veamos cuantos morfismos de anillos hay entre $\q$ y $\k$. Puesto que la composición de morfismos de anillos es un morfismo de anillos y la aplicación
        \begin{align*}
            i\colon \z &\to \q \\
            a &\mapsto \frac{a}{1}
        \end{align*}
        es un morfismo de anillos, todos los morfismos de anillos $\tilde{f}\colon \q \to \k$ cumplen que $\tilde{f} \circ i \colon \z \to \k$ es un morfismo de anillos. Pero solamente hay un morfismo de anillos entre $\z$ y $\k$: $f$. Por lo tanto, todos los morfismos de anillos $\tilde{f}\colon \q \to \k$ cumplen que $\tilde{f} \circ i = f$. En virtud de la propiedad universal del simetrizado, concluimos que existe un único morfismo de anillos
            \begin{figure}[H]
                \centering
                \begin{minipage}{0.4\textwidth}
                    \[
                        \begin{aligned}
                            \tilde{f} \colon \q &\to \k \\
                            \frac{n}{m} &\mapsto n m^{-1} \equiv
                            \frac{n}{m}
                        \end{aligned}
                    \]

                \end{minipage}
                \begin{minipage}{0.4\textwidth}
                    \begin{center}
                        \begin{tikzpicture}[scale=10]
                            \node (z) {$\z$};
                            \node[below=2em of z] (q) {$\q = S^{-1}\z$};
                            \node[right=4em of z] (k) {$\k$};
                            \draw[->] (z) to (q);
                            \draw[->] (z) to node[auto] {$f$} (k);
                            \draw[->] (q) to node[below] {$\tilde{f}$} (k);
                        \end{tikzpicture}
                    \end{center}
                \end{minipage}
            \end{figure}
            En este caso, decimos que $\k$ tiene característica $\car \k = 0$ y que su cuerpo primo es $\q$.
        \item $\ker \lp f\rp \neq \lc 0\rc$. En primer lugar, observamos que $\lc 0\rc$ es un ideal primo de $\k$, puesto que $\faktor{\k}{\lc 0\rc} \cong \k$, que es íntegro. Entonces, $\ker \lp f\rp = f^{-1}\lp \lc 0\rc \rp$ es un ideal primo de $\z$, que también es principal por ser $\z$ un dominio de ideales principales. Así pues, $\ker \lp f\rp = \lp p\rp = p\z$, siendo $p\in \z$ un entero primo. Pongamos $\F_p = \faktor{\z}{p\z}$ y veamos cuantos morfismos de anillos hay entre $\F_p$ y $\k$. Puesto que la composición de morfismos de anillos es un morfismo de anillos y la aplicación
        \begin{align*}
            \pi\colon \z &\to \F_p \\
            a &\mapsto \class{a}
        \end{align*}
        es un morfismo de anillos, todos los morfismos de anillos $\tilde{f}\colon \F_p \to \k$ cumplen que $\tilde{f} \circ \pi \colon \z \to \k$ es un morfismo de anillos. Pero solamente hay un morfismo de anillos entre $\z$ y $\k$: $f$. Por lo tanto, todos los morfismos de anillos $\tilde{f}\colon \F_p \to \k$ cumplen que $\tilde{f} \circ \pi = f$. En virtud de la propiedad universal del anillo cociente, concluimos que existe un único morfismo de anillos
            \begin{figure}[H]
                \centering
                \begin{minipage}{0.4\textwidth}
                    \[
                        \begin{aligned}
                            \tilde{f} \colon \F_p &\to \k \\
                            \class{a} &\mapsto a
                        \end{aligned}
                    \]

                \end{minipage}
                \begin{minipage}{0.4\textwidth}
                    \begin{center}
                        \begin{tikzpicture}[scale=10]
                            \node (z) {$\z$};
                            \node[below=2em of z]
                            (q) {$\F_p = \sfrac{\z}{p\z}$};
                            \node[right=4em of z] (k) {$\k$};
                            \draw[->] (z) to (q);
                            \draw[->] (z) to node[auto] {$f$} (k);
                            \draw[->] (q) to node[below] {$\tilde{f}$} (k);
                        \end{tikzpicture}
                    \end{center}
                \end{minipage}
            \end{figure}
            En este caso, decimos que $\k$ tiene característica $\car \k = p$ y que su cuerpo primo es $\F_p$.
    \end{itemize}
\end{defi}

\iffalse
\begin{defi}[caraterística de un cuerpo]\idx{cuerpo!primo}
    Sea $\k$ un cuerpo. Entonces $\exists ! f \colon \z \to \k$ morfismo de
    anillos, ya que $f(1) = 1$ $\implies \forall 0 \leq n \in \n$
    \[
        \begin{cases}
            f(n) = f(\overbrace{1+\cdots+1}^{n})
            = \overbrace{f(1)+\cdots+f(1)}^{n} = \overbrace{1+\cdots+1}^n =
            n \in \k
            \\
            f(-n) = f( \overbrace{(-1) + \cdots + (-1)}^n ) =
            \overbrace{f(-1) + \cdots + f(-1)}^n = \overbrace{(-1) +
            \cdots + (-1)}^n = -n \in \k
        \end{cases}
    \]
    ya que $0 = f(0) = f((-1) + 1) = f(1) + f(-1) = 1 + f(-1)$ $\implies$
    $f(-1) = -1$.

    \begin{itemize}
        \item Caso 1: $\ker{f} = 0$. Tomamos $S = \z \setminus \left\{
            0 \right\}$ ($\z$ es íntegro) y $f(S) \subseteq \k
            \setminus \left\{ 0 \right\} = \k^\ast$. Por la
            propiedad universal de la simerización (o localización),
            tenemos que $\exists ! \tilde{f}$.
            \begin{figure}[H]
                \centering
                \begin{minipage}{0.4\textwidth}
                    \begin{center}
                        \begin{tikzpicture}[scale=10]
                            \node (z) {$\z$};
                            \node[below=2em of z] (q) {$\q = S^{-1}\z$};
                            \node[right=4em of z] (k) {$\k$};
                            \draw[->] (z) to (q);
                            \draw[->] (z) to node[auto] {$f$} (k);
                            \draw[->] (q) to node[below] {$\tilde{f}$} (k);
                        \end{tikzpicture}
                    \end{center}
                \end{minipage}
                \begin{minipage}{0.4\textwidth}
                    \[
                        \begin{aligned}
                            \tilde{f} \colon \q &\to \k \\
                            \frac{n}{m} &\mapsto n m^{-1} = \colon
                            \frac{n}{m}
                        \end{aligned}
                    \]

                \end{minipage}
            \end{figure}
            morfismo de anillos inyectivo (por el ejercicio 4).
            Diremos que $\k$ tiene característica 0 ($\car \k = 0$)
            y $\q$ es cuerpo primo de $\k$.
        \item Caso 2: $\ker f \neq 0$. Entonces $\left\{ 0 \right\}$ es
            ideal primo de $\k$, ya que $\sfrac{\k}{\langle \left\{
            0 \right\} \rangle} \cong \k$ íntegro. En este caso,
            $\ker f$ es ideal primo y principal, por lo tanto, $\ker
            f = \langle p \rangle$ con $p \in \z$ primo. Por la
            propiedad universal del cociente, tenemos
            \begin{figure}[H]
                \centering
                \begin{minipage}{0.4\textwidth}
                    \begin{center}
                        \begin{tikzpicture}[scale=10]
                            \node (z) {$\z$};
                            \node[below=2em of z]
                            (q) {$\F_p = \sfrac{\z}{p\z}$};
                            \node[right=4em of z] (k) {$\k$};
                            \draw[->] (z) to (q);
                            \draw[->] (z) to node[auto] {$f$} (k);
                            \draw[->] (q) to node[below] {$\tilde{f}$} (k);
                        \end{tikzpicture}
                    \end{center}
                \end{minipage}
                \begin{minipage}{0.4\textwidth}
                    \[
                        \begin{aligned}
                            \tilde{f} \colon F_p &\to \k \\
                            \bar{n} &\mapsto n
                        \end{aligned}
                    \]

                \end{minipage}
            \end{figure}
            morfismo inyectivo. Diremos que $\k$ tiene
            característica $p$ ($\car \k = p$) y $\F_p$ es cuerpo
            primo de $\k$.
    \end{itemize}

\end{defi}
\fi

\begin{ej}
    Sea $\k$ un cuerpo. Demostrar que el cuerpo primo de $\k$ es
    \[
        \bigcap\limits_{\substack{K \text{ cuerpo} \\ K \subseteq
        \k}}K.
    \]
\end{ej}

\begin{defi}[inmersión de Fröbenius]
     Sea $\k$ un cuerpo con $\car \k = p$. Llamamos inmersión de Fröbenius a la
     aplicación
     \[
         \begin{aligned}
             \Frob_p \colon \k &\to \k \\
             x &\mapsto x^p.
         \end{aligned}
     \]
\end{defi}

\begin{obs}
    $\Frob_p$ es un morfismo de anillos, ya que
    \begin{gather*}
        \Frob_p(x+y) = (x+y)^p = \sum^p_{i = 0} {p \choose i}
        x^{p-i} y^i = \\ = x^p + \sum^{p-1}_{i = 1} \dot{p}x^{p-i} y^i +
        y^p = x^p + y^p = \Frob_p(x) + \Frob_p(y),
    \end{gather*}
    \[
        \Frob_p(xy) = (xy)^p = x^py^p = \Frob_p(x)\Frob_p(y)
    \]
    y $\Frob_p(1) = 1^p = 1$. Además, es inyectiva porque $\k$ es un cuerpo.
\end{obs}

\begin{example}
    Estudiemos la inmersión de Fröbenius cuando $\k = \F_p$. El grupo multiplicativo $\F^*_p = \F_p
    \setminus \left\{ 0 \right\}$ tiene $p-1$ elementos, de modo que $\forall a \in \F_p ^\ast$, $a^{p-1} = 1$, lo que implica que $a^p = a$.
    Así pues, $\Frob_p = \Id$. En particular, si $f(x) \in \F_p[x]$,
    \[
        \left( f(x) \right)^p = \left( \sum^n_{i=0} a_i x^i \right)^p =
        \sum^n_{i=0} a^p_i\left( x^i \right)^p + \dot{p}= \sum^n_{i=0} a_i\left(
        x^p \right)^i = f\left( x^p \right).
    \]
\end{example}

\begin{defi}[cuerpo!perfecto]
    Sea $\k$ un cuerpo. Diremos que $\k$ es perfecto si $\car \k = 0$ o bien
    $\car \k = p$ y $\Frob_p$ es una aplicación biyectiva.
\end{defi}

\begin{example}
    \begin{enumerate}[1.]
        \item[]
        \item $\q$, $\real$, $\cx$ son perfectos ($\car = 0$).
        \item $\F_p$ es perfecto ($\Frob_p = \Id$).
        \item Todo cuerpo finito es perfecto. Puesto que $\Frob_p$ es inyectiva y $\k$ es finito, $\Frob_p$ es biyectiva.
        \item $\F_p(x)$ no es perfecto.
    \end{enumerate}
\end{example}

\section{Extensión de cuerpos}

\begin{defi}[extensión!de cuerpos]\idx{grado de una extensión}
    Sean $\k, \E$ cuerpos tales que $\k\subseteq \E$. Decimos que forman una extensión de cuerpos y la notamos como $\E / \k$. Además, $\E$ tiene estructura de espacio vectorial sobre $\k$ y decimos que la dimensión de la extensión es $\dim_{\k} \E \equiv \lb \E \colon \k \rb$.
\end{defi}

\begin{example}
    \begin{enumerate}[1)]
        \item[]
        \item Consideremos la extensión de cuerpos $\real \subset \cx$. Su dimensión es $\left[ \cx \colon \real \right]=2$ y
            $\left\{ 1, i \right\}$ es una base del $\real$-e.v. $\cx$.
        \item Consideremos la extensión de cuerpos $\q \subset \real$. Ejercicio:
            demostrar que $\{ \log(p) \mid p \in \z, \, p
            \text{ primo} \}$ es un conjunto $\q$-l.i. de
            elementos de $\real$. Así pues, observamos que su dimensión no es finita.
        \item Sea $\k$ un cuerpo y sea $p(x) \in \k[x]$ un polinomio primo y mónico. $\E =
            \faktor{\k[x]}{\lp p(x) \rp}$ es un cuerpo. Se tiene que
            \[
                0 = \class{p(x)} = a_0 + a_1 \class{x} + \cdots +
                a_{n-1} \class{x}^{n-1} + \class{x}^n.
            \]
            Ejercicio: demostrar que $\left\{ 1,
            \class{x}, \dots, \class{x}^{n-1} \right\}$ es una $\k$-base de
            $\E$, $\left[ \E \colon \k \right] = n$.

            Ejercicio: demostrar que si $\k \subseteq \E \subseteq \mathbb{L}$
            es una extensión de cuerpos, entonces $\left[ \mathbb{L}
            \colon \k \right] = \left[ \mathbb{L} \colon \E \right]
            \left[ \E \colon \k \right]$.
    \end{enumerate}
\end{example}

\begin{defi}[elemento!algebraico]\idx{elemento!trascendental}\idx{polinomio!mínimo irredcutible}
    Sea $\k \subseteq \E$ una extensión de cuerpos y sea $\alpha \in \E$.
    Consideremos el morfismo de anillos
    \[
        \begin{aligned}
            \varphi \colon \k[x] &\to \E \\
            f &\mapsto f(\alpha).
        \end{aligned}
    \]
    Observamos que $\im(\varphi) = \left\{ a_0 + a_1\alpha + \cdots +
    a_n \alpha^n \mid a_i \in \k,\, n \geq 0\right\}$ es un subanillo de $\E$. Podemos estudiar dos casos en función del núcleo de $\varphi$.
    \begin{itemize}
        \item $\ker(\varphi) = 0$. En este caso, no existe ningún polinomio $f \in
            \k[x]$ no nulo que tenga $\alpha$ por raíz. Decimos que $\alpha$ es un
            elemento trascendental sobre $\k$.
        \item $\ker(\varphi) \neq 0$. Por ser $\k\lb x\rb$ un dominio de ideales principales, $\ker \lp \varphi \rp = \lp \Phi\lp x\rp \rp$. Decimos que $\Phi\lp x\rp$ es el polinomio mínimo irreducible de $\alpha$ en $\k\lb x\rb$ y lo notamos como $\Phi\lp x\rp = \Irr(\alpha, \k, x)$ (a menudo lo tomamos mónico para que sea único). Decimos también que $\alpha$ es algebraico sobre $\k$.
    \end{itemize}
\end{defi}

\begin{defi}[extensión!algebraica]\idx{extensión!trascendental}
    Sea $\k \subseteq \E$ una extensión. Decimos que es algebraica si para todo
    $\alpha \in \E$, $\alpha$ es algebraico sobre $\k$. Decimos que una
    extensión es trascendental cuando no es algebraica, es decir, cuando existe algún
    $\alpha \in \E$ tal que $\alpha$ es trascendental sobre $\k$.
\end{defi}

\begin{example}
    \begin{enumerate}[1)]
        \item[]
        \item $i \in \cx$ es algebraico sobre $\real$. $\Irr(i,
            \real, x) = x^2 + 1 \in \real[x]$.
        \item $\forall n \geq 1$, $e^{\frac{2\pi i}{n}} \in \cx$ es
            algebraico sobre $\real$ y anulado por $x^n - 1$. Por
            ejemplo. $n=4$, $\Irr\left(
            e^{\frac{2\pi i}{4}}, \real, x \right) = x^2 + 1 \neq
            x^4 -1$.
        \item $\sqrt[3]{2}$ es algebraico sobre $\q$, $\Irr\left(
            \sqrt[3]{2}, \q, x\right) = x^3 -2$.
        \item $e$ y $\pi$ son trascendentales sobre $\q$.
    \end{enumerate}
\end{example}

\begin{prop}
    Sea $\k \subseteq \E$ una extensión de cuerpos. Si $\k \subseteq \E$ es
    finita, entonces es algebraica.
\end{prop}
\begin{proof}
    Tenemos que $\left[ \E \colon \k \right] = n < \infty$. Tomamos un $\alpha \in
    \E$ cualquiera. Veamos que $\alpha$ es algebraico sobre $\k$.
    Consideremos los $n+1$ vectores $\left\{ 1, \alpha, \dots, \alpha^n \right\}$ del $\k$-e.v. $\E$. Son liniealmente dependientes porque la dimensión de este espacio vectorial es $n$. Por lo tanto existen $\lambda_0, \dots,
    \lambda_n \in \k$ tales que
    \[
        \lambda_0 + \lambda_1 \alpha + \cdots + \lambda_n \alpha^n = 0.
    \]
    Sea, pues, $f(x) = \lambda_0 + \lambda_1 x + \cdots + \lambda_n x^n
    \in \k[x]$, que anula a $\alpha$. Concluimos que la extensión $\E \subseteq \k$ es
    algebraica.
\end{proof}

\begin{teo*}
    Sea $\k$ un cuerpo y sea $f(x) \in \k[x]$ un polinomio de grado $n \geq 1$.
    Entonces, existe un cuerpo $\E$ tal que $\k \subseteq \E$, $\left[ \E \colon \k
    \right] \leq n$ y $f(x)$ tiene una raíz en $\E$. Es decir, $f(x) = (x-
    \alpha)f_1(x)$, para ciertos $\alpha \in \E$ y $f_1\lp x\rp \in \E[x]$.
\end{teo*}
\begin{proof}
    Sea $p(x)$ un factor primo de $f(x)$. Puesto que $\k\lb x\rb$ es un dominio
    de ideales principales y $\lp p\lp x\rp \rp$ es un ideal primo, $\lp p\lp
    x\rp \rp$ es un ideal maximal. Por lo tanto, $\E = \faktor{\k[x]}{\lp p(x)
    \rp}$ es un cuerpo. Además, $\k \subseteq \E$ y $\lb \E \colon \k \rb =
    \grad \lp p\lp x\rp \rp \leq n$. Si tomamos $\alpha = \class{x} \in \E$,
    tenemos que $f(\alpha) = p(\alpha)g(\alpha) = p\lp \class{x} \rp g\lp \alpha
    \rp = \class{p\lp x\rp}g\lp \alpha \rp = 0g(\alpha) = 0$, de modo que $\alpha$ es una raíz de $f\lp x\rp$ en $\E$.
\end{proof}

\begin{col}
    Sea $\k$ un cuerpo y sea $f(x) \in \k[x]$ un polinomio con grado $n \geq 1$. Entonces,
    existe un cuerpo $\E$ con $\k \subseteq \E$ y $\left[ \E \colon \k \right]
    \leq n!$ tal que $f(x)$ descompone totalmente en factores
    de grado $1$ en $\E[x]$.
\end{col}
\begin{proof}
    Procederemos por inducción sobre $n$. El caso base $n=1$ está claro,
    veamos ahora al caso general.

    Por el teorema anterior, existe un cuerpo $\E_1$ con $\k\subseteq \E_1$ y $\lb \E_1 \colon \k \rb \leq n$ tal que $f(x) = (x -
    \alpha) f_1(x)$, con $\alpha\in \E_1$ y $f_1\lp x\rp \in \E_1\lb x\rb$. Entonces, $f_1\lp x\rp$ tiene grado $n-1$ y, en virtud de la hipótesis de inducción, existe un cuerpo $\E$ con $\E_1\subseteq \E$ y $\lb \E \colon \E_1 \rb \leq \lp n-1\rp !$ tal que $f_1\lp x\rp$
        descompone totalmente en factores de grado $1$ en $\E[x]$. Por lo tanto, $f(x) = (x - \alpha)(x - \alpha_1)
        \cdots (x-\alpha_{n-1})$ en $\E[x]$ y $\k \subseteq \E_1 \subseteq \E$ satisfacen que
        \[
            \left[ \E \colon \k  \right] = \left[ \E \colon \E_1
            \right] \left[ \E_1 \colon \k \right] \leq (n-1)! n = n!.
        \]
\end{proof}

\begin{defi}[cuerpo!adjunción]
    Sea $\k \subseteq \E$ una extensión de cuerpos y sea $\alpha \in \E$.
    Consideremos el morfismo de anillos
    \[
        \begin{aligned}
            \varphi \colon \k[x] &\to \E \\
            f &\mapsto f(\alpha).
        \end{aligned}
    \]
    Definimos $\k[\alpha] \equiv \im \varphi$ y llamamos cuerpo de adjunción de $\k$ y $\alpha$ al menor
    subcuerpo de $\E$ que contiene a la vez a $\alpha$ y a $\k$, y lo notamos como $\k\lp \alpha\rp$.
\end{defi}

\begin{obs}
    Observamos que $\k[\alpha] \subseteq \k(\alpha)$. Sea $S=\k\lb \alpha\rb\setminus \lc 0\rc$ y consideremos el diagrama
    \begin{center}
        \begin{tikzpicture}
            \node (k) {$\k[\alpha]$};
            \node[right=3em of k] (f) {$\k(\alpha)$};
            \node[below=2em of k] (s) {$S^{-1}\k[\alpha]$};
            \draw[->] (k) to node[above] {$\Id$} (f);
            \draw[->] (k) to (s);
            \draw[->] (s) to node[below] {$f$} (f);
        \end{tikzpicture}
    \end{center}
    En virtud de la propiedad universal del simetrizado, existe un único morfismo de anillos $f\colon S^{-1}\k\lb \alpha\rb \to \k\lp \alpha\rp$, y es inyectivo porque es un morfismo de anillos que tiene por dominio un cuerpo. Además, la imagen por un morfismo de anillos de un cuerpo es un cuerpo, de modo de $\im f$ es un cuerpo que contiene a $\k\lb \alpha\rb$ y, en particular, a $\alpha$ y a $\k$. Pero $\k\lp \alpha\rp$ es el menor cuerpo que contiene a $\alpha$ y a $\k$, de modo que $f$ es exhaustivo. Por lo tanto, el cuerpo de
    fracciones de $\k[\alpha]$ es $\k(\alpha)$, es decir, $S^{-1}\k\lb \alpha\rb = \k\lp \alpha\rp$.
    
    Consideremos de nuevo el morfismo de anillos 
    \[
        \begin{aligned}
            \varphi \colon \k[x] &\to \E \\
            f &\mapsto f(\alpha).
        \end{aligned}
    \]
    y estudiemos dos casos en función del núcleo de $\varphi$.
    \begin{itemize}
        \item $\ker \varphi = \lc 0\rc$, es decir, $\alpha$ es trascendental sobre $\k$. En este caso, $\varphi$ es inyectiva y tenemos que $\k[x] \cong \im
            (\varphi) = \k[\alpha]$. Entonces, $\k(x) = \lp \k\lb x\rb \rp ^{-1} \k\lb x\rb \cong \lp \k\lb \alpha \rb \rp ^{-1} \k\lb \alpha \rb = \k(\alpha)$.
        \item $\ker \varphi \neq \lc 0\rc$, es decir, $\alpha$ es algebraico sobre $\k$. Por ser $\k\lb x\rb$ un dominio de ideales principales, $\ker \varphi = \varphi ^{-1}\lp \lc 0\rc \rp = \lp \Phi(x) \rp = \lp \Irr\lp \alpha, \k, x\rp \rp$ y, entonces, 
            \[
                \faktor{\k[x]}{\lp \Phi(x) \rp} \cong
                \im \varphi = \k[\alpha].
            \]
            Puesto que $\lc 0\rc$ es un ideal primo, también lo es su antiimagen $\lp \Phi\lp x\rp \rp$ y, puesto que $\k\lb x\rb$ es un dominio de ideales principales, $\lp \Phi\lp x\rp \rp$ es maximal. Así pues, $\k\lb \alpha\rb$ es un cuerpo y $\k[\alpha] = \k(\alpha)$. Además, $\left[ \k(\alpha) \colon \k \right] = \lb \k\lb \alpha\rb \colon \k \rb = \grad \lp \Phi\lp x\rp \rp = \grad\left(
            \Irr(\alpha, \k, x)\right)$.
    \end{itemize}
\end{obs}

\begin{defi}[extensión!simple]\idx{extensión!finitamente generada}
    Sea $\k\subseteq \E$ una extensión de cuerpos y sea $\alpha\in\E$. Decimos que $\k \subseteq \E$ es una extensión simple si $\E = \k(\alpha)$
    y decimos que $\k \subseteq \E$ es una extensión finitamente generada si
    $\E = \k\left( \alpha_1, \dots, \alpha_n \right)$.
\end{defi}

\begin{prop}
    Sea $\k \subseteq \E$ una extensión de cuerpos. Entonces, $\k \subseteq \E$
    es finita si y solo si es algebraica y finitamente generada.
\end{prop}
\begin{proof}
    Empecemos por la implicación directa. Ya hemos visto que si una extensión de cuerpos es finita, entonces es algebraica. 
    Veamos, pues, que también es finitamente generada. Procederemos por inducción sobre $n=\dim_{\k}\E$. Para $n = 1$,
    tenemos que $\E = \k$ y hemos acabado. Para el caso general $n>1$, tenemos que $\k\subset \E$ y que existe algún $\alpha \in \E \setminus \k$. Entonces,
    \[
        n = \lb \E \colon \k \rb = \lb \E \colon \k\lp\alpha \rp \rb \lb \k\lp \alpha\rp \colon \k \rb,
    \]
    dónde $\lb \k\lp \alpha\rp \colon \k \rb>1$. Así pues, $\lb \E \colon \k\lp\alpha \rp \rb<n$ y, en virtud de la hipótesis de inducción, $\E$ es finitamente generado y podemos escribir $\E = \k(\alpha)\left( \beta_2, \dots, \beta_m \right) =
    \k\left( \alpha, \beta_2, \dots, \beta_{m} \right)$.

    Veamos ahora la implicación invesa. Tenemos que $\E = \k\left(
    \alpha_1, \alpha,_2, \dots, \alpha_m \right)$ y que los $\alpha_i$ son algebraicos
    sobre $\k$. Sean $\E_0 = \k$ y $\E_i = \E_{i-1} \left( \alpha_i
    \right)$, para todo $1\leq i \leq m$. Entonces,
    \begin{gather*}
        \left[ \E \colon \k \right] = \left[ \E_m \colon \E_0 \right] =
        \left[ \E_m \colon \E_{m-1} \right] \cdots \left[ \E_1 \colon
        \E_0 \right] = \\ = \prod^m_{i=1}\left[ \E_i \colon \E_{i-1} \right] =
        \prod^m_{i = 1} \left[ \E_{i-1}\left( \alpha_i \right) \colon
        \E_{i-1} \right] = \prod^m_{i = 1} \grad \lp \Irr\left( \alpha_i,
        \E_{i-1}, x \right) \rp < \infty.
    \end{gather*}
\end{proof}

\begin{teo}[Teorema del elemento primitivo]
    Sea $\k \subseteq \E$ una extensión de cuerpos finita con $\car \k = 0$.
    Entonces, $\E$ es una extensión simple de $\k$, $\E = \k \lp \alpha\rp$, para algún $\alpha \in \E$.
\end{teo}
\begin{proof}
    En virtud de la proposición anterior, podemos decir que
    $\E = \k\left( \alpha_1, \dots, \alpha_n \right)$ para ciertos $\alpha_i\in \E$
    algebraicos sobre $\k$. Para terminar la demostración basta ver, pues, que si $\E = \k(\alpha,
    \beta)$ con $\alpha$ y $\beta$ algebraicos sobre $\k$, entonces existe algún
    $\gamma \in \E$ algebraico sobre $\k$ tal que $\E = \k(\gamma)$.

    Sea $f(x) = \Irr(\alpha, \k, x) \in \k \lb x\rb$, con $\grad(f\lp x\rp) = n \geq 1$. Como $\car
    \k = 0$, $f^{\prime}\lp x\rp \neq 0$ y naturalmente $f\lp x\rp \ndiv f^{\prime} \lp x\rp$. Además, $f^{\prime}\lp x\rp \ndiv f \lp x\rp$, ya que $f\lp x\rp$ es irreductible, lo que implica que $\mcd\left( f\lp x\rp, f^{\prime} \lp x\rp \right)$ $= 1$. Por
    ser $\k\lb x\rb$ un dominio de ideales principales, se satisface la identidad 
    de B\'ezout (\ref{defi:idenbezout}), es decir, que existen
    $ a\lp x\rp, b\lp x\rp \in \k[x]$ tales que $a\lp x\rp f\lp x\rp + b\lp x\rp f^{\prime}\lp x\rp = 1$.
    
    Consideremos análogamente $g\lp x\rp = \Irr(\beta, \k, x) \in \k\lb x\rb$, con $\grad\lp g\lp x\rp \rp = m \geq 1$.
    Sea $\mathbb{L}$, $\k \subseteq \E
    = \k(\alpha, \beta) \subseteq \mathbb{L}$ un cuerpo de descomposición de $f \lp x\rp$ y $g\lp x\rp$, es decir, que $f\lp x\rp$ y $g\lp x\rp$ descomponen
    en factores de grado $1$ en $\mathbb{L}[x]$. La identidad de Bézout
    $1 = a\lp x\rp f\lp x\rp + b\lp x\rp f{^\prime}\lp x\rp$ se satisface
    en $\mathbb{L}[x]$, cosa que implica que $f\lp x\rp$ y $f^{\prime}\lp x\rp$
    también son primos entre sí en $\mathbb{L}[x]$.

    Afirmamos que $f\lp x\rp$ no tiene raíces múltiples en $\mathbb{L}[x]$ puesto que,
    de lo contrario, $f\lp x\rp$ y $f^{\prime}\lp x\rp$ tendrían un factor 
    en común. Entonces, podemos escribir
    \[
        f(x) = \prod\limits^n_{i=1} (x -\alpha_i), \, \text{con }
        \alpha_1 = \alpha \text{ y todos los } \alpha_i \in
        \mathbb{L} \text{ distintos entre ellos.}
    \]
    Análogamente,
    \[
        g(x) = \prod\limits^m_{j=1} (x -\beta_j), \, \text{con }
        \beta_1 = \beta \text{ y todos los } \beta_i \in
        \mathbb{L} \text{ distintos entre ellos.}
    \]

    Sea $\lambda \in \k \setminus \left\{ \frac{\alpha_1 - \alpha_i}{\beta_j
    - \beta_1} \mid 1\leq i\leq n, 2\leq j\leq m \right\}$ un elemento cualquiera. 
    Recordemos que $\car \k =0 \implies \q \subseteq \k$, de modo que $\k$ es 
    infinito y $\lambda$ siempre existe. Pongamos ahora $\gamma =
    \alpha + \lambda\beta$ y veamos que $\k(\gamma) = \k(\alpha, \beta)$.
    Está claro que $\k(\gamma) \subseteq \k(\alpha, \beta) \subseteq \mathbb{L}\lb x\rb$ puesto que $\gamma =
    \alpha + \lambda\beta \in \k(\alpha, \beta)$. Veamos la otra inclusión.

    Basta comprobar que $\beta \in \k(\gamma)$ ya que, entonces, $\alpha =
    \gamma -\lambda\beta \in \k(\gamma)$. Puesto que $\beta$ es algebraico
    sobre $\k$ y $\k \subseteq \k(\gamma)$, $\beta$ es algebraico sobre
    $\k(\gamma)$. Sea $h(x) = \Irr\left(\beta, \k(\gamma), x \right)\in \k \lp \gamma\rp \lb x\rb \subseteq \mathbb{L}\lb x\rb$.
    Sabemos que $g(x) \in \k[x] \subseteq \k(\gamma)[x] \subseteq \mathbb{L}\lb x\rb$ anula a
    $\beta$ y que $h(x)$ es el mínimo polinomio que anula a $\beta$,
    de manera que $h(x) \vert g(x)$ en $\k\lp\gamma\rp\lb x\rb \subseteq \mathbb{L}\lb x\rb$.
    Así pues, en $\mathbb{L}\lb x\rb$, $h\lp x\rp$ es de la forma
    \[
        h(x) = \prod\limits^r_{j=1}\left( x - \beta_{i_j} \right) \,
        \text{con } m \geq r \geq 1 \text{ y } m\geq i_j \geq 1
        \text{ distintos entre ellos}.
    \]
    Si vemos ahora que $h(x) = x - \beta_1 = x - \beta \in \k(\gamma)[x]$,
    entonces $\beta \in \k(\gamma)$ y habremos terminado.

    Consideremos el polinomio $f(\gamma - \lambda x) \in \k(\gamma)[x] \subseteq \mathbb{L}\lb x\rb$. Sabemos que $\beta$
    es raíz de $f(\gamma -\lambda x)$ porque $f(\gamma -\lambda\beta) =
    f(\alpha) = 0$, lo que implica que $h(x) \vert f(\gamma - \lambda\beta)$.
    Supongamos ahora que $r > 1$. Tenemos que $\beta_2 \in \mathbb{L}$ es una raíz de $h(x) \in \mathbb{L}\lb x\rb$ y,
    por lo tanto, de $f(\gamma -\lambda x) \in \mathbb{L}\lb x\rb$. O, dicho de otra forma, $\gamma
    -\lambda\beta_2$ es una raíz de 
    \[
        f\lp x\rp = \prod\limits^n_{i=1} \left( x -\alpha_i \right)
    \]
    en $\mathbb{L}\lb x\rb$. Esto implica que $\gamma -\lambda\beta_2 = \alpha_i$ para cierto
    $1 \leq i \leq n$ y se tiene que
    \[
        \alpha_1 + \lambda\beta_1 - \lambda\beta_2 = \alpha_i 
        \implies \lambda = \frac{\alpha_1 - \alpha_i}{\beta_2 - \beta_1},
    \]
    lo cual supone una contradicción con la definición de $\lambda$ y concluimos que 
    $h\lp x\rp = x-\beta$, acabando la demostración.
\end{proof}

\section{Cuerpos finitos}
Recordemos antes de comenzar esta sección que $p$ denota un primo natural y que $\F_p = \z$/$p\z$.

\begin{prop}\label{prop:simple}
    Sea $\F$ un cuerpo finito. Entonces,
    \begin{enumerate}[i)]
        \item $\car \F = p$ y $\abs{\F} = p^n$ con $n \geq 1$. Lo notaremos como $\F =\F_q$, siendo $q =p^n$.
        \item $\F = \F_p(\alpha)$ para cierto $\alpha$ algebraico sobre $\F_p$,
            es decir, $\F$ es una extensión simple de $\F_p$.
        \item Para todo $\alpha\in \F$, $\alpha$ es un raíz del polinomio $x^q-x$. Además, si $\k\subseteq \E$ es una extensión de cuerpos y $\alpha \in \E$ es una raíz de $x^q-x$, entonces $\alpha \in \F$.
\end{enumerate}
\end{prop}
\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item Como $\F$ es finito, $\car \F = p$.
            Por lo tanto, $\F_p \subseteq \F$ y $\F$ es un $\F_p$-e.v.
            Como $\F$ es finito, $\dim_{\F_p} \F = \left[ \F \colon
            \F_p \right] = n < \infty$. Existe, pues, una base $v_1, \dots,
            v_n$ de $\F$ (como $\F_p$-e.v.) y concluimos que $\F =
            \left\{ \text{combinaciones lineales de $v_1, \dots, v_n$ con coeficientes
            en $\F_p$}\right\}$, que tiene cardinal $p^n$.
        \item Ya hemos visto que un subgrupo finito del grupo
            multiplicativo de un cuerpo es cíclico. En particular,
            si $\F$ es finito, $\F^\ast$ es cíclico: $\F^\ast =
            \langle\alpha\rangle = \left\{ 1, \dots, \alpha^m
            \right\}$. Tenemos $\F = \F^\ast \cup \left\{ 0
            \right\} = \left\{ 0, 1, \alpha, \dots, \alpha^m
            \right\} \subseteq \F_p(\alpha)$ y concluimos que $\F =
            \F_p(\alpha)$ es una extensión simple y algebraica.
        \item Claramente $0\in \F$ es una raíz de $x^q-x$.
            Sea $\alpha \in \F^\ast$ un elemento invertible cualquiera. $\F^\ast$ es un grupo de orden $q-1$
            y se tiene que
            $\alpha^{q} = \alpha^{q-1} \alpha  = 1 \alpha = \alpha$.
            Es decir, $\forall \alpha \in \F$, $\alpha$ es raíz de $x^q -x$.
            Sea $\k\subseteq \E$ es una extensión de cuerpos y $\alpha \in \E$. 
            Entonces, $q = \abs{\F} \leq \abs{\lc \alpha \in \E \mid \alpha \text{ es una raíz de } x^q - x \rc} \leq q$
            y sigue la igualdad.
    \end{enumerate}
\end{proof}

\begin{teo*}
    Sea $q = p^n$, con $p$ primo y $n \geq 1$. Existe un cuerpo de
    cardinal $q$.
\end{teo*}
\begin{proof}
    Sea $f(x) = x^q -x \in \F_p[x]$ y sea $\E$ un cuerpo de
    descomposición de $f(x)$. Tenemos que $f^\prime(x) = q x^{q-1}-1 = -1$, puesto que
    $q = p^n = 0$, y sigue que $\mcd\left( f, f^\prime \right) = 1$. 
    Deducimos, pues, que $f\lp x\rp$ no tiene raíces múltiples, es decir, que
    \[
        f\lp x\rp = \lp x-\alpha_1\rp \cdots \lp x-\alpha_q\rp,
    \]
    con $\alpha_1, \dots, \alpha_q \in \E$. Veamos que $\F = \lc \alpha_i \mid 1\leq i\leq q\rc$
    es un cuerpo. Sean $\alpha, \beta \in \F$ elementos cualesquiera.
    \begin{itemize}
        \item $\begin{aligned}[T]
                (\alpha +\beta)^q = \alpha^q + \dot{p} +
                \beta^q = \alpha^q + \beta^q = \alpha + \beta
                \implies \alpha+\beta \in \F.
                \end{aligned}$
        \item $(\alpha\beta)^q = \alpha^q\beta^q = \alpha\beta
                \implies \alpha\beta\in\F.$
        \item Distingamos en función de la paridad de $q$, que es la misma que la de $p$.
            Si $q$ es impar, $(-\alpha)^q = (-1)^q\alpha^q = -\alpha$. Si $q$ es par, $p=2$
            y $(-\alpha)^q = (-1)^q\alpha^q = \alpha^q = -\alpha$. Por lo tanto, $-\alpha \in \F$.
        \item $\left( \alpha^{-1} \right)^q = \left( \alpha^q
                \right)^{-1} = \alpha^{-1} \implies
                \alpha^{-1} \in \F$.
    \end{itemize}
    Por lo tanto, $\F$ es un cuerpo de cardinal $q$.
\end{proof}
\begin{col}
    Para todo $n \geq 1$, existe un polinomio en $\F_p[x]$ irreductible y de
    grado $n$.
\end{col}
\begin{proof}
    Dado $n \geq 1$, sea $\F_q$ un cuerpo de cardinal $q=p^n$ (que existe
    por el teorema anterior). En virtud de la proposición \ref{prop:simple}, $\F_q$ es
    una extensión simple algebraica de $\F_p$. Es decir,
    \[
        \F_q = \F_p(\alpha) \cong \faktor{\F_p[x]}{\langle \Phi_\alpha(x)
        \rangle},
    \]
    donde $\Phi_\alpha(x) = \Irr(\alpha, \F_p, x)$. 
    $\Phi_\alpha(x)$
    es mónico y $\grad\left( \Phi_\alpha \right) = \left[ \F_q \colon \F_p \right] = n
    $, porque $q = p^n$.
\end{proof}

\begin{col}\label{col:col2}
    Sea $p(x) \in \F_p[x]$ un polinomio irreductible de grado $n$. Entonces,
    $p(x) \vert f(x) = x^q - x$ en $\F_p[x]$, donde $q= p^n$.
\end{col}
\begin{proof}
    Sabemos que $\F_q = \faktor{\F_p[x]}{\langle p(x) \rangle}$.
    Consideremos $\alpha = \class{x} \in \F_q$. Se tiene que
    $\alpha^{q-1} = 1$ porque $\F^\ast_q$ es un grupo de orden $q-1$. Por lo
    tanto, $\alpha^q = \alpha$, lo que implica que $\alpha$ es una raíz de $f\lp x\rp$.
    $\alpha$ también es raíz de $p(x)$, ya que
    \[
        p(\alpha) = p\left( \class{x} \right) = \class{p(x)} = 0.
    \]
    Finalmente, por ser $p\lp x\rp$ irreductible, $p\lp x\rp \vert f\lp x\rp$.
\end{proof}

\begin{teo*}
    Sean $\F$ y $\mathbb{G}$ dos cuerpos del mismo cardinal $q = p^n$.
    Entonces, $\F \cong \mathbb{G}$, es decir, existe un morfismo de anillos
    biyectivo entre ellos.
\end{teo*}
\begin{proof}
    Sabemos que $\F = \F_p(\alpha) \cong \faktor{\F_p[x]}{\langle
    \Phi_\alpha(x) \rangle}$, siendo $\Phi_\alpha(x) = \Irr(\alpha, \F_p, x)$
    un polinomio irreductible de grado $n$ en $\F_p[x]$, para cierto $\alpha\in \F$.

    El corolario anterior nos asegura que $\Phi_\alpha(x) \mid x^q -x$ en $\F_p$ y,
    como hemos visto en la proposición \ref{prop:simple}, $\mathbb{G} = \left\{ \text{raíces de } x^q-x \right\}$.
    Así pues, $x^q-x$ descompone totalmente en $\mathbb{G}$, de modo que podemos encontrar un $\beta \in \mathbb{G}$
    que anule $\Phi_\alpha(x)$. Esto implica que $\Phi_\alpha(x) = \Irr(\beta, \F_p, x) = \Phi_\beta(x)$.
    Concluimos, pues, que
    \[
        \mathbb{G} = \F_p(\beta) \cong \faktor{\F_p[x]}{\langle
        \Phi_\beta(x) \rangle} = \faktor{\F_p[x]}{\langle
        \Phi_{\alpha(x)} \rangle} \cong \F_p\left( \alpha
        \right) = \F.
    \]
\end{proof}
