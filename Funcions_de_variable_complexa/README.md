Apunts a latex de l'assignatura. Si no ens ajudes ja, fes-ho! Si som més, és més fàcil.

Si ja ens ajudes, recorda llegir les recomanacions d'estil i les normes sobre com
fer commits. Moltes gràcies.

## TODO

 - [ ] Al tema 1 la projecció estereogràfica és un pi en minúscula.
 - [ ] Canviar els ". Y por ..."
 - [ ] Passar les D de disc a mathcal.
 - [ ] Passar les U d'oberts a mathcal.
 - [ ] Passar les d a deltes.
 - [ ] Acabar les frases amb un punt a temes 3 i 4.
