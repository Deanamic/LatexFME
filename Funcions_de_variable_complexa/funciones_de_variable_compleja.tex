\chapter{Funciones de variable compleja}
\section{Funciones componentes, límites y continuidad}
\begin{defi}[función!de variable compleja]
	Llamamos función de variale compleja a una función $f$ definida sobre un
	conjunto $U \subseteq \cx$ abierto, es decir
	\[
		\begin{aligned}
			f \colon U \subseteq \cx &\to \cx \\
			z &\mapsto f(z) = w.
		\end{aligned}
	\]
\end{defi}
\begin{obs}
	Se tiene que $f(z) = u(z) + i v(z)$, donde $u,v \colon U \to \real$ son
	funciones de variable compleja y valores reales. Notamos así
	\[
		u = \re\left( f \right), \quad v = \im\left( f \right).
	\]
\end{obs}
\begin{defi*}
    Definimos el límite de las funciones de variable compleja de forma análoga a
    la del resto de funciones, diremos que
    \[
        \lim_{z \to z_0} f(z) = w_0 \iff \forall \varepsilon > 0 \exists \delta
        > 0 \tq \abs{z - z_0} \leq \delta \implies \abs{f(z) - w_0} <
        \varepsilon.
    \]
\end{defi*}
\begin{obs}
    Aunque tan solo definimos el límite, el resto de definiciones son análogas a
    las definiciones del análisis real.
\end{obs}
\section{Polinomios y funciones racionales}
\begin{example}
    El ejemplo más simple de funciones de variable compleja son los polinomios
    \[
        p(x) = a_0 + a_1 x + \cdots + a_n x^n \in \cx[x].
    \]
    Estas funciones están bien definidas y son continuas $\forall z \in \cx$.

    Otro ejemplo muy común son las funciones racionales, definidas como $R(x) =
    \frac{p(x)}{q(x)} \in \cx(x)$ donde $p(x), q(x) \in \cx[x]$. Estas funciones
    están bien definidas y son continuas $\forall z \in \cx \setminus \left\{
    \text{raices de }q(z) \right\}$.
\end{example}
\begin{defi}[transformaciones de Möbius]
    Las transformaciones de Möbius son funciones racionales de la forma
    \[
        f(z) = \frac{az + b}{cz+d},
    \]
    con $a, b, c, d \in \cx$ y $ad - bc \neq 0$.
\end{defi}
\begin{obs}
    Podemos ampliar la definición a $\overline{\cx}$ poniendo $f(\infty) =
    \frac{a}{c}$ y $f\left( -\frac{d}{c} \right) = \infty$.
\end{obs}
\begin{obs}
    Podemos hacer una identificación entre las transformaciones de Möbius y
    $\text{GL}_2(\cx)$ obtenida naturalmente de la identificación
    \[
        f(z) = \frac{az + b}{cz + d} \leftrightarrow
        \begin{pmatrix}
            a & b \\ c & d
        \end{pmatrix}
        \in \text{GL}_2(\cx).
    \]
    De esta identificación obtenemos las siguientes propiedades:
    \begin{itemize}
        \item Cualquier transformación de Móbius es composición de 
            \[
                \begin{aligned}
                    z &\mapsto z + b &\leftrightarrow
                    \begin{pmatrix}
                        1 & b \\ 0 & 1
                    \end{pmatrix},
                    \\
                    z &\mapsto az ( a \neq 0) &\leftrightarrow
                    \begin{pmatrix}
                        a & 0 \\ 0 & 1
                    \end{pmatrix},
                    \\
                    z &\mapsto \frac{1}{z} &\leftrightarrow
                    \begin{pmatrix}
                        0 & 1 \\ 1 & 0
                    \end{pmatrix}.
                \end{aligned}
            \]
        \item Dados $z_1, z_2, z_3, w_1, w_2, w_3 \in \cx$ existe una única $f$
            transformación de Möbius tal que $f\left( z_i \right) = w_i$ $(i =
            1,2,3)$.
        \item Una transformación de Möbius envía rectas a rectas y
            circunferencias a circunferencias.
        \item Con la topología de $\overline\cx$ inducida por $\Sph^2$ las
            transformaciones de Möbius son funciones continuas.
    \end{itemize} 
\end{obs}
\begin{prop}
    Sea $K \subseteq \real^n$ ($n \geq 1$) un conjunto compacto y sea $f \colon
    K \to \real$ continua. Entonces $f$ toma un valor máximo y un valor mínimo
    en $K$.
\end{prop}
\begin{prop}
    Sea $f(x) \in \cx(x)$ una función, $f(x)$ tiene alguna raíz
    compleja $\iff cf(ax + b)$ tiene alguna raíz. Con $a, c \in \cx^\ast$ y $b
    \in \cx$.
\end{prop}
\begin{proof}
    Si $\alpha \in \cx$ es raíz de $f$, entonces se tiene que $f(\alpha) = 0$ y
    que tomando $\beta = \frac{\alpha - b}{a}$ se tiene
    \[
        c f(a \beta + b) = c f(\alpha) = 0.
    \]
    Por otro lado, si $\alpha$ es raíz de $cf(ax+ b)$ entonces $\beta =
    a\alpha + b$ cumple que
    \[
        f(\beta) = f\left( a\alpha + b \right) = \frac{0}{c} = 0.
    \]
\end{proof}
\begin{prop}
    Sea $f(z) = a_0 + a_1 z + \cdots + a_n z^n$, $n \geq 1$, $a_n \neq 0$ un
    polinomio no constante, entonces la función $\abs{f(z)}$ tiene un valor
    mínimo en $\cx$.
\end{prop}
\begin{proof}
    Se tiene que
    \[
        \lim_{z \to \infty} \abs{f(z)} = \lim_{z \to \infty} \abs{z^n}
        \abs{\frac{f(z)}{z^n}} = \lim_{z \to \infty} \abs{z^n} \abs{a_n} =
        \infty.
    \]
    Por lo tanto $\exists r > 0$ tal que $\abs{z} > r
    \implies \abs{f(z)} > \abs{a_0}$. Consideramos ahora $K = \overline{D}(0; r)$
    el disco cerrado de centro 0 y radio $r$. Sea $m = \min \left\{ \abs{f(z)}
    \mid z \in K\right\}$ que existe por ser $K$ compacto. Entonces $m
    \leq \abs{f(0)} = \abs{a_0}$ y por lo tanto, $m = \min \left\{ f(z) \mid z
    \in \cx \right\}$.
\end{proof}
\begin{teo*}[Teorema fundamental del álgebra]
    Todo polinomio no constante, $f$, con coeficientes complejos tiene alguna raíz
    compleja.
\end{teo*}
\begin{proof}
    Sabemos por la proposición anterior que $\abs{f(x)}$ tiene un valor mínimo $m$. Sea
    $z_0 \in \cx$ tal que $\abs{f\left( z_0 \right)} = m$, consideramos ahora la
    función $f\left( x + z_0 \right) = b_0 + b_1 x + \cdots + b_n x^n$, podemos
    suponer que el valor mínimo se toma en el punto 0 y que por tanto este valor
    es $\abs{b_0}$. Si $b_0 = 0$ hemos acabado ya que $m = 0$ y $f$ tiene una
    raíz en $z_0$. Supondremos que $b_0 \neq 0$ y llegaremos a contradicción.

    Consideraremos la función $\frac{1}{b_0} f\left( x + z_0 \right)$ (que está
    bien definida porque $b_0 \neq 0$) que toma el valor mínimo en 0 y vale 1.
    Podemos por tanto reescribir el polinomio como $\frac{1}{b_0}f(x + z_0) = 1
    + c_h x^h + \cdots + c_n x^n$ donde $h$ es el primer índice con $c_h \neq 0$
    ($1 \leq h \leq n$). Consideramos por último el polinomio
    \[
        \frac{1}{b_0} f\left( \frac{1}{\sqrt[h]{-c_h}} x +
        \frac{z_0}{\sqrt[h]{-c_h}} \right) = 1 - x^h +
        \underbrace{d_{h+1}x^{h+1} + \cdots + d_n x^n}_{x^hg(x)}.
    \]
    Siendo $g(x) \in \cx[x]$ un polinomio que cumple que $\lim_{z \to 0} g(z) =
    0$. Sea $t \in (0, 1)$ tal que $\abs{g(t)} < 1$. Entonces
    \[
        \begin{aligned}
            \abs{\frac{1}{b_0} f\left( \frac{1}{\sqrt[h]{-c_h}} t +
            \frac{z_p}{\sqrt[h]{-c_h}} \right)} &= \abs{1-t^h + t^hg(t)} \leq \\
            &\leq \abs{1-t^h} + t^h\abs{g(t)} = \\ &= 1 - t^h + t^h\abs{g(t)} <
            \\&< 1 - t^h + t^h.
        \end{aligned}
    \]
    Lo cual supone una contradicción con el hecho de que el valor mínimo era 1.
\end{proof}
\section{Sucesiones y series de funciones}
\begin{defi}[sucesión de funciones]
    Definimos una sucesión de funciones como una familia ordenada de funciones
    $f_i \colon U \to \cx$ ($i \geq 0$) con $U \subseteq \cx$ abierto. La notaremos como
    $\left( f_n \right)_{n \geq 0}$.
\end{defi}
\begin{defi}[convergencia!puntual]
    Dada una sucesión de funciones $\left( f_n \right)_{n \geq 0}$ diremos que
    tiende puntualmente hacia $f$ y lo notaremos como $\left( f_n
    \right)_{n \geq 0} \to f$ si $\left( f_n(z) \right)_{n \geq 0} \to f(z),\,
    \forall z \in U$.
\end{defi}
\begin{defi}[convergencia!uniforme]
    Dada una sucesión de funciones $\left( f_n \right)_{n \geq 0}$ diremos que
    tiende uniformemente hacia $f$ si $\forall \varepsilon > 0 \,\exists N$ tal
    que $n \geq N \implies \abs{f_n(z) - f(z)} < \varepsilon$, $\forall z \in U$.
\end{defi}
\begin{defi}[convergencia!uniforme sobre compactos]
    Dada una sucesión de funciones $\left( f_n \right)_{n \geq 0}$ diremos que
    tiende uniformemente sobre compactos hacia $f$ si $\forall K \subseteq U$
    compacto, $\left( f_n \right)_{n\geq 0}$ converge uniformemente hacia $f$
    sobre $K$.
\end{defi}
\begin{example}
    $f_n = x^n$ no converge uniformemente en $[0, 1]$, pero converge
    uniformemente sobre compactos en $(0, 1)$.
\end{example}
\begin{teo*}
    Sea $U \subseteq \cx$ abierto y $\left( f_n \right)$ una sucesión que
    converge uniformemente sobre compactos hacia $f$. Entonces $f_n$ continua
    $\forall n \implies f$ continua.
\end{teo*}
\begin{defi}[serie!de funciones]
    Definimos una serie de funciones como un par de sucesiones de funciones
    $\left( f_n \right)$ y $\left( s_n \right)$ relacionadas de la forma
    \[
        s_n(x) = \sum^n_{j = 0} f_n(x).
    \]
    Denotaremos las series como $\sum^\infty_{n=0} f_n$ o simplemente $\sum
    f_n$. Llamaremos suma parcial $n$-\'esima de la serie a $s_n$ y t\'ermino
    $n$-\'esimo de la serie a $f_n$. 
\end{defi}
\begin{defi}[serie!convergente]\idx{serie!absolutamente convergente}
    Diremos que una serie $\sum f_n$ es convergente si la sucesión de sumas
    parciales $\left( s_n \right)$ es convergente y diremos que una serie es
    absolutamente convergente si la serie $\sum \abs{f_n}$ es convergente.
    Además, cuando la serie sea convergente llamaremos suma de la serie a la
    función límite de $\left( s_n \right)$.
\end{defi}
\begin{obs}
    La convergencia (absoluta o no) de una serie puede ser tanto puntual como
    uniforme como uniforme sobre compactos.
\end{obs}
\begin{prop}
    Si una serie es absolutamente convergente puntualmente, entonces es
    convergente puntualmente. Análogamente si una serie es absolutamente
    convergente uniformemente entonces es convergente uniformemente.
\end{prop}
\begin{prop}[Criterio M de Weierstrass]\label{prop:criterioM}
    Dadas $f_n \colon U \to \cx$ ($n \geq 0$). Si $\abs{f_n(z)} \leq M_n \,\forall
    z \in U$ y $\sum^\infty_{n = 0} M_n$ es convergente, entonces $\sum f_n$ es
    absolutamente y uniformemente convergente en $U$.
\end{prop}
\begin{defi}[serie!de potencias]
    Una serie de potencias es una serie de funciones $\sum f_n$ donde $f_n = a_n
    \left( z - z_0 \right)^n$. Llamaremos a $z_0 \in \cx$ el centro de la serie
    y llamaremos coeficiente $n$-\'esimo a $a_n$.
\end{defi}
\begin{obs}
    Realizando el cambio $w = z - z_0$ envíamos el centro a 0 y simplificamos el
    estudio de series de potencias.
\end{obs}
\begin{defi}[radio de convergencia]
    Dada una serie de potencias $\sum a_n \left( z - z_0 \right)^n$, definimos
    su radio de convergencia como
    \[
        R = \left( \limsup_{n \to \infty} \sqrt[n]{\abs{a_n}} \right)^{-1} \in
        [0, \infty].
    \]
\end{defi}
\begin{obs}
    Recordemos que
    \[
        \limsup \sqrt[n]{\abs{a_n}} = \inf \left\{ \sup \left\{
        \sqrt[n]{\abs{a_n}} \right\} \right\} = \sup \left\{ \text{límites de
        las sucesiones parciales} \right\}.
    \]
\end{obs}
\begin{teo}[Teorema de Cauchy-Hadamard]
    Sea $\sum a_n \left( z - z_0 \right)^n$ una serie de potencias. Entonces la
    serie es absolutamente convergente si $\abs{z - z_0} < R$ y divergente si
    $\abs{z - z_0} > R$ donde $R$ es el radio de convergencia. Por lo tanto, la
    serie de funciones define una función en $\text{D}\left( z_0; R \right)$.
\end{teo}
\begin{proof}
    Para la demostración emplearemos el criterio de la raíz:
    \[
        \begin{aligned}
            \limsup_{n \to \infty} \sqrt[n]{\abs{a_n \left( z - z_0 \right)^n}}
            &= \limsup_{n \to \infty} \sqrt[n]{\abs{a_n}} \abs{z-z_0} =\\&= \abs{z -
            z_0} \limsup_{n \to \infty} \sqrt[n]{\abs{a_n}} =\\&= \abs{z - z_0}R^{-1}.
        \end{aligned}
    \]
    Por lo tanto el resultado queda demostrado.
\end{proof}
\begin{prop}
    Toda serie de funciones $\sum a_n \left( z - z_0 \right)^n$ es absoluta y
    uniformente convergente sobre compactos $K \subseteq \text{D}\left( z_0; R
    \right)$.
\end{prop}
\begin{proof}
    Sea $r = \max\left\{ \abs{z - z_0} \mid z \in K \right\}$ que existe por ser
    $K$ compacto, además este valor se alcanza para algún $z_1 \in K \subseteq
    U$. Sea $M_n = \abs{a_n}r^n$, que satisface que
    \[
        \abs{f_n(z)} = \abs{a_n\left( z - z_0 \right)^n} \leq \abs{a_n} r^n =
        M_n\, \forall z \in K.
    \]
    Se tiene ahora que $\sum M_n$ es convergente ya que $r < R$ y por el
    \hyperref[prop:criterioM]{Criterio M de Weierstrass}, $\sum a_n \left( z -
    z_0 \right)^n$ es absoluta y uniformemente convergente en $K$.
\end{proof}
\begin{col}
    $f(z) = \sum^\infty_{n=0}a_n\left( z-z_0 \right)^n$ es continua en
    $\text{D}\left( z_0; R \right)$.
\end{col}
\begin{example}
    La serie de potencias más básica que podemos tomar como ejemplo es la serie
    $\sum z^n$, donde $a_n = 1 \,\forall n$ y $z_0 = 0$, observamos que
    $R = \left( \limsup_{n \to \infty} \sqrt[n]{\abs{1}} \right)^{-1} = 1$ y por
    lo tanto define una función en $\text{D}\left( 0; 1 \right)$. Tambi\'en es
    interesante notar que la función no está definida en la frontera (ya que los
    t\'erminos de la serie no tienden a 0).

    Observamos por otro lado que
    \[
        \sum^N_{n=0} z^n = \frac{1 - z^{N+1}}{1 - z} \stackrel{\abs{z} < 1}{\to}
        \frac{1}{1-z}.
    \]
    Y que por lo tanto la función suma es $f(z) = \frac{1}{1-z}$ que está
    definida en $\cx \setminus \left\{ 1 \right\}$. Es interesante observar cuál
    es la expansión en series de potencias de esta función en otros puntos. Para
    ello tomaremos $z_0 \neq 1$ y se tiene
    \[
        \begin{aligned}
            \frac{1}{1-z} &= \frac{1}{1 - z_0 - \left( z - z_0 \right)} =\\&=
            \frac{1}{1-z_0} \left( \frac{1}{1-\left( \frac{z-z_0}{1-z_0}
            \right)} \right) = \\&= \frac{1}{1-z_0} \sum_{n \geq 0} \left(
            \frac{z - z_0}{1 - z_0} \right)^n =\\&= \sum_{n \geq 0}
            \frac{1}{\left( 1 - z_0 \right)^{n+1}} \left( z - z_0 \right)^n.
        \end{aligned}
    \]
    Si calculamos ahora el radio de convergencia, obtenemos
    \[
        R = \left( \limsup_{n \to \infty} \sqrt[n]{\frac{1}{\abs{1 - z_0}^{n+1}}}
        \right)^{-1} = \left( \limsup_{n \to \infty}
        \sqrt[n]{\frac{1}{\abs{1-z_0}}} \frac{1}{\abs{1-z_0}} \right)^{-1} = \abs{1-z_0}.
    \]
    Es interesante notar que el radio de convergencia de la serie es tan grande
    como la distancia desde el centro a la única discontinuidad (que es el 1).
    \begin{center}
        \begin{tikzpicture}
            \begin{axis}[
                    ticks=none,
                    axis equal,
                    xmin = -2, xmax = 11,
                    ymin = -9, ymax = 3
                ]
                \draw[fill=red, opacity=0.5] (axis cs:0,0) circle [radius=1];
                \draw[fill=green,opacity=0.5] (axis cs:5,-3) circle [radius=5];
                \addplot[mark=*,blue] coordinates {(0,0)}
                node[auto,pin=180:$z_0$] {};
                \addplot[mark=*,blue] coordinates {(5, -3)}
                node[auto,label=$z_1$] {};
                \addplot[mark=*,white] coordinates {(1, 0)};
                \addplot[mark=o] coordinates{(1, 0)};
            \end{axis}
        \end{tikzpicture}
    \end{center}
\end{example}
\begin{example}
    Partiremos ahora de la función
    \[
        f(z) = \frac{1}{1+z^2} = \frac{1}{1 - \left( -z^2 \right)} =
        \sum^\infty_{n=0} \left( -z^2 \right)^n = \sum^\infty_{n=0} (-1)^n
        z^{2n} = \sum^\infty_{n=0} a_n z^n
    \]
    donde $a_n = (-1)^{n/2}$ si $n$ es par y $a_n = 0$ si $n$ es impar. Vemos
    ahora que
    \[
        R = \limsup_{n \to \infty} \sqrt[n]{\abs{a_n}} = 1.
    \]
\end{example}
\begin{defi}[exponencial comleja]
    Definimos la exponencial compleja $e^z$ como la función suma de la serie de
    potencias $\sum \frac{z^n}{n!}$.
\end{defi}
\begin{obs}
    La exponencial compleja está bien definida en todo $\cx$:
    \[
        R = \left( \limsup_{n \to \infty} \sqrt[n]{\abs{\frac{1}{n!}}}
        \right)^{-1} = 0^{-1} = \infty.
    \]
\end{obs}
\begin{prop}[Fórmula de Euler]
    Dado $t \in \real$, se tiene que $e^{it} = \cos t + i \sin t$.
\end{prop}
\begin{proof}
    \[
        \begin{aligned}
            e^{it} = \sum^\infty_{n=0} \frac{(it)^n}{n!} &=
            \sum^\infty_{k=0} \frac{(it)^{2k}}{(2k)!} +
            \sum^\infty_{k=0}\frac{(it)^{2k+1}}{(2k+1)!} =\\&=
            \sum^\infty_{k=0} \frac{(-1)^k}{(2k)!}t^{2k} + i
            \sum^\infty_{k=0} \frac{(-1)^k}{(2k+1)!}t^{2k+1} = \\ &=
            \cos t + i \sin t.
        \end{aligned}
    \]
\end{proof}
\begin{prop}
    Se tiene que $\forall z, w \in \cx$, $e^{z + w} = e^z+ e^w$.
\end{prop}
\begin{proof}
    \[
        \begin{aligned}
            e^ze^w &= \left( \sum^\infty_{n=0} \frac{z^n}{n!} \right) \left(
            \sum^\infty_{n=0} \frac{w^n}{n!} \right) =
            \sum^\infty_{n=0} \left( \sum^n_{k=0} \frac{z^k}{k!}
            \frac{w^{n-k}}{(n-k)!} \right) =\\&= \sum^\infty_{n=0}
            \frac{1}{n!} \sum^n_{k=0} \frac{n!z^kw^{n-k}}{k!(n-k)!} =
            \sum^\infty_{n=0} \frac{(z+w)^n}{n!} = e^{z+w}.
        \end{aligned}
    \]
\end{proof}
\begin{col}
    Tenemos que $e^{x+iy} = e^xe^{iy} = e^x\left( \cos y + i\sin y \right)$ y que
    $e^z = u(z) + iv(z)$ donde $u(z) = e^x\cos y$ y $v(z) = e^x\sin y$.
\end{col}
\begin{prop}
    \begin{enumerate}[i)]
    \item[]
    \item $e^z$ es una función periódica de periodo $2\pi i$.
    \item $e^z = 1 \iff z \in 2\pi i \z$.
    \item $e^z = e^w \iff z \in w + 2\pi i \z$.
    \item $\Im\left( e^z \right) = \cx^\ast$ (toma todos los valores complejos excepto el 0).
    \item Rectas horizontales en el plano complejo pasan a semirectas que pasan
        por el origen al aplicar $e^z$. Las rectas verticales pasan a ser
        circunferencias y un rectángulo en $\cx$ pasa a ser un sector de corona
        circular.
    \end{enumerate}
\end{prop}
\begin{defi}[seno complejo]\idx{coseno complejo}
    Igual que hemos definido la exponencial compleja como una serie de
    potencias, podemos definir las funciones trigonometricas de la siguiente
    forma:
    \[
        \cos z = \sum^\infty_{n=0} \frac{(-1)^n}{(2n)!}z^{2n}, \quad
        \sin z = \sum^\infty_{n=0} \frac{(-1)^n}{(2n+1)!}z^{2n+1}.
    \]
\end{defi}
\begin{obs}
    Una definición equivalente que no requiere del uso de series de potencias es
    la siguiente: $\cos z = \frac{e^{iz} + e^{-iz}}{2}$ y $\sin z =
    \frac{e^{iz} - e^{-iz}}{2}$.
\end{obs}
\begin{obs}
    El seno y el coseno tienen un periodo de $2\pi$.
\end{obs}
\begin{defi}[logaritmo]
    La función $e^z \colon \cx \to \cx^\ast$ es exhaustiva pero no inyectiva ,
    de modo que definimos su inversa (el logarimo) como
    \[
        \log z = w = x +iy \iff z = e^w = e^xe^{iy} \iff
        \begin{cases}
            e^x = \abs{z}  \\
            y = \arg z
        \end{cases}
        \iff w = \log \abs{z} + i \arg z.
    \]
    Por lo tanto se trata de una función multivaluada (ya que depende de
    $\arg$ que también lo es).
\end{defi}
\begin{defi}[determinación!del logaritmo]
    Una determinación del logaritmo $\Log \colon U \subseteq \cx^\ast \to \cx$
    es una función tal que $\Log(z) \in \log(z)$, $\forall z \in U$.
\end{defi}
\begin{obs}
    Hacer una determinación del logaritmo es equivalente a hacer una
    determinación del argumento, ya que $\Log(z) = \log\abs{z} + i \Arg(z)$.
    Además, $\Log(z)$ es continua $\iff$ $\Arg(z)$ es continua.
\end{obs}
\begin{defi}[determinación!principal del logaritmo]
    La determinación principal del logaritmo es aquella que cumple que
    $\im\left(\Log(z) \right) \in (-\pi, \pi]$.
\end{defi}
\begin{obs}
    En la determinación principal, $\Log(z)$ es continua en $\cx \setminus
    (-\infty, 0]$.
\end{obs}
\begin{defi}[función!potencia]
    Sea $a \in \cx$ definimos la función potencia $z^a = e^{a \log(z)}$.
\end{defi}
\begin{obs}
    \begin{itemize}
        \item[]
        \item Si $a \in \z$, $z^a$ es univaluada.
        \item Si $a = \frac{r}{s} \in \q$ con $\mcd (r, s) = 1$ entonces $z^a$
            toma $s$ valores distintos.
        \item En cualquier otro caso, $z^a$ toma infinitos (numerable) valores
            distintos.
    \end{itemize}
\end{obs}
\begin{obs}
    Se puede definir una determinación del logaritmo de tal forma que $z^a$ sea
    siempre univaluada.
\end{obs}

\section{Funciones analíticas}
\begin{defi}[función!analítica]
    Sea $U \subseteq \cx$ un abierto y sea $f \colon U \to \cx$. Decimos que $f$
    es analítica en $U$ si $\forall z_0 \in U$ existe una serie de potencias
    $\sum a_n \left( z-z_0 \right)^n$ tal que $f(z) = \sum a_n \left( z - z_0
    \right)^n$ $\forall z \in \text{D}\left( z_0; R \right)$ con $R > 0$. Dicho
    de otra forma, diremos que una función es analítica si localmente es una
    serie de potencias.
\end{defi}
\begin{example}
    La función $f(z) = \frac{1}{1-z}$ es analítica en $U = \cx \setminus \left\{
    1 \right\}$.
\end{example}
\begin{prop}
    \begin{enumerate}[i)]
        \item[]
        \item La condición de ser analítica se conserva por sumas restas
            multiplicaciones y divisiones (siempre que no sea por cero).
        \item Por tanto los polinomios y funciones racionales son analíticas.
        \item La composición de funciones analíticas es analítica allí donde
            esté definida.
        \item La inversa de una función analítica es analítica allí donde esté
            definida.
        \item La serie de potencias $\sum a_n \left( z -z_0 \right)^n$ es
            analítica en el disco $\text{D}\left( z_0; R \right)$.
    \end{enumerate}
\end{prop}
\begin{teo}[Prolongación analítica]
    Sea $\Omega \subseteq \cx$ un abierto conexo y sea $f \colon \Omega \to \cx$
    analítica. Si $f$ se anula en un conjunto de puntos que tiene un punto de
    acumulación en $\Omega$, entonces $f=0$ $\forall z \in \Omega$.
\end{teo}
\begin{proof}
    Sea $z_0 \in \Omega$ un punto de acumulación de ceros de $f$, es decir, que
    $\exists \left( w_n \right)_{n \geq 0}$ tal que $w_n \to z_0$, $f\left(
    w_n \right) = 0$ $\forall n$ y $w_n \neq z_0$.
    
    Queremos ver que $f=0$ en algún disco abierto que contenga $z_0$. Sea $f$ una serie de poténcias $f(z) =
    \sum a_n \left(z - z_0 \right)^n$ si $z \in \text{D}\left( z_0; R \right)$
    $R > 0$, queremos ver que $a_n = 0$ $\forall n$ y que por lo tanto, $f(z) =
    0$ $\forall z \in \text{D}\left( z_0; R \right)$. Sino fuese así, tomamos $m
    = \min \left\{ n \in \n \mid a_n \neq 0 \right\}$ entonces
    \[
        f(z) = a_m \left( z -z_0 \right)^m + a_{m+1}\left( z-z_0
        \right)^{m+1} + \cdots = \left( z-z_0 \right)^m \underbrace{\left( a_m +
        a_{m+1}\left( z-z_0 \right) + \cdots\right)}_{g(z)}
    \]
    sabemos que $g(z)$ es continua porque viene dada por una serie de potencias
    y por lo tanto
    \[
        0 \neq a_m = g\left( z_0 \right) = \lim_{z \to z_0} g(z) =
        \lim_{n \to \infty} g\left( w_n \right) = \lim_{n \to \infty}
        \frac{f\left( w_n \right)}{\left( w_n - z_0 \right)^m} = 0.
    \]
    Lo cual supone una contradicción.

    Sea $U \subseteq \Omega$ el interior del conjunto de ceros de $f$. Sabemos
    que $U \neq \emptyset$ ya que acabamos de demostrar que $z_0 \in U$, $U$ es
    abierto, veremos ahora que $U$ también es cerrado.

    Sea $z_1 \in \overline{U}$. Veamos que $z_1 \in U$. $z_1 \in \overline{U}
    \implies$ $\forall n \geq 1$, $\text{D}\left( z_1; \frac{1}{n} \right)
    \cap U \neq \emptyset$. Sea $w_n \in \text{D}\left( z_1;
    \frac{1}{n} \right) \cap U$ tales que $w_n \neq z_1$. Entonces $\left(
    w_n \right) \to z_1$, $f\left( w_n \right) = 0$ ($w_n \in U$) y $w_n \neq z_1$, concluimos
    pues que $z_1$ es punto de acumulación de ceros de $f$. Por el mismo
    argumento que antes, $z_1 \in U$ y $U$ es cerrado.

    Como $\Omega$ es conexo y $\emptyset \neq U$ es abierto y cerrado, $\Omega =
    U$.
\end{proof}
\begin{obs}
    Una visión alternativa del teorema nos da proporciona el siguiente
    resultado:

    Sean $f, g \colon \Omega \to \cx$ analíticas tales que coinciden en un
    conjunto de puntos que tiene un punto de acumulación, entonces coinciden en
    todo $\Omega$.
\end{obs}
\begin{col}
    Sea $\Omega$ un abierto conexo, $U \subseteq \Omega$ abierto y $f \colon U
    \to \cx$ analítica. Existe como máximo una función $\tilde{f} \colon \Omega
    \to \cx$ analítica tal que $\tilde{f} \vert_U = f$.
\end{col}
\begin{defi}[prolongación analítica]
    Sea $\Omega$ un abierto conexo, $U \subseteq \Omega$ abierto y $f \colon U
    \to \cx$ analítica. Si existe una función $\tilde{f} \colon \Omega \to \cx$
    analítica tal que $\tilde{f} \vert_U = f$, la denominamos prolongación
    analítica de $f$.
\end{defi}
\begin{example}
    \begin{itemize}
        \item[]
        \item La función $f(z) = \sum \frac{z^n}{n!}$ en el disco
            $U = \text{D}\left( 0; 23 \right)$ es analítica y por lo tanto
            existe una única función $g(z)$ en $\Omega = \cx$ tal que $g(z)
            \vert_U = f(z)$.
        \item Consideramos la función $f(z) = \sum z^n$ definida en $U =
            \text{D}\left( 0; 1 \right)$ consideramos $\Omega = \cx \setminus
            \left\{ 1 \right\}$ y $g(z) = \frac{1}{1-z}$: $g(z)$ es la
            prolongación analítica de $f(z)$ en $\Omega$.
        \item $\Gamma(z) = \int^\infty_{0} e^{-t} t^{z-1} \dif t$ es convergente
            $\iff \re(z) > 0$. Sin embargo, se puede extender analíticamente a
            $\Omega = \cx \setminus \left\{ 0, -1, -2, \dots \right\}$.
        \item $\zeta(z) = \sum_{n \geq 1} \frac{1}{n^z}$ es convergente $\iff
            \re(z) > 1$. Sin embargo se puede extender analíticamente a $\Omega
            = \cx \setminus \left\{ 1 \right\}$.
    \end{itemize}
\end{example}
