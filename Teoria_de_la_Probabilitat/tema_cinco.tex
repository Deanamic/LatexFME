\chapter{Funcions característiques i famílies exponencials}

\section[Funció generadora de moments. Propietats i aplicació: fites de Chernoff]
{
    Funció generadora de moments. Propietats i aplicació: fites de Chernoff
    \sectionmark{Funció generadora de moments}
}
\sectionmark{Funció generadora de moments}

Aquesta eina ens permetrà estudiar de manera unificada les variables aleatòries discretes i
les absolutament contínues.

\begin{defi}[funció!generadora!de moments]
   Donada una variable aleatòria $X$, la seva funció generadora de moments \'es
   \[
       M_X(s) = \esp\lb e^{sX} \rb,
   \]
   on $s \in \cx$. Si $\esp\lb |X|^i \rb < \infty \: \forall i \geq 1$ aleshores $\esp\lb X^i \rb < \infty$
   i tenim que
   \[
       e^{sX} = \sum_{i \geq 0} \frac{X^i}{i!}s^i \implies \esp\lb e^{sX} \rb = 
       \esp\lb \sum_{i \geq 0}  \frac{X^i}{i!}s^i \rb,
   \]
   que, pel teorema de la convergència dominada, \'es igual a
   \[
       \sum_{i \geq 0} \esp\lb \frac{X^i}{i!}s^i \rb = \sum_{i \geq 0} 
       \frac{\esp \lb X^i \rb}{i!}s^i = M_X(s).
   \]
   Aquesta sèrie de potències podria tenir radi de convergència igual a 0. Per exemple,
   si $\esp \lb X^i \rb = i!^2$ aleshores el radi de convergència \'es 0.

\end{defi}
\begin{prop}
    \label{prop:propietatsMoments}
    La funció generadora de moments t\'e les següents propietats:
    \begin{enumerate}[i)]
        \item Si $Y = aX + b$, amb $X$ variable aleatòria amb funció generadora de moments
            $M_X(s)$ i $a \in \real, b \in \real$, aleshores $M_Y(s) = e^{bs} M_x(as)$.
        \item $\dv[k]{}{s} M_X(s) \big |_{s = 0} = \esp \lb X^k \rb$.
        \item Si $X$ i $Y$ són variables aleatòries independents, aleshores $M_{X+Y}(s) =
            M_X(s)M_Y(s)$.
     \end{enumerate}
\end{prop}
\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item $M_Y(s) = \esp \lb e^{Ys} \rb = \esp \lb e^{(aX + b)s}\rb = e^{bs} \esp \lb
            e ^{(as)X} \rb = e^{bs}M_X(as)$.
        \item La prova es deixa com a exercici pel lector.
        \item $M_{X+Y}(s) = \esp \lb e^{s(X+Y)} \rb = \esp \lb e^{sX} e^{sY} \rb =
            \esp \lb e^{sX} \rb \esp \lb e^{sY} \rb = M_X(s) M_Y(s)$.
    \end{enumerate}
\end{proof}
\begin{obs}
    De la fórmula anterior, si $X$ i $Y$ tenen tots els moments definits, aleshores $X +
    Y$ tamb\'e: $\esp\lb (X+Y)^k \rb = \sum \limits_{i = 0}^k \binom{k}{i} \esp \lb X^i \rb \esp \lb Y^{k-i}\rb$.
\end{obs}
\begin{example}
    $X \sim \operatorname{Be}(p)$. Aleshores
    \[
        M_X(s) = \esp \lb e^{sX} \rb = e^{s \cdot 1} p + e^{s \cdot 0}(1 - p) = 
        e^sp + (1- p)
    \]
\end{example}
\begin{example}
    $X \sim \operatorname{Exp}(\lambda)$. Aleshores
    \[
        M_X(s) = \int_{-\infty}^{+\infty} e^{sx}f_X(x) \dif x = \int_0^{\infty} e^{sx}
        \lambda e^{-\lambda x} \dif x = \int_0^{\infty} \lambda e^{(s-\lambda)x}\dif x
        = \lb \lambda \frac{e^{(s-\lambda)x}}{s - \lambda} \rb_0^{\infty}.
    \]
    Que, si $s-\lambda$ t\'e part real negativa, \'es igual a
    \[
        \lambda \frac{-1}{s-\lambda} = \frac{\lambda}{\lambda-s}.
    \]
    Observem que el radi de convergència \'es $\lambda$.
\end{example}
\begin{obs}
    En general, el càlcul de les funcions generadores de moments serà de la forma
    \begin{enumerate}[i)]
        \item En el cas discret, $M_X(s) = \sum\limits_{x \in \im (x)} e^{sx} P(X = x)$.
        \item En el cas absolutament continu, $M_X(s) = \int_{\real} e^{sx} f_X(x) \dif x$.
    \end{enumerate}
\end{obs}

\subsection{Aplicació. Teorema de Chernoff}
\begin{obs}
    Prenem $n$ variables aleatòries $\{X_i\}_{i=1}^n$ independents i amb la mateixa
    distribució. Sigui $S_n = \sum \limits_{i=1}^n X_i$, $\esp[X_i] = \mu, \var[X_i] = \sigma^2$.
    Si apliquem la desigualtat de Txebixov (\ref{teo:txebixov}) obtenim que
    \[
        P\lp |S_n - n\mu| > \varepsilon \rp \leq \frac{n\sigma^2}{\varepsilon^2} \implies
        0 \leq P\lp \left | \frac{S_n}{n} - \mu \right | > \varepsilon\rp \leq \frac{\sigma^2}{n\varepsilon^2}
    \]
    Que va a $0$ quan $n$ tendeix a $\infty$. Com veurem ara, aquesta fita es pot millorar
    en casos particulars.
\end{obs}
\begin{teo}[Teorema de Chernoff]
    \label{teo:Chernoff}
    Siguin $\{X_i\}_{i=1}^n$ variables aleatòries independents $X_i \sim \operatorname{Be}(p_i)$. Sigui
    $X = \sum\limits_{i=1}^n X_i$, $\mu = \esp \lb X \rb = \sum\limits_{i = 1}^n p_i$. Aleshores
    es tenen les següents fites:
    \begin{enumerate}[i)]
        \item Cua superior: $P\lp X-\mu \geq \delta \mu\rp = P\lp X \geq (1 + \delta)\mu \rp\leq
            e^{-\frac{\delta^2}{2+\delta}\mu}$, amb $\delta \geq 0$.
        \item Cua inferior: $P\lp X-\mu \leq -\delta \mu\rp = P\lp X \leq (1 - \delta)\mu \rp \leq
            e^{-\frac{\delta^2}{2}\mu}$, amb $\delta \in (0,1)$.
    \end{enumerate}
\end{teo}
\begin{proof}
    Demostrarem nom\'es la cua superior. Calculem la funció generadora de moments de $X$:
    \[
        M_X(s) = \prod_{i=1}^n M_{X_i}(s) = \prod_{i=1}^n \lp p_ie^s + (1-p_i) \rp =
        \prod_{i=1}^n \lp 1 + p_i \lp e^s -1 \rp \rp.
    \]
    Que, per $s \in \real$ i $p_i \in [0, 1]$, fent servir $1 + y \leq e^y$ \'es menor o igual a 
    \[
        \prod_{i=1}^n e^{p_i \lp e^s - 1 \rp} = e^{\sum\limits_{i=1}^n p_i \lp e^s-1 \rp} = 
        e^{\mu \lp e^s - 1 \rp}
    \]
    Si ara usem Markov, per $a \geq 0$
    \[
        P(X \geq a) \stackrel{s \in \real^+}{=} P(sx \geq sa) = P(e^{sx} \geq e^{sa})
        \stackrel{\text{Markov}}{\leq} \frac{\esp \lb e ^{sx} \rb}{e^{sa}} \leq \frac{e^{\mu
        \lp e^s - 1 \rp}}{e^{as}}
    \]
    Fent ara $a = (1 + \delta) \mu$:
    \begin{equation}
        \label{equation:Chernoff1}
        P\lp X \geq (1 + \delta) \mu\rp \leq e^{\lb \lp e^s -1 \rp \mu - \lp \lp 1 + \delta
        \rp \mu \rp s \rb} = f(s) = e^{g(s)}
    \end{equation}
    Volem trobar $s \in \real$, $s > 0$ tal que optimitzi el valor de $f(s)$:
    \[
        g'(s) = e^s \mu - (1 + \delta) \mu = 0 \implies s = \log(1+\delta).
    \]
    I, com $g''(s) = e^s \mu$,
    \[
        g''(\log(1+\delta)) = e^{\log(1 + \delta)} \mu = (1 + \delta) \mu > 0.
    \]
    És a dir, $\log(1+\delta)$ \'es el mínim que buscàvem. Si avaluem ara a la fita \ref{equation:Chernoff1}
    obtinguda anteriorment, tenim que
    \[
        P\lp X \geq (1 + \delta) \mu \rp \leq e^{\delta \mu - (1 + \delta) \mu \log(1+\delta)}
        = e^{\mu \lp \delta - (1 + \delta) \log(1+\delta)\rp}.
    \]
    A m\'es, utilitzant que $\log(1+x) \geq \frac{x}{1 + \frac{x}{2}}$ per $x \geq 0$
    \[
        e^{\mu \lp \delta - (1 + \delta) \log(1+\delta)\rp} \leq
        e^{\mu \lp \delta - (1 + \delta) \lp \frac{\delta}{1 + \frac{\delta}{2}}\rp\rp}
        = e^{\mu \frac{\delta + \frac{\delta^2}{2} - \delta - \delta^2}{1 + \frac{\delta}{2}}}
        = e^{-\mu \frac{\delta^2}{2 + \delta}}.
    \]
\end{proof}
\begin{obs}
    Per demostrar la fita per a la cua inferior, cal fer el següent:
    \[
        P(X \leq a) \stackrel{s > 0}{=} P(Xs \leq as) = P(e^{sX} \leq e^as) =
        P(e^{-sX} \geq e^{-as}) \leq \frac{\esp \lb e^{-sX}\rb}{e^{-as}} \leq 
        \frac{e^{\mu \lp e^{-s} - 1\rp} }{e^{-as}}.
    \]
    I optimitzar-ho per $a = (1 - \delta)\mu$.
\end{obs}
\begin{col}
    Amb la notació anterior i amb $\delta \in (0, 1)$:
    \[
        P\lp |X - \mu| \geq \delta \mu \rp \leq 2 e^{-\frac{\delta^2}{3} \mu}.
    \]
    Que fem notar que \'es una millor fita que Txebixov.
\end{col}
\begin{proof}
    Sigui $0 < \delta < 1$. Pel teorema de Chernoff (\ref{teo:Chernoff}) sabem que 
    \[
         P(X-\mu \geq \delta \mu) = P\lp X \geq (1 + \delta \mu) \rp \leq
        e^{-\frac{\delta^2}{2+\delta}\mu} \stackrel{\mu > 0,\, \delta \in (0, 1)}{\leq}
        e^{-\frac{\delta^2}{3}\mu}
    \]
    i que
    \[
        P(X-\mu \leq -\delta \mu) = P(X \leq (1 - \delta \mu) \leq
        e^{-\frac{\delta^2}{2}\mu} \stackrel{\mu > 0}{\leq}
        e^{-\frac{\delta^2}{3}\mu}.
    \]
    I, finalment,
    \[
        P\lp |X - \mu| \geq \delta \mu \rp = P \lp X - \mu \geq \delta \mu \rp
        + P \lp X - \mu \leq -\delta \mu \rp \leq 2e^{-\frac{\delta^2}{3}\mu}.
    \]
\end{proof}
\begin{obs}
    Hem vist que donada $X$ amb moments $\{a_i\}_{i\geq 1}$, podem construir
    la funció generadora de moments $M_X(s)$. Una pregunta a fer-se \'es si
    es pot reconstruir la variable $X$ donats els seus moments. Aquest problema es coneix
    com el problema de Hamburger i hi ha criteris que garanteixen que es pot fer. Veiem-ne
    un exemple.
\end{obs}
\begin{teo*}[Condició de Carleman]
    Amb la notació de l'observació anterior, si $a_{2n} \neq 0 \: \forall n$ i 
    $\sum \limits_{n \geq 1} a_{2n}^{-\frac{1}{2n}} = \infty$, aleshores els 
    moments determinen unívocament la variable aleatòria.
\end{teo*}

\section{Funcions característiques}
Hem vist que la funció generadora de moments introdueix un problema tècnic (analític)
perquè molts cops no la podrem avaluar. Això ho podrem solucionar prenent la variable
aleatòria $s = it$, $t \in \real$. En el cas absolutament continu,
\[
    \esp \lb e^{sX} \rb = \int_\real e^{sx}f_X(x) \dif x \stackrel{s = it}{=}
    \int_\real e^{itx} f_X(x) \dif x.
\]
I
\[
    \int_\real \left | e^{itx} f_X(x) \right | \dif x  = \int_\real |f_X(x)| \dif x
    = 1,
\]
d'on $E\lb e^{itX} \rb$ està ben definit.

\begin{defi}[funció!característica]
    La funció característica de $X$ \'es 
    \[
        \begin{aligned}
            \phi_X \colon \real &\to \cx \\
            t &\mapsto \esp \lb e^{itX} \rb 
        \end{aligned}
    \]
\end{defi}
\begin{obs}
    Les propietats de la funció generadora de moments es tradueixen immediatament a
    la funció característica. En particular, si $X, Y$ són independents,
    \[
        \phi_{X+Y}(t) = \phi_X(t) \phi_Y(t) 
    \]
\end{obs}
\begin{obs}
    En el cas absolutament continu, $\phi_X(t)$ \'es la transformada de Fourier de
    $f_X(t)$.
\end{obs}
\begin{example}
    \begin{enumerate}[1.]
        \item[]
        \item $X \sim \operatorname{Geom}(p) \implies \phi_X(t) = \frac{p}{e^{-it} - (1-p)}$
        \item $X \sim \operatorname{Poisson}(\lambda) \implies \phi_X(t) = e^{\lambda\lp e^{it - 1} \rp}$
        \item $X \sim \operatorname{U}[0,1] \implies \phi_X(t) = \frac{1}{it} \lp e^{it} -1 \rp$
    \end{enumerate}
\end{example}
\begin{example}
    $X \sim \operatorname{Cauchy}$, $f_X(x) = \frac{1}{\pi(1+x^2)}$. Llavors,
    \[
        M_X(s) = \int_{\real} \frac{e^{sx}}{\pi(1+x^2)} \dif x =
        \begin{cases}
            1 & \text{si }s = 0 \\
            \text{no està definida} & \text{si } s \in \real, \, s \neq 0
        \end{cases}.
    \]
    D'altra banda, $\phi_X(t) = \int_{-\infty}^{\infty} \frac{e^{itx}}{\pi (1 + x^2)}
    \dif x = e^{-|t|}$ %TODO repassar tema integrals a complexa i explicar aixo
\end{example}
\begin{example}
    $X \sim \operatorname{N}(0,1)$:
    \begin{gather*}
        \phi_X(t) = \frac{1}{\sqrt{2\pi}} \int_{\real} e^{itx} e^{-\frac{1}{2}x^2}
        \dif x \stackrel{u = it}{=} \frac{1}{\sqrt{2\pi}} \int_{\real} 
        e^{-\frac{1}{2}(u-x)^2 + \frac{1}{2}u^2} \dif x = \\
        = \frac{1}{\sqrt{2\pi}} e^{\frac{1}{2}u^2} \int_{\real} e^{-\frac{1}{2}(u-x)^2} 
        \dif x = e^{\frac{1}{2} u^2} = e^{-\frac{1}{2}t^2},
    \end{gather*}
    on hem usat que $\int_{\real} e^{-\frac{1}{2}(u-x)^2} = \sqrt{2 \pi}$. Veurem com
    es demostra per un cas m\'es senzill on la demostració \'es anàloga:
    \[
        \lp \int_{\real} e^{-x^2} \dif x \rp^2 = \lp \int_{\real} e^{-x^2} \dif x \rp 
        \lp \int_{\real} e^{-y^2} \dif y \rp = \int_{\real^2} e^{-x^2 - y^2} 
    \]
    Que, fent un canvi de variables a polars, \'es
    \[
        \int_0^{2\pi} \int_0^{\infty} e^{-R^2} 
        \lp - \frac{2R}{2} \rp \dif R \dif \theta = 2 \pi \lb -\frac{e^{-R^2}}{2} \rb_0^{\infty} = 
        \frac{2 \pi}{2} = \pi \implies \int_{\real} e^{-x^2} \dif x = \sqrt{\pi}.
    \]
    Per tant, si $Y \sim \operatorname{N}(\mu, \sigma^2)$, 
    \[
        \phi_Y(t) = \phi_{\sigma X + \mu}(t)
        \stackrel{\ref{prop:propietatsMoments}} = 
        e^{i \mu t} \phi_X(\sigma t) = e^{i \mu t - \frac{\sigma^2t^2}{2}},
    \]
    i, finalment, si $X \sim \operatorname{N}(\mu_1, \sigma_1^2)$, 
    $Y \sim \operatorname{N}(\mu_2, \sigma_2^2)$ independents,
    \[
        \phi_{X+Y}(t) = e^{i(\mu_1 + \mu_2)t - \frac{(\sigma_1^2 + \sigma_2^2)t^2}{2}}.
    \]
    La pregunta que un es pot fer ara \'es si a partir de $\phi_{X+Y}(t)$
    podem assegurar que $X + Y$ \'es normal. Amb el proper resultat veurem que, en general,
    la funció característica determina unívocament la variable aleatòria $X$.
\end{example}
\begin{teo}[Teorema d'inversió]
    Sigui $X$ una variable aleatòria amb probabilitat induïda $P_X$ i funció característica
    $\phi_X(t)$. Aleshores, si $a < b$
    \[
        P_X( (a, b) ) + \frac{1}{2}(P_X(\left\{ a \right\}) + P_X(\left\{ b \right\}) )
        = \lim_{T \to \infty} \frac{1}{2\pi} \int_{-T}^{T} \frac{e^{-ita} -
        e^{-itb}}{it} \phi_X(t) \dif t
    \]
\end{teo}
\begin{proof}
    Per a avaluar la integral del teorema, usarem la següent integral:
    \[
        \lim_{T \to \infty} \int_0^T \frac{\sin(tc)}{t} \dif t =
        \begin{cases}
            -\frac{\pi}{2} & c < 0 \\
            0 & c = 0 \\
            \frac{\pi}{2} & c > 0
        \end{cases}
    \]
    Fem el càlcul de la integral original:
    \begin{gather*}
        \frac{1}{2\pi} \int_{-T}^T \frac{e^{-ita} - e^{-itb}}{it} \phi_X(t) \dif t
        = \frac{1}{2\pi} \int_{-T}^T \frac{e{-ita} - e^{-itb}}{it} \lp \int_{\real}
        e^{itx} \dif p_x \rp \dif t = \\
        = \frac{1}{2\pi} \int_{-T}^T \int_{\real} 
        \frac{e^{it(x - a)} - e^{it(x-b)}}{it} \dif p_x \dif t = \frac{1}{2\pi} 
        \int_{\real}\int_{-T}^T \frac{e^{it(x - a)} - e^{it(x-b)}}{it} \dif t \dif P_X.
    \end{gather*}
    Observem ara que
    \[
        \int_{-T}^T \frac{e^{itc}}{it}\dif t = 2 \int_0^T \frac{\sin(tc)}{t} \dif t,
    \]
    perquè $\sin(tc) = \frac{e^{itc} - e^{-itc}}{2i}$. I, substituint a l'expressió
    anterior, tenim que \'es igual a
    \[
        = \frac{1}{\pi} \int_{\real}\int_{0}^T \lp \frac{\sin(t(x-a))}{t} -
        \frac{\sin(t(x-b))}{t} \rp \dif t \dif P_X.
    \]
    Definim ara $g(x, T, a, B) = \int_{0}^T \lp \frac{\sin(t(x-a))}{t} - \frac{\sin(t(x-b))}{t} \rp \dif t$. 
    Tenim que
    \[
        \lim_{T \to \infty} g(x, T, a, b) =
        \begin{cases}
            0 & x < a < b \\
            \frac{\pi}{2} &x = a < b \\
            \pi &  a < x < b \\
            \frac{\pi}{2} & a < x = b \\
            0 & a < b < x
        \end{cases}
    \]
    Finalment, calculem el límit de l'integral anterior
    \begin{gather*}
        \lim_{T \to \infty} \int_{\real} \frac{g(x, T, a, b)}{\pi} \dif P_X 
        \stackrel{\text{TCD}}{=} \int_{\real} \lim_{T \to \infty} 
        \frac{g(x, T, a, b)}{\pi} \dif P_X = \\ 
        = \frac{1}{2}(P_X(\left\{ a \right\}) + P_X(\left\{ b \right\})) + P_X( (a, b) ).
    \end{gather*}
\end{proof}
\begin{col}
    $\phi_X(t$) caracteritza $\lambda$-g.a. la variable aleatòria $X$ (la seva distribució),
    on $\lambda$ \'es la mesura de Lebesgue a $\real$.
\end{col}
\begin{proof}
    Si $\phi_X(t) = \phi_Y(t)$ per dues variables aleatòries $X, Y$ aleshores, pel teorema
    d'inversió, $\forall a < b$:
    \[
        \frac{1}{2}(P_X(\left\{ a \right\}) + P_X(\left\{ b \right\})) + P_X( (a, b) ) = 
        \frac{1}{2}(P_Y(\left\{ a \right\}) + P_Y(\left\{ b \right\})) + P_Y( (a, b) ).
    \]
    I, quan $a \to -\infty$, veiem que el terme $P_X (\left\{ a \right\})$ va a $0$:
    \[
        \lim_{a \to -\infty} P_X(\left\{ a \right\}) =  \lim_{a \to -\infty} P(X = a) =
        \lim_{a \to -\infty} F_X(a) - \lim_{h \to 0^+} F_X(a - h) = 0.
    \]
    D'on, estudiant el límit de l'expressió anterior a $a \to \infty$, tenim
    \[
        \frac{1}{2}P_X(\left\{ b \right\}) + P_X( (-\infty, b) ) = 
        \frac{1}{2}P_Y(\left\{ b \right\}) + P_Y( (-\infty, b) ).
    \]
    \'Es a dir, 
    \[
        F_X(b) + \frac{1}{2} P_X(\left\{ b \right\}) =
        F_Y(b) + \frac{1}{2} P_Y(\left\{ b \right\}).
    \]
    Com $P_X(\left\{ b \right\})$ i $P_Y(\left\{ b \right\})$ són diferents de zero en
    un conjunt numerable (són punts de discontinuïtat de les funcions creixents 
    $F_X$ i $F_Y$), tenim que $F_X = F_Y$ $\lambda$-g.a..
\end{proof}
\subsection{Propietats addicionals de la funció característica}
\begin{prop}
    Sigui $X$ una variable aleatòria amb funció característica $\phi_X(t) = \esp \lb 
    e^{itx} \rb$. Aleshores,
    \begin{enumerate}[i)]
        \item $\phi_X(0) = 1$ i $|\phi_X(t)| \leq 1$ $\forall t \in \real$.
        \item $\phi_X(t)$ \'es uniformement contínua.
        \item $\forall t_1, \dots, t_n \in \real$, $\forall z_1, \dots, z_n \in \cx$,
            $\sum \limits_{j,\,k} \phi_X(t_j - t_k) z_j \bar{z}_k \geq 0$ ($\forall n \in
            \n$).
    \end{enumerate}
\end{prop}
\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item \'Es immediat.
        \item Volem fitar $|\phi_X(x) - \phi_X(y)|$ quan $|x - y| < \varepsilon$. Fem
            $x = t + h$, $y = t$ i volem fitar $|\phi_X(t+h) - \phi_X(t)|$ en termes de
            $h$:
            \begin{gather*}
                |\phi_X(t+h) - \phi_X(t)| = \left|\esp \lb e^{i(t+h)X} - e^{itX} \rb\right|
                = \left|\esp \lb e^{itX} \lp e^{ihX} - 1 \rp \rb\right| \leq \\
                \leq \esp \lb \left |e^{itX} \right | \left | e^{ihX} - 1 \right | \rb
                \stackrel{Y(h) = \left | e^{ihX} - 1 \right |}{=} \esp \lb Y(h) \rb
            \end{gather*}
            Observem que $0 \leq Y(h) \leq 2$. A m\'es, $\lim_{h\to 0} Y(h) = 0$. Aplicant 
            el teorema de convergència dominada,
            \[
                \lim_{h \to 0} \esp \lb Y(h) \rb = \esp \lb \lim_{h \to 0} Y(h) \rb = 0.
            \]
            D'on $\lim_{h \to 0} |\phi_X(t+h) - \phi_X(t)| = 0$ amb independència de $t$.
        \item Reescrivim l'expressió donada:
            \begin{gather*}
                \sum_{j,\,k} \phi_X(t_j - t_k) z_j \bar{z}_k = 
                \sum_{j,\,k} \esp \lb e^{i(t_j - t_k)X}\rb z_j \bar{z}_k  =
                \esp \lb \sum_{j,\,k} e^{it_jX} z_j e^{-it_kX}\bar{z}_k \rb  = \\
                = \esp \lb \lp \sum_{j} e^{it_jX} z_j \rp 
                \lp \sum_{k} e^{-it_kX}\bar{z}_k \rp \rb  =
                \esp \lb \lp \sum_{j} e^{it_jX} z_j \rp 
                 \overline{\lp\sum_{k} e^{it_kX}z_k\rp} \rb =\\
                = \esp \lb \left |\sum_{j} e^{it_jX} z_j \right|^2 \rb \geq 0
            \end{gather*}
    \end{enumerate}
\end{proof}
\begin{obs}
    Les propietats anteriors són condicions suficients per assegurar que una funció donada
    \'es funció característica d'una variable aleatòria (teorema de Bochner).
\end{obs}
\begin{obs}
    Si $(X, Y)$ \'es un vector de variables aleatòries, podem definir la seva funció
    característica com
    \[
        \phi_{(X, Y)}(t_1, t_2) = \esp \lb e^{it_1X} e^{it_2Y} \rb
    \]
\end{obs}

\section{Famílies exponencials: exemples}
Veurem ara que podem tractar de manera unificada moltes variables aleatòries que han
aparegut fins ara.
\begin{defi}[família!exponencial]
    Una família de variables aleatòries \'es exponencial amb paràmetres $\vec{\theta} =
    (\theta_1, \dots, \theta_n)$ si la seva funció de densitat (o funció de probabilitat)
    \'es de la forma:
    \[
        P(X, \vec{\theta}) = P(X | \vec{\theta}) = f(x, \vec{\theta}) = g(x) \exp \lp\sum_{i=1}^n
        \theta_i t_i(x) - c(\vec{\theta}) \rp
    \]
    amb $g(x) \geq 0$ i $\left\{ t_i(x) \right\}$ linealment independents.
\end{defi}
\begin{example}
    Bernoulli per $x \in \left\{ 0, 1 \right\}$:
    \begin{gather*}
        P(x, p) = P(x | p) = p^{x} (1-p)^{1-x} = 
        \exp \lp x \log p + (1-x) \log(1-p) \rp = \\
        = \exp \lp x \log \lp \frac{p}{1-p} \rp + \log(1 - p) \rp .
    \end{gather*}
    És a dir, $\theta_1 = \log \lp \frac{p}{1-p} \rp$, $t_1(x) = x$, $g(x) = 1$,
    i $c(\theta) = -\log(1-p) = \log\lp\frac{1}{1-p}\rp = \log\lp 1+e^{\theta_1} \rp$.
\end{example}
\begin{example}
    Normal:
    \begin{gather*}
        f(x | \mu, \sigma) = \frac{1}{\sqrt{2\pi \sigma^2}} 
        \exp \lp-\frac{(x-\mu)^2}{2 \sigma^2} \rp =
        \frac{1}{\sqrt{2\pi}} \exp \lp-\log \sigma -\frac{x^2}{2 \sigma^2} + 
        \frac{x\mu}{\sigma^2} - \frac{\mu^2}{2\sigma^2} \rp = \\
        = \frac{1}{\sqrt{2\pi}} \exp \lp x\frac{\mu}{\sigma^2} + 
        x^2\lp -\frac{1}{2\sigma^2} \rp -\lp \log \sigma  
        + \frac{\mu^2}{2\sigma^2} \rp \rp.
    \end{gather*}
    És a dir, $\theta_1 = \frac{\mu}{\sigma^2}$, $\theta_2 = -\frac{1}{2\sigma^2}$,
    $t_1(x) = x$, $t_2(x) = x^2$ i
    \begin{gather*} 
        c(\theta_1, \theta_2) = \log \sigma  + \frac{\mu^2}{2\sigma^2} = 
        \frac{1}{2} \log (\sigma^2) + \frac{\mu^2}{2\sigma^2} = \\
        = \frac{1}{2} \log \lp -\frac{1}{2\theta_2} \rp + 
        \frac{\theta_1^2}{4 \theta_2^2}(-\theta_2)
        = \frac{1}{2} \log \lp-\frac{1}{2\theta_2} \rp - \frac{\theta_1^2}{4\theta_2}
    \end{gather*}
\end{example}
\begin{prop}
    $\esp \lb t_i(X) \rb = \pdv{}{\theta_i} c(\theta_1, \dots, \theta_n)$ 
    en una família exponencial.
\end{prop}
\begin{proof}
    Farem el cas absolutament continu. Observem que $\int_{\real} f(x, \vec{\theta})
    \dif x = 1$, de manera que si integrem a tots dos costats de la definició de
    $f(x, \vec{\theta})$ per famílies exponencials obtenim:
    \begin{equation}
        \label{equation:eqexponencial}
        \exp c(\vec{\theta}) = \int_{\real} g(x) \exp \lp \sum_{i=1}^n 
        \theta_i t_i(x) \rp \dif x.
    \end{equation}
    I, derivant a tots dos costats respecte de $\theta_i$, tenim que:
    \begin{gather*}
        \pdv{}{\theta_i} c(\vec{\theta}) = \frac{\pdv{}{\theta_i} 
        \int_{\real} g(x) \exp \lp \sum_{i=1}^n \theta_i t_i(x) \rp \dif x}
        {\exp \lp c (\vec{\theta}) \rp} \stackrel{\text{TCD}}{=}  \\
        \stackrel{\text{TCD}}{=}  \frac{\int_{\real} g(x) t_i(x) \exp \lp 
        \sum_{i=1}^n \theta_i t_i(x) \rp \dif x}
        {\exp \lp c (\vec{\theta}) \rp} = \esp \lb t_i(x) \rb.
    \end{gather*}
\end{proof}
\begin{defi}[família!exponencial!natural]
    Una família exponencial es diu natural si $\exists k \colon t_k(x) = x$.
\end{defi}
\begin{prop}
    Sigui $X$ una variable aleatòria d'una família exponencial natural. Aleshores 
    $\phi_X(t) =\exp \lp c(\theta_1 + it, \dots, \theta_n) - c(\vec{\theta})\rp$
\end{prop}
\begin{proof}
    Calculem la integral:
    \begin{gather*}
        \phi_X(t) = \esp \lb e^{itX} \rb = \int_{\real} e^{itx} g(x) \exp \lp 
        \theta_1 x + \sum_{r = 2}^n \theta_r t_r(x) - c(\vec{\theta}) \rp \dif x = \\
        \int_{\real} g(x) \exp \lp (\theta_1 +it) x + \sum_{r = 2}^n \theta_r t_r(x)
        - c(\vec{\theta}) \rp \dif x.
    \end{gather*}
    Que, utilitzant l'equació \ref{equation:eqexponencial} que hem fet servir 
    a la demostració de la proposició anterior, tenim que \'es igual a
    \[
        \exp \lp c(\theta_1 + it, \dots, \theta_n) - c(\vec{\theta})\rp
    \]
\end{proof}
\begin{example}
    En el cas de la normal, $\theta_1 = \frac{\mu}{\sigma^2}$,  $\theta_2 =
    -\frac{1}{2\sigma^2}$. Aleshores
    \[
        \phi_X(t) = \exp \lp c(\theta_1 + it, \theta_2) - c(\theta_1, \theta_2) \rp
    \]
    i
    \[
        c(\theta_1, \theta_2) = \frac{\mu^2}{2\sigma^2} + \log(\sigma) = 
        - \frac{\theta_1^2}{4\theta_2} + h(\theta_2).
    \]
    De manera que
    \[
        c(\theta_1 + it, \theta_2) - c(\theta_1, \theta_2) = 
        -\frac{(\theta_1+it)^2}{4\theta_2}  + \frac{\theta_1^2}{4\theta_2} =
        -\frac{2 \theta_1 it}{4 \theta_2} + \frac{t^2}{4\theta_2} =
        \mu it - \sigma^2 \frac{t^2}{2}.
    \]
    Per tant, $\phi_X(t) = \exp \lp it\mu -\sigma^2 \frac{t^2}{2} \rp$. 

\end{example}
