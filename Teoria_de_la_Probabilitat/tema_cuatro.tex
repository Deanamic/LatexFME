\chapter{Variables aleatòries contínues}

\section[Mesures de probabilitat absolutament contínues. Funció de densitat]
    {Mesures de probabilitat absolutament contínues. Funció de densitat
    \sectionmark{Mesures absolutament contínues}}
    \sectionmark{Mesures absolutament contínues}

Sigui $(\Omega,\Asuc,p)$ un espai de probabilitat i sigui $X$ una v.a. Aleshores $X$ indueix una probabilitat $P_X$ sobre $(\real,\B)$.
Ara estudiarem v.a. $X$ on $P_X$ és ``compatible'' amb la mesura de Lebesgue $\lambda$.
    
\begin{defi}[mesura absolutament contínua]
    Sigui $\lp\Omega,\Asuc\rp$ un espai de mesurable i $\mu_1, \mu_2$ dues mesures sobre $\lp\Omega,\Asuc\rp$. Diem que $\mu_1$ és
    absolutament contínua respecte a $\mu_2$ ($\mu_1 \ll \mu_2$) si
    \[\forall A \in \Asuc, \quad \mu_2 (A) = 0 \implies \mu_1 (A) = 0\]
\end{defi}

\begin{defi}[variable aleatòria!absolutament contínua]\index{variable aleatòria!contínua((tlab))}
    Una v.a. $X$ és absolutament contínua (també anomenada contínua) si $P_X \ll \lambda$.
\end{defi}

\begin{obs}
    Les variables aleatòries discretes no són contínues.
    Sigui $X$ v.a. discreta amb $\im(X) = \lc a_i \rc_{i \ge 1}$, i $P(X=a_i)=p_i>0$.
    Aleshores $P_X(\lc a_i \rc)=p_i$, però $\lambda(\lc a_i \rc) = 0$.
\end{obs}

\begin{teo}[Teorema de Radon-Nikodym]
    Sigui $(\Omega,\Asuc)$ un espai mesurable i $\mu_1 \ll \mu_2$ dues mesures.
    Aleshores existeix una funció $f_{\mu_1}\colon \Omega\to\real$ mesurable en $(\Omega,\Asuc)$ tal que
    \[\forall A \in\Asuc,\quad \mu_1(A) = \int_A \dif\mu_1 = \int_A f_{\mu_1}\dif\mu_2\]
    És a dir, $\dif\mu_1 = f_{\mu_1}\dif\mu_2$, o ``$\frac{\dif\mu_1}{\dif\mu_2} = f_{\mu_1}$'' (derivada de Radon-Nikodym).
\end{teo}

\begin{proof}
    És una prova complicada i amb eines avançades que no farem.
\end{proof}

\begin{col}
    En la nostra situació, $\mu_1 = P_X$ i $\mu_2 = \lambda$, per tant si $P_X \ll \lambda$ tenim
    \[\forall A \in\B,\quad P_X(A) = \int_A f_{X}\dif\lambda = \int_\real f_{X} \fid_A \dif\lambda\]
\end{col}

\begin{defi}[funció!de densitat de probabilitat]
    A $f_X = f_{\mu_1}$ l'anomenarem funció de densitat de probabilitat de $X$.
\end{defi}

\begin{obs}
    El teorema de Radon-Nikodym no afirma la unicitat de $f_{\mu_1}$, de fet si $f_{\mu_1}$ i $\overline{f_{\mu_1}}$ satisfan les
    condicions, aleshores el teorema també afirma que $f_{\mu_1} = \overline{f_{\mu_1}}$ $\mu_2$-g.a.
\end{obs}

\begin{prop}[Propietats de la funció de densitat]
    Sigui $X$ v.a. amb funció de densitat $f_X$,
    \begin{enumerate}[i)]
        \item $f_X \ge 0 \quad \lambda$-g.a.
        \item $\int_\real f_X \dif\lambda = 1$.
        \item Si $f_X$ és integrable Riemann, $\forall A = (a,b)$ interval, $P_X(A) = \int_A f_X\dif\lambda = \int_a^b f_X(x)\dif x$.
        \item $P(X=x) = P_X(\{x\}) = \int_{\{x\}} f_X \dif\lambda = 0, \quad\forall x\in\real$.
    \end{enumerate}
\end{prop}

\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item Sigui $A = \lc x\in\real \colon f_X(x)<0 \rc$, volem veure que té mesura (de Lebesgue) 0.
            \[A_n = \bigcup_{n \ge 1} \lc x\in\real \colon f_X(x)<\frac{-1}{n} \rc\]
            i $\lc A_n \rc_{n \ge 1}$ és creixent.
            \[0 \le P_X(A_n) = \int_{A_n} f_X \dif\lambda \leq \frac{-1}{n}\lambda(A_n) \le 0 \implies \lambda(A_n)=0, \,\forall n\]
    \end{enumerate}
    Per tant, $\lambda(A) = \lim_{n}\lambda \lp A_n\rp =0$. Les altres tres en són conseqüència.
\end{proof}

\begin{obs}
    Tota funció $f$ que compleixi les tres primeres propietats defineix una v.a. $X$:
    \[F(x) = \int_{-\infty}^{x} f(x)\dif x\]
    De fet quan es defineix informalment v.a. contínua se sol definir com una funció amb aquestes propietats.
\end{obs}

Ara volem calcular l'esperança d'una v.a. $X$ i, en general, calcular $\esp[g(x)]$ per $g\colon\real\to\real$ funció mesurable.

\begin{prop}
    \begin{enumerate}[i)]
        \item[]
        \item Per l'esperança d'una v.a. $X$ contínua tenim\[\esp[X] = \int_\real x \dif p_X = \int_\real xf_X\dif\lambda = \int_{-\infty}^{+\infty} xf(x)\dif x.\]
        \item I per la seva variància \[\var[X] = \int x^2f_X(x) \dif\lambda - \lp\int xf_X \dif\lambda \rp^2.\]
    \end{enumerate}
\end{prop}

\begin{obs}
    Si volem que $\esp[g(x)] < +\infty$, és prou amb demanar 
    \[
        \int_\real \abs{g(x)}\abs{f_X(x)} \dif\lambda < +\infty.
    \]
\end{obs}

\section{Models de variables aleatòries contínues}

\subsection*{Distribució uniforme}

Dóna un valor a $[a,b)$ uniformement a l'atzar.

\begin{defi}[distribució!uniforme!contínua]
    $\operatorname{U}(a,b)$ és la distribució amb funció de densitat
    \[f_X(x) = \frac{1}{b-a} \fid_{[a,b]}(x),\]
    on $\fid_{[a,b]}$ és la funció identitat de $[a,b]$ (val 1 si $x\in[a,b]$ i 0 si no).
\end{defi}

\begin{prop}
    \begin{enumerate}[i)]
        \item[]
        \item $\esp[X] = \frac{a+b}{2}.$
        \item $\var[X] = \frac{(b-a)^2}{12}.$
    \end{enumerate}
\end{prop}

\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item $\esp[X] = \int_{-\infty}^{+\infty} xf(x)\dif x = \int_{-\infty}^{+\infty} x\frac{1}{b-a}\dif x = \frac{1}{b-a} \lb \frac{x^2}{2} \rb_{x=a}^b = \frac{1}{2}\frac{b^2-a^2}{b-a} = \frac{a+b}{2}.$
        \item $\esp[X^2] = \frac{b^2 + ab + a^2}{3} \implies \var[X] = \frac{(b-a)^2}{12}.$
    \end{enumerate}
\end{proof}

%TODO No entenc això de la tangent

\subsection*{Distribució exponencial}

Es pot entendre com l'extensió de la distribució geomètrica.

\begin{defi}[distribució!exponencial]
    $\operatorname{Exp}(\lambda)$ és la distribució amb funció de densitat
    \[f_X(x) = \lambda e^{-\lambda x} \fid_{[0,+\infty]}(x).\]
\end{defi}

\begin{prop}
    \begin{enumerate}[]
        \item[]
        \item $\esp[X] = \frac{1}{\lambda}.$
        \item $\var[X] = \frac{1}{\lambda^2}.$
    \end{enumerate}
\end{prop}

\subsection*{Distribució normal}

Es pot entendre com l'extensió de la distribució binomial.

\begin{defi}[distribució!normal]
    $\operatorname{N}(\mu, \sigma^2)$ és la distribució amb funció de densitat
    \[f_X(x) = \frac{1}{\sqrt{2\pi\sigma^2}} e^{-\frac{(x-\mu)^2}{2\sigma^2}}.\]
\end{defi}

\subsection*{Altres distribucions contínues}

\begin{defi}\index{distribució!gamma((tlab))}\index{distribució!de Weibull((tlab))}\index{distribució!beta((tlab))}\index{distribució!de Cauchy((tlab))}
Hi ha altres distribucions de v.a. contínues:
\begin{center}
    \everymath{\displaystyle}
    \begin{tabular}{lcccc}
        
        Nom     & Símbol                              & Funció de densitat $F_X(x)$                                             \\ \hline 
        & & \\ \vspace{0.3cm}
        Gamma   & $\operatorname{\Gamma}(\lambda,z)$  & $\frac{\lambda^z}{\Gamma(z)} x^{z-1} e^{-\lambda x}\fid_{[0,+\infty)}(x)$ \\ \vspace{0.3cm}
        Weibull & $\operatorname{Weib}(\alpha,\beta)$ & $\alpha\beta\exp(-\alpha x^\beta)\fid_{[0,\alpha)}(x)$ \\ \vspace{0.3cm}
        Beta    & $\operatorname{\upbeta}(a,b)$       & $\frac{\Gamma(a+b)}{\Gamma(a)\Gamma(b)}x^{a-1}(1-x)^{b-1}\fid_{(0,1)}(x)$ \\
        Cauchy  & $\operatorname{Cauchy}(0,1)$        & $\frac{1}{\pi(1+x^2}\fid_{[0,+\infty)}(x)$                                \\
    \end{tabular}
\end{center}
\begin{center}
    \everymath{\displaystyle}
    \begin{tabular}{lcccc}
        Nom     & $\esp[X]$             & $\var[X]$ \\ \hline
        & & \\ \vspace{0.3cm}
        Gamma   & $\frac{z}{\lambda}$   & $\frac{z}{\lambda^2}$ \\ \vspace{0.3cm}
        Weibull & $\alpha^\frac{-1}{\beta}\Gamma(1+\frac{1}{\beta})$ & $\alpha^{\frac{-2}{\beta}}(\Gamma(1+\frac{2}{\beta})-\Gamma(1-\frac{2}{\beta}))^2$ \\ \vspace{0.3cm}
        Beta    & $\frac{a}{a+b}$         & $\frac{ab}{(a+b)^2(a+b+1)}$ \\
        Cauchy  & $+\infty$             & No definida\\
    \end{tabular}
\end{center}

On $\Gamma(z) = \int_0^{+\infty} x^{z-1}e^{-x} \dif x$ és la funció gamma.

\end{defi}

\section[Conceptes de variables aleatòries contínues]
    {Distribució conjunta i marginals. Independència. Distribucions condicionades
    \sectionmark{Conceptes de distribucions contínues}}
    \sectionmark{Conceptes de distribucions contínues}
 
Farem tota l'anàlisi per dues variables aleatòries, es pot generalitzar immediatament a $k$ variables aleatòries.
Prenem $(X,Y)$ v.a. multidimensional de dimensió 2.

\begin{defi}[vector de variables aleatòries!absolutament continu]\index{vector de variables aleatòries!continu((tlab))}
    Direm que $(X,Y)$ és un vector de variables aleatòries absolutament continu si $P_{(X,Y)} \ll \lambda_{\real^2}$.
\end{defi}

\begin{obs}
    En aquesta situació l'extensió del teorema de Radon-Nikodyn a $\real^2$ garanteix que $\exists f_{(X,Y)}\colon\real^2\to\real$
    mesurable en $(\real^2, \B_{\real^2})$ i per la qual
    \[B \in \B_{\real^2},\quad P_{(X,Y)}(B) = \int_B f_{(X,Y)} \dif \lambda_{\real^2}\]
    que, si $f_{(X,Y)}$ és integrable Riemann i $B = (a,b) \times (c,d)$, és $\int_a^b \int_c^d f_{(X,Y)}(x,y) \dif x \dif y$
\end{obs}

\subsection{Distribució conjunta i marginals}

\begin{defi}[funció!de densitat de probabilitat!conjunta]\index{funció!de densitat de probabilitat!d'un vector((tlab))}
    $f_{(X,Y)}$ és la funció de densitat de probabilitat del vector $(X,Y)$, o funció de densitat conjunta de $X$ i $Y$.
\end{defi}

\begin{defi}
    D'aquesta forma podem definir $F_{(X,Y)}(x,y)$ a partir de $f_{(X,Y)}(x,y)$:
    \[F_{(X,Y)}(x,y) = P(X\le x, Y\le y) = \int_B f_{(X,Y)} \dif\lambda_{\real^2}\]
    on $B = (-\infty, x) \times (-\infty, y)$.
    
    Si $f_{(X,Y)}$ és integrable Riemann, $F_{(X,Y)}(x,y) = \int_{-\infty}^x \int_{-\infty}^y f_{(X,Y)}(u,v) \dif u \dif v$.
\end{defi}

\begin{prop}
    Si $(X,Y)$ és un vector de v.a. absolutament continu (a $\real^2$), aleshores tant $X$ com $Y$ són v.a. absolutament contínues
    (respecte $\lambda_\real$).
\end{prop}
\begin{proof}
    Es pot fer per contradicció.
\end{proof}

\begin{obs}
    Per tant, $X$ i $Y$ tenen funcions de densitat $f_X(x)$ i $f_Y(y)$, respectivament.
\end{obs}

\begin{defi}[funció!de densitat de probabilitat!marginal]
    Donat el vector absolutament continu $(X,Y)$ amb funció de densitat conjunta $f_{(X,Y)}$, les funcions de probabilitat marginals
    són $f_X$ i $f_Y$.
\end{defi}

\begin{prop}
    En particular, si són integrables Riemann,
    \[f_X(u) = \int_{-\infty}^{+\infty} f_{(X,Y)}(u,v) \dif v.\]
    \[f_Y(v) = \int_{-\infty}^{+\infty} f_{(X,Y)}(u,v) \dif u.\]
\end{prop}
\begin{proof}
    \[F_X(x) = p(X\le x) = p(X\le x, Y\in(-\infty,+\infty)) = P((-\infty,x)\times\real).\]
    Que, per ser integrable Riemann, val $\int_{-\infty}^{x}\int_{-\infty}^{+\infty} f_{(X,Y)}(u,v)\dif v \dif u$. Per tant,
    \begin{gather*}
        \int_{-\infty}^x f_X(u) \dif u = \int_{-\infty}^{x}\int_{-\infty}^{+\infty} f_{(X,Y)}(u,v)\dif v \dif u \implies \\
        0 = \int_{-\infty}^x \lb f_X(u) - \int_{-\infty}^{+\infty} f_{(X,Y)}(u,v)\dif v \rb \dif u.
    \end{gather*}
    Derivant obtenim que
    \[0 = f_X(u) - \int_{-\infty}^{+\infty} f_{(X,Y)}(u,v)\dif v.\]
\end{proof}

\begin{obs}
    $f_{(X,Y)}$ conté més informació que $f_X$ i $f_Y$.
\end{obs}

\begin{obs}
    \[F_{(X,Y)}(x,y) = \int_{-\infty}^x \int_{-\infty}^y f_{(X,Y)}(u,v) \dif u \dif v \implies
    \frac{\partial^2 F_{(X,Y)}}{\partial x \partial y}(x,y) = f_{(X,Y)}(x,y).\]
\end{obs}

\begin{obs}
    Si $g\colon\real^2\to\real$ mesurable:
    \[\esp[g(X,Y)] = \int_{\real^2} g(x,y) f_{(X,Y)} \dif\lambda_{\real^2} =
    \int_{-\infty}^{+\infty}\int_{-\infty}^{+\infty} g(x,y) f_{(X,Y)}(x,y) \dif y \dif x.\]
\end{obs}

\subsection{Independència}

És l'únic cas en què podem obtenir informació de $(X,Y)$ sabent només $X$ i $Y$.

\begin{prop}
    Si $X,Y$ són independents,
    \[f_{(X,Y)}(u,v) = f_X(u)f_Y(v)\]
\end{prop}
\begin{proof}
    $F_{(X,Y)}(x,y) = F_X(x)F_Y(y)$, per tant, $\forall x,y$,
    \[ 
        \int_{-\infty}^x \int_{-\infty}^y f_{(X,Y)}(u,v) \dif v \dif u =
        \int_{-\infty}^x f_X(u) \dif u \int_{-\infty}^y f_Y(v) \dif v = \int_{-\infty}^x \int_{-\infty}^y f_X(u)f_Y(v) \dif v \dif u
    \]
    Com que és cert $\forall x,y$, tenim que $f_{(X,Y)}(u,v) = f_X(u)f_Y(v)$.
\end{proof}

\begin{obs}
    Si $X,Y$ són independents, $\forall B_1,B_2\in\B$:
    \[p(x \in B_1, y \in B_2) = p(x \in B_1)p(y \in B_2) = \int_{B_1} f_X(u) \dif\lambda \int_{B_2} f_Y(v) \dif\lambda\]
    \[p((x,y) \in B_1 \times B_2) = \int_{B_1 \times B_2}f_{(X,Y)}(u,v) \dif\lambda_{\real^2}\]
    Per Fubini, $\int_{B_1} f_X(u) \dif\lambda \int_{B_2} f_Y(v) \dif\lambda = \int_{B_1 \times B_2}f_{(X,Y)}(u,v) \dif\lambda_{\real^2}$
\end{obs}

\begin{obs}
    Notació:
    \[\esp[(X,Y)] = (\esp[X],\esp[Y])\]
    \[\var[(X,Y)] = \begin{pmatrix} \var[X] & \cov(X,Y) \\ \cov(X,Y) & \var[Y] \end{pmatrix}\]
    Si $X,Y$ són independents, $\var[(X,Y)]$ és una matriu diagonal $\operatorname{diag}(\var[X],\var[Y])$.
\end{obs}

\subsection{Distribucions condicionades}

\begin{defi}[variable aleatòria!contínua!condicionada]\index{variable aleatòria!absolutament contínua!condicionada((tlab))}
    Siguin $X,Y$ v.a. absolutament contínues i sigui $x$ pel qual $f_X(x) > 0$. La v.a. $Y$ condicionada a $X=x$ ($Y \vert X=x$) és
    la v.a. amb funció de distribució
    \[F_{Y \vert X}(y,x) = \frac{1}{f_X(x)}\int_{-\infty}^yf_{(Y,X)}(v,x) \dif v.\]
    En aquesta situació, la funció de densitat de $Y \vert X=x$ és $\frac{f_{(Y,X)}(y,x)}{f_X(x)}.$
\end{defi}

\begin{defi}[esperança!condicionada!contínua]
    Per a $x \tq f_X(x) > 0$, l'esperança condicionada $\esp[Y \vert X=x]$ és
    \[\int_{-\infty}^{+\infty} yf_{Y \vert X}(y,x) \dif y = \psi(x).\]
    I $\esp[Y \vert X]$ és $\psi(X).$
\end{defi}

\begin{teo*}
    \[\esp[\esp[Y \vert X]] = \esp[Y]\]
\end{teo*}
\begin{proof}
    \begin{align*}
        \esp[\esp[Y \vert X]] &= \int_{-\infty}^{+\infty} \esp[Y \vert X=x]f_X(x) \dif x = \\
        &= \int_{-\infty}^{+\infty}\int_{-\infty}^{+\infty} yf_{Y \vert X}(y,x) \dif y f_X(x) \dif x = \\
        &= \int_{-\infty}^{+\infty}\int_{-\infty}^{+\infty} y\frac{f_{Y,X}(y,x)}{f_X(x)} \dif y f_X(x) \dif x = \\
        &= \int_{-\infty}^{+\infty}\int_{-\infty}^{+\infty} yf_{Y,X}(y,x) \dif x \dif y = \\
        &= \int_{-\infty}^{+\infty} y \int_{-\infty}^{+\infty} f_{Y,X}(y,x) \dif x \dif y = \\
        &= \int_{-\infty}^{+\infty} yf_Y(y) \dif y \\
        &= \esp[Y]
    \end{align*}
\end{proof}

\section[Distribució normal, multivariant i distribucions associades]
    {Distribució normal, multivariant i distribucions associades
    \sectionmark{Distribució normal, multivariant i associades}}
    \sectionmark{Distribució normal, multivariant i associades}

\begin{defi}[distribució!normal]
    La distribució normal ve donada per dos paràmetres $\mu$ i $\sigma^2$ i té funció de 
distribució
    \[\frac{1}{\sqrt{2\pi\sigma^2}} e^\frac{(x-\mu)^2}{2\sigma^2}\]
    La denotarem per $\normald(\mu,\sigma^2)$
\end{defi}

\begin{example}
    \begin{gather*}
    P(x\in[\mu-\sigma,\mu+\sigma]) \approx 0,68\\
    P(x\in[\mu-2\sigma,\mu+2\sigma]) \approx 0,955\\
    P(x\in[\mu-1,95\sigma,\mu+1,95\sigma]) \approx 0,95
    \end{gather*}
\end{example}

\begin{teo}[Teorema de Moivre-Laplace]
    Prenem $p$ constant, $X_n=\operatorname{Bin}(n,p)$ i $a,b\in\real$. Llavors,
    \[P\lp a \le \frac{X_n-np}{\sqrt{np(1-p)}} \le b\rp \stackrel{n\rightarrow\infty}{\longrightarrow} \frac{1}{\sqrt{2\pi}} \int_a^b e^\frac{-x^2}{2} \dif x\]
\end{teo}

\begin{obs}
    Demostrarem més endavant, usant funcions característiques, que si $X \sim \normald(\mu_1, \sigma_1^2)$ i $Y \sim \normald(\mu_2, \sigma_2^2)$ són independents
    $X+Y \sim \normald\lp\mu_1+\mu_2, \sigma_1^2+\sigma_2^2\rp$.
\end{obs}

Ara veurem unes quantes distribucions relacionades amb la normal.

\begin{defi}[distribució!$\chi$ quadrat]
    Donades $X \sim \normald(0,1)$, $\lc X_i \rc_{i=1}^n$ independents, la distribucó $\chi^2$ (chi quadrat) és 
    \[\chi_n^2 \sim X_1^2 + \cdots X_n^2\]
\end{defi}

\begin{defi}[distribució!de Fisher-Snedecor]\index{distribució!F((tlab))}
    Donades $\chi^2_{d_1}, \chi^2_{d_2}$ independents, la distribució de Fisher-Snedecor o 
distribució F és
    \[\operatorname{F} \sim \frac{\chi^2_{d_1}/d_1}{\chi^2_{d_2}/d_2}\]
\end{defi}

\begin{defi}[distribució!t de Student]
    Donades $\normald(0,1)$ i $\chi_k^2$ independents, la distribució t de Student és
    \[t \sim \frac{\normald(0,1)}{\sqrt{\chi_k^2 / k}}\]
\end{defi}

\subsection{Distribució normal multivariant}

Es tracta d'un vector de variables aleatòries que generalitza la normal univariant.

\begin{defi}[distribució!normal multivariant]
    El vector aleatori $\overrightarrow{X} = (X_1, \dots, X_n)$ té una distribució normal multivariant si $f_{\overrightarrow{X}}(x_1,\dots,x_n)$ s'escriu com
    \[\frac{1}{\sqrt{(2\pi)^n \det{\Sigma}}} \exp\lp-\frac{1}{2} (\mathbf{x} - \pmb{\mu})^T \Sigma^{-1} (\mathbf{x} - \pmb{\mu})\rp\]
    on $\mathbf{x} = (x_1,\dots,x_n), \pmb{\mu} = (\mu_1,\dots,\mu_n) \in \real^n$ i $\Sigma$ és una matriu $n \times n$ simètrica definida positiva.
\end{defi}

\begin{obs}
    \[ \int_{-\infty}^{+\infty} \cdots \int_{-\infty}^{+\infty} f_{\overrightarrow{X}}(x_1,\dots,x_n) \dif x_1 \cdots \dif x_n = 1 \]
\end{obs}

\begin{example}
    Si $U_1,\dots,U_n$ són v.a. $\normald(0,1)$ independents, $\overrightarrow{U} = (U_1,\dots,U_n)$. Aleshores 
    \begin{align*}
    f_{\overrightarrow{U}} (x_1,\dots,x_n) &\stackrel{\text{indep.}}{=}
    \prod_{i=1}^n \frac{1}{\sqrt{2\pi}} \exp\lp-\frac{x_i^2}{2}\rp \\
    &= \frac{1}{\sqrt{(2\pi)^n \cdot 1}} \exp\lp-\frac{1}{2}\lp x_1^2 + \cdots x_n^2 \rp\rp \\
    &= \frac{1}{\sqrt{(2\pi)^n \det(\Sigma)}} \exp\lp -\frac{1}{2}\mathbf{x}^T \Sigma^{-1} \mathbf{x} \rp
    \end{align*}
    on $\Sigma = I_n$ és la identitat, que és simètrica i definida positiva.
\end{example}

\begin{obs}
    De fet, el que veurem ara és que si $\overrightarrow{X}$ és una multivariant,
    aleshores $\overrightarrow{X}$ es pot construir mitjançant una transformació lineal
    de $\overrightarrow{U} = \lp U_1,\dots,U_n \rp$ on $U_i \sim \normald(0,1)$ i
    $\lc U_i \rc_{i=1}^n$ independents.
\end{obs}

\begin{teo*}
    Si $\overrightarrow{X}$ és una normal univariant, aleshores $\exists A$ matriu
    quadrada no singular, un vector $\mathbf{b}$ pel qual $\overrightarrow{X} =
    A\overrightarrow{U} + \mathbf{b}$, on $\overrightarrow{U} = (U_1,\dots,U_n)$
    amb $U_i \sim \normald(0,1)$ són v.a. independents.
\end{teo*}

\begin{proof}
    \newcommand{\xmm}{(\mathbf{x}-\pmb{\mu})}
    $\Sigma$ és simètrica i definida positiva, per tant $\Sigma^{-1}$ també, i per tant
    $\Sigma^{-1} = A^TDA$, on $D$ és una matriu diagonal amb tots els elements de la
    diagonal positius, i $A$ és una matriu no singular (ve de fer un canvi de base). Així,
    \[ -\frac{1}{2}\xmm^T \Sigma^{-1} \xmm
    = -\frac{1}{2} \lb A\xmm \rb^T D \lb A\xmm \rb \]
    Si fem $A\xmm = \mathbf{y}$, la funció de densitat obtinguda és
    \[f_\mathbf{y}(y_1,\dots,y_n) =
    \frac{1}{\sqrt{(2\pi)^n \det(D)}} \exp(-\frac{1}{2} \mathbf{y}^T D \mathbf{y})\]
    Aquesta fórmula és conseqüència de tenir en compte el jacobià de canvi de base.
    Ara, si $D = \diag(d_1^2,\dots,d_n^2)$ amb els $d_i^2 > 0$,
    \[f_\mathbf{y}(y_1,\dots,y_n) =
    \prod_{i=1}^n \frac{1}{2\pi d_i^2} \exp(-\frac{1}{2} y_i^2 d_i^2)\]
    Observem que amb això no demostrem el que volíem perquè les components no són
    $\normald(0,1)$. Això ho podem arreglar prenent
    \[\Sigma^{-1} = A^TDA = Ad^TdA= (dA)^T \cdot I_n (dA)\]
    on $d = \diag(d_1,\dots,d_n)$, i ara sí que obtenim v.a. $\normald(0,1)$.
\end{proof}

\begin{obs}
    Ho veurem quan fem funcions característiques: $\esp[\overrightarrow{X}] = \pmb{\mu}$, $\Sigma_{ij} = \cov(X_i, Y_j)$.
\end{obs}

\begin{teo*}
    Sigui $\overrightarrow{X}$ una normal multivariant amb paràmetres
    $\pmb{\mu}$ i $\Sigma$ ($\normald(\pmb{\mu}, \Sigma)$) i sigui
    $M$ una matriu $m \times n$ amb rang $m$, $m\le n$, on $n$ és el nombre de
    components de $\overrightarrow{X}$. Considerem $\overrightarrow{Y} = M\overrightarrow{X}$. Aleshores
    \[\overrightarrow{Y} \sim \normald(M\pmb{\mu},M\Sigma M^T)\]
\end{teo*}

\begin{example}
    Si prenem $\lc a_i \rc_{i=1}^n$ tal que $\sum_{i=1}^n a_i^2 > 0$ (algun és no nul),
    aleshores prenent $M = (a_1,\dots,a_n)$ tenim que $M\overrightarrow{X} = \sum_{i=1}^n a_i X_i$ és una normal.
    Per tant, encara que les $\lc X_i \rc_{i=1}^n$ siguin normals no independents
    (però provenen de la mateixa normal multivariant), la seva suma sí que és una normal.
    Això no ho podem afirmar en general per qualsevol parella de v.a. normals $Y_1,Y_2$.
\end{example}

\subsection{Estimadors i teorema de Fisher}

Els resultats que veurem a continuació es veuen amb més detall a l'assignatura d'Estadística.

\begin{defi}[esperança!mostral]
    Donades les v.a. $\lc X_i \rc_{i=1}^n$ independents i idènticament distribuïdes,
    l'esperança mostral és
    \[\bar{X} = \frac{X_1 + \cdots X_n}{n}\]
\end{defi}

\begin{defi}[variància!mostral]
    La variància mostral és
    \[S^2 = \frac{1}{n-1} \sum_{i=1}^n (X_i - \bar{X})^2\]
\end{defi}

\begin{obs}
    En particular, si $\esp[X_i] = \mu, \var[X_i] = \sigma^2$, aleshores
    $\esp[\bar{X}] = \mu, \var[\bar{X}] = \frac{\sigma^2}{n}, \esp[S^2] = \sigma^2$.
\end{obs}

El teorema de Fisher particularitza aquests estimadors quan $X_i \sim \normald(\mu,\sigma^2)$.

\begin{teo}[Teorema de Fisher]
    Siguin $\lc X_i \rc_{i=1}^n$ v.a. independents amb $X_i \sim \normald(\mu,\sigma^2)$. Aleshores, $\bar{X}$ i $S^2$ són independents i tenim que
    \[\bar{X} \sim \normald\lp\mu,\frac{\sigma^2}{n}\rp,\quad\quad S^2 = \frac{n-1}{\sigma^2} \xi_{n-1}^2\]
\end{teo}

\section[Funcions de variables aleatòries absolutament contínues]
    {Funcions de variables aleatòries absolutament 
    \\ contínues
    \sectionmark{Funcions de variables aleatòries contínues}}
    \sectionmark{Funcions de variables aleatòries contínues}

Volem resoldre el següent problema:

\begin{problema}
    Sigui $\overrightarrow{X} = (X_1,\dots,X_n)$ v.a. multidimensional absolutament
    contínua amb funció de densitat $f_{\overrightarrow{X}}(x_1,\dots,x_n)$. Sigui
    $g\colon \real^n \to \real^n$ bijectiva i $\C^1(\real^n)$.
    Si $g(\overrightarrow{X}) = \overrightarrow{Y}$, com podem trobar $f_{\overrightarrow{Y}}$?
\end{problema}

\begin{sol}
    Farem primer el cas unidimensional, $n=1$. Sigui $X$ v.a. amb funció de densitat
    $f_X(x)$ i $g\colon\real\to\real$ derivable i bijectiva. Suposem que $g$ és a més
    monòtona creixent i $g^{-1} = h$. Aleshores, si fem $Y=g(X)$:
    \[F_Y(u) = P(Y\le u) = P(g(X)\le u) = P(X\le h(u)) =
    \int_{-\infty}^{h(u)} f_X(x) \dif x\]
    que fent $x = h(y), \dif x = h(y) \dif y$ és
    \[F_Y(u) = \int_{-\infty}^{u} f_X(h(y))h'(y) \dif y\]
    Fent els càlculs corresponents si $g$ és monòtona decrexent obtenim que
    \[f_Y(y) = f_X(h(y)) (h'(y))\]
    
    En el cas multivariant, el paper de ser bijectiva i estrictament monòtona ve donat
    pel fet que $\det(J_h(y_1,\dots,y_n)) \neq 0$ ($J_h$ és la jacobiana de $h$) i,
    procedint com abans, la transformació que fem és
    \[f_{\overrightarrow{Y}}(y_1,\dots,y_n) =
    f_{\overrightarrow{X}}(h(y_1,\dots,y_n)) \det(J_h(y_1,\dots,y_n))\]
\end{sol}

\subsection{Aplicacions}

\begin{problema}
    Siguin $X_1, X_2$ v.a. i $\overrightarrow{X} = (X_1,X_2)$ i volem calcular la
    funció de densitat de $X_1+X_2$ en termes de $f_{X_1}(x_1)$, $f_{X_2}(x_2)$ o
    $f_{\overrightarrow{X}}(x_1,x_2)$.
\end{problema}

\begin{sol}
    %TODO
\end{sol}

\begin{problema}
    Siguin $X_1, X_2$ v.a., volem calcular la funció de densitat de $X_1 \cdot X_2$.
\end{problema}

\begin{sol}
    %TODO
\end{sol}

Una altra aplicació n'és la simulació de v.a. Partint d'una v.a. $\operatorname{U}[0,1]$
construirem qualsevol altra v.a. $X$.

\begin{lema}[Simulació de variables aleatòries]
    Sigui $X$ una v.a. amb funció de distribució $F_X(x)$. Aleshores $F_X(X)$ és
    una uniforme $\operatorname{U}[0,1]$ (i per tant $F_X^{-1}(U) = X$).
\end{lema}

\begin{proof}
    Buscarem la funció de densitat de $F_X(X)$. Això ho podem fer perquè $F_X(x)$ és
    $\C^1(\real)$ i estrictament creixent (després veurem que fem si no és estrictament
    creixent):
    \[\begin{aligned}
        g \colon \real &\to [0,1] \\
        y &\mapsto F_X(y)
    \end{aligned},\quad
    \begin{aligned}
        h \colon (0,1) &\to \real \\
        x &\mapsto F_X^{-1}(x)
    \end{aligned} \quad (h=g^{-1})\]
    Calculem doncs
    \begin{align*}
        f_{F_{X}(X)}(x) &= f_{g(X)}(x) \\
        &= f_X(F_X^{-1}(x)) \frac{\dif}{\dif x}(h(x)) \\
        &= f_X(F_X^{-1}(x)) \frac{\dif}{\dif x}(F_X^{-1}(x)) \\
        &= f_X(F_X^{-1}(x)) \frac{1}{f_X(F_X^{-1}(x))} = 1
    \end{align*}
    per tant $F_X(X)$ és una v.a. uniforme.
    
    Aquest argument només és vàlid quan $F_X$ és estrictament creixent. En cas
    contrari, els punts on $F_X'(x)$ és 0 és un conjunt numerable i argumentant sobre
    la resta obtenim que $f_{F_X(X)}(x) = 1$ llevat d'un conjunt de mesura 0.
\end{proof}
