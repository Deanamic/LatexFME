#!/bin/bash

upload(){
    NAME=$1
    DEST=$2
    MSG=$3
    BRANCH="master"
    PID="9184456"
    read -p "Nom d'usuari: " user
    read -s -p "Contrasenya: " password
    echo ""

    if make "silent$NAME" ; then
        make "silent$NAME"
        echo ""
        echo "Això trigarà una mica... Keep calm and carry on!"

        #Get Oauth access token
        T=$(curl -s -d "grant_type=password&username=$user&password=$password" \
            -X POST "https://gitlab.com/oauth/token" | \
            python -c "import sys, json; print(json.load(sys.stdin)['access_token'])")

        AUT="Authorization: Bearer $T"
        CONTENT=$(base64 -w 0 $NAME.pdf)
        JSON='{"branch": "'$BRANCH'", "encoding": "base64", "content": "'$CONTENT'", "commit_message": "'$MSG'"}'

        ANS=$(curl -s -H "$AUT" "https://gitlab.com/api/v4/projects/$PID/repository/files/$DEST?ref=$BRANCH")

        #Aixo d'aqui es bastant cutrillo
        if [ "$ANS" = '{"message":"404 File Not Found"}' ]; then
            echo -n "$JSON" | curl -s -X POST -H "$AUT" -H "Content-Type: application/json" -d @- \
                "https://gitlab.com/api/v4/projects/$PID/repository/files/$DEST"
        else 
            echo -n "$JSON" | curl -s -X PUT -H "$AUT" -H "Content-Type: application/json" -d @- \
                "https://gitlab.com/api/v4/projects/$PID/repository/files/$DEST"
        fi
        echo ""

        echo "Ja s'ha actualitzat, mira Telegram per assegurar-te'n."
    else 
        echo ""
        echo "Eps! No compila"
    fi
}
